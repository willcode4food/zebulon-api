USE [master]
GO
/****** Object:  Database [zebulonDB]    Script Date: 11/28/2015 1:31:00 PM ******/
CREATE DATABASE [zebulonDB]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'tnGameServerDB', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.MSSQLSERVER\MSSQL\DATA\zebulonDB.mdf' , SIZE = 8256KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'tnGameServerDB_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.MSSQLSERVER\MSSQL\DATA\zebulonDB_log.ldf' , SIZE = 16832KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [zebulonDB] SET COMPATIBILITY_LEVEL = 110
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [zebulonDB].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [zebulonDB] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [zebulonDB] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [zebulonDB] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [zebulonDB] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [zebulonDB] SET ARITHABORT OFF 
GO
ALTER DATABASE [zebulonDB] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [zebulonDB] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [zebulonDB] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [zebulonDB] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [zebulonDB] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [zebulonDB] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [zebulonDB] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [zebulonDB] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [zebulonDB] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [zebulonDB] SET  DISABLE_BROKER 
GO
ALTER DATABASE [zebulonDB] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [zebulonDB] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [zebulonDB] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [zebulonDB] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [zebulonDB] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [zebulonDB] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [zebulonDB] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [zebulonDB] SET RECOVERY FULL 
GO
ALTER DATABASE [zebulonDB] SET  MULTI_USER 
GO
ALTER DATABASE [zebulonDB] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [zebulonDB] SET DB_CHAINING OFF 
GO
ALTER DATABASE [zebulonDB] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [zebulonDB] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [zebulonDB] SET DELAYED_DURABILITY = DISABLED 
GO
USE [zebulonDB]
GO
/****** Object:  User [DJANGO\willcode4food]    Script Date: 11/28/2015 1:31:00 PM ******/
CREATE USER [DJANGO\willcode4food] FOR LOGIN [DJANGO\willcode4food] WITH DEFAULT_SCHEMA=[dbo]
GO
ALTER ROLE [db_owner] ADD MEMBER [DJANGO\willcode4food]
GO
/****** Object:  UserDefinedFunction [dbo].[splitstring]    Script Date: 11/28/2015 1:31:01 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[splitstring] ( @stringToSplit VARCHAR(MAX) )
RETURNS
 @returnList TABLE ([Name] [nvarchar] (500))
AS
BEGIN

 DECLARE @name NVARCHAR(255)
 DECLARE @pos INT

 WHILE CHARINDEX(',', @stringToSplit) > 0
 BEGIN
  SELECT @pos  = CHARINDEX(',', @stringToSplit)  
  SELECT @name = SUBSTRING(@stringToSplit, 1, @pos-1)

  INSERT INTO @returnList 
  SELECT @name

  SELECT @stringToSplit = SUBSTRING(@stringToSplit, @pos+1, LEN(@stringToSplit)-@pos)
 END

 INSERT INTO @returnList
 SELECT @stringToSplit

 RETURN
END
GO
/****** Object:  Table [dbo].[AspNetRoles]    Script Date: 11/28/2015 1:31:01 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetRoles](
	[Id] [nvarchar](128) NOT NULL,
	[Name] [nvarchar](256) NOT NULL,
 CONSTRAINT [PK_dbo.AspNetRoles] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AspNetUserClaims]    Script Date: 11/28/2015 1:31:01 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUserClaims](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [nvarchar](128) NOT NULL,
	[ClaimType] [nvarchar](max) NULL,
	[ClaimValue] [nvarchar](max) NULL,
 CONSTRAINT [PK_dbo.AspNetUserClaims] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AspNetUserLogins]    Script Date: 11/28/2015 1:31:01 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUserLogins](
	[LoginProvider] [nvarchar](128) NOT NULL,
	[ProviderKey] [nvarchar](128) NOT NULL,
	[UserId] [nvarchar](128) NOT NULL,
 CONSTRAINT [PK_dbo.AspNetUserLogins] PRIMARY KEY CLUSTERED 
(
	[LoginProvider] ASC,
	[ProviderKey] ASC,
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AspNetUserRoles]    Script Date: 11/28/2015 1:31:01 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUserRoles](
	[UserId] [nvarchar](128) NOT NULL,
	[RoleId] [nvarchar](128) NOT NULL,
 CONSTRAINT [PK_dbo.AspNetUserRoles] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC,
	[RoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AspNetUsers]    Script Date: 11/28/2015 1:31:01 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUsers](
	[Id] [nvarchar](128) NOT NULL,
	[Email] [nvarchar](256) NULL,
	[EmailConfirmed] [bit] NOT NULL,
	[PasswordHash] [nvarchar](max) NULL,
	[SecurityStamp] [nvarchar](max) NULL,
	[PhoneNumber] [nvarchar](max) NULL,
	[PhoneNumberConfirmed] [bit] NOT NULL,
	[TwoFactorEnabled] [bit] NOT NULL,
	[LockoutEndDateUtc] [datetime] NULL,
	[LockoutEnabled] [bit] NOT NULL,
	[AccessFailedCount] [int] NOT NULL,
	[UserName] [nvarchar](256) NOT NULL,
 CONSTRAINT [PK_dbo.AspNetUsers] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[egs_accounts]    Script Date: 11/28/2015 1:31:01 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[egs_accounts](
	[idAccount] [bigint] IDENTITY(1,1) NOT NULL,
	[idAspNetRoles] [nvarchar](128) NULL,
	[accountName] [nvarchar](256) NULL,
 CONSTRAINT [PK_tngs_account] PRIMARY KEY CLUSTERED 
(
	[idAccount] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[egs_applicationTypes]    Script Date: 11/28/2015 1:31:01 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[egs_applicationTypes](
	[idApplicationType] [bigint] IDENTITY(1,1) NOT NULL,
	[applicationType] [nvarchar](150) NULL,
 CONSTRAINT [PK_tngs_applicaitonType] PRIMARY KEY CLUSTERED 
(
	[idApplicationType] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[egs_clients]    Script Date: 11/28/2015 1:31:01 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[egs_clients](
	[idClient] [bigint] IDENTITY(1,1) NOT NULL,
	[idAccount] [bigint] NOT NULL,
	[idApplicationType] [bigint] NOT NULL,
	[name] [nvarchar](50) NOT NULL,
	[secret] [nvarchar](4000) NOT NULL,
	[description] [nvarchar](150) NULL,
	[isActive] [bit] NULL,
	[refreshTokenLifetime] [int] NULL,
	[allowedOrigin] [nvarchar](500) NULL,
	[clientIdentifier] [nvarchar](50) NULL,
 CONSTRAINT [PK_tngs_clients] PRIMARY KEY CLUSTERED 
(
	[idClient] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[egs_counter]    Script Date: 11/28/2015 1:31:01 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[egs_counter](
	[idCounter] [int] IDENTITY(1,1) NOT NULL,
	[idGame] [bigint] NULL,
	[counterStart] [bigint] NULL,
	[counterEnd] [bigint] NULL,
 CONSTRAINT [PK_egs_counter] PRIMARY KEY CLUSTERED 
(
	[idCounter] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[egs_counterLanguage]    Script Date: 11/28/2015 1:31:01 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[egs_counterLanguage](
	[idCounterLanguage] [int] IDENTITY(1,1) NOT NULL,
	[idLanuage] [int] NULL,
	[idCounter] [int] NULL,
	[counterName] [nvarchar](100) NULL,
 CONSTRAINT [PK_egs_counterLanguage] PRIMARY KEY CLUSTERED 
(
	[idCounterLanguage] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[egs_country]    Script Date: 11/28/2015 1:31:01 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[egs_country](
	[idCountry] [int] IDENTITY(1,1) NOT NULL,
	[name] [varchar](150) NULL,
 CONSTRAINT [PK_egs_country] PRIMARY KEY CLUSTERED 
(
	[idCountry] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[egs_game]    Script Date: 11/28/2015 1:31:01 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[egs_game](
	[idGame] [bigint] IDENTITY(1,1) NOT NULL,
	[idAccount] [bigint] NULL,
	[name] [varchar](150) NULL,
	[description] [varchar](max) NULL,
 CONSTRAINT [PK_egs_game] PRIMARY KEY CLUSTERED 
(
	[idGame] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[egs_gamePiece]    Script Date: 11/28/2015 1:31:01 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[egs_gamePiece](
	[idGamePiece] [bigint] IDENTITY(1,1) NOT NULL,
	[idGame] [bigint] NULL,
	[idGamePieceType] [int] NULL,
	[createdDate] [datetime] NULL,
	[updatedDate] [datetime] NULL,
	[isActive] [bit] NULL,
	[createdBy] [nvarchar](128) NULL,
 CONSTRAINT [PK_egs_gamePiece] PRIMARY KEY CLUSTERED 
(
	[idGamePiece] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[egs_gamePieceAttribute]    Script Date: 11/28/2015 1:31:01 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[egs_gamePieceAttribute](
	[idGamePieceAttribute] [bigint] IDENTITY(1,1) NOT NULL,
	[idGamePiece] [bigint] NULL,
	[idGamePieceAttributeType] [int] NULL,
	[value] [nvarchar](max) NULL,
 CONSTRAINT [PK_egs_gamePieceAttribute] PRIMARY KEY CLUSTERED 
(
	[idGamePieceAttribute] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[egs_gamePieceAttributeType]    Script Date: 11/28/2015 1:31:01 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[egs_gamePieceAttributeType](
	[idGamePieceAttributeType] [int] IDENTITY(1,1) NOT NULL,
	[idGame] [bigint] NULL,
	[pieceAttributeType] [nvarchar](100) NULL,
 CONSTRAINT [PK_egs_gamePieceAttributeType] PRIMARY KEY CLUSTERED 
(
	[idGamePieceAttributeType] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[egs_gamePieceAttrubuteTypeLanguage]    Script Date: 11/28/2015 1:31:01 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[egs_gamePieceAttrubuteTypeLanguage](
	[idGamePieceAttributeLanguage] [bigint] IDENTITY(1,1) NOT NULL,
	[idGamePieceAttributeType] [int] NULL,
	[idLanguage] [int] NULL,
	[name] [nvarchar](50) NULL,
 CONSTRAINT [PK_egs_gamePieceAttrubuteLanguage] PRIMARY KEY CLUSTERED 
(
	[idGamePieceAttributeLanguage] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[egs_gamePieceCollection]    Script Date: 11/28/2015 1:31:01 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[egs_gamePieceCollection](
	[idGamePieceCollection] [bigint] IDENTITY(1,1) NOT NULL,
	[idGame] [bigint] NULL,
	[idGamePieceCollectionType] [int] NULL,
	[size] [int] NULL,
 CONSTRAINT [PK_egs_pieceCollection] PRIMARY KEY CLUSTERED 
(
	[idGamePieceCollection] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[egs_gamePieceCollectionAttribute]    Script Date: 11/28/2015 1:31:01 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[egs_gamePieceCollectionAttribute](
	[idGamePieceCollectionAttribute] [bigint] IDENTITY(1,1) NOT NULL,
	[idGamePieceCollection] [bigint] NULL,
	[value] [nvarchar](max) NULL,
	[dataName] [nvarchar](500) NULL,
 CONSTRAINT [PK_egs_gamePieceCollectionAttribute] PRIMARY KEY CLUSTERED 
(
	[idGamePieceCollectionAttribute] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[egs_gamePieceCollectionAttributeLanguage]    Script Date: 11/28/2015 1:31:01 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[egs_gamePieceCollectionAttributeLanguage](
	[idGamePieceCollectionAttributeLanguage] [bigint] IDENTITY(1,1) NOT NULL,
	[idGamePIeceCollectionAttribute] [bigint] NULL,
	[idLanguage] [int] NULL,
	[name] [nvarchar](500) NULL,
 CONSTRAINT [PK_egs_gamePieceCollectionAttributeLanguage] PRIMARY KEY CLUSTERED 
(
	[idGamePieceCollectionAttributeLanguage] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[egs_gamePieceCollectionGame]    Script Date: 11/28/2015 1:31:01 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[egs_gamePieceCollectionGame](
	[idGamePieceCollectionGame] [bigint] IDENTITY(1,1) NOT NULL,
	[idGamePieceCollection] [bigint] NOT NULL,
	[idGamePiece] [bigint] NOT NULL,
 CONSTRAINT [PK_egs_gamePieceCollectionGamePiece] PRIMARY KEY CLUSTERED 
(
	[idGamePieceCollectionGame] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[egs_gamePieceCollectionLanguage]    Script Date: 11/28/2015 1:31:01 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[egs_gamePieceCollectionLanguage](
	[idGamePieceCollectionLanguage] [int] IDENTITY(1,1) NOT NULL,
	[idGamePieceCollection] [bigint] NULL,
	[idLanguage] [int] NULL,
	[collectionName] [nvarchar](100) NULL,
 CONSTRAINT [PK_egs_GamePieceCollectionLanguage] PRIMARY KEY CLUSTERED 
(
	[idGamePieceCollectionLanguage] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[egs_gamePieceCollectionPlayer]    Script Date: 11/28/2015 1:31:01 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[egs_gamePieceCollectionPlayer](
	[idGamePieceCollectionPlayer] [bigint] IDENTITY(1,1) NOT NULL,
	[idGamePieceCollection] [bigint] NOT NULL,
	[idPlayer] [bigint] NOT NULL,
	[isDefault] [bit] NULL CONSTRAINT [DF_egs_gamePieceCollectionPlayer_isDefault]  DEFAULT ((0)),
	[dateCreated] [datetime] NULL,
	[dateUpdated] [datetime] NULL,
 CONSTRAINT [PK_egs_gamePieceCollectionPlayer_1] PRIMARY KEY CLUSTERED 
(
	[idGamePieceCollectionPlayer] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[egs_gamePieceCollectionPlayerPiece]    Script Date: 11/28/2015 1:31:01 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[egs_gamePieceCollectionPlayerPiece](
	[idGamePieceCollectionPlayerPiece] [bigint] IDENTITY(1,1) NOT NULL,
	[idGamePIeceCollectionPlayer] [bigint] NOT NULL,
	[idGamePiece] [bigint] NOT NULL,
 CONSTRAINT [PK_egs_gamePieceCollectionPlayer] PRIMARY KEY CLUSTERED 
(
	[idGamePieceCollectionPlayerPiece] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[egs_gamePieceCollectionType]    Script Date: 11/28/2015 1:31:01 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[egs_gamePieceCollectionType](
	[idGamePieceCollectionType] [int] IDENTITY(1,1) NOT NULL,
	[idGame] [bigint] NULL,
	[collectionType] [nvarchar](100) NULL,
 CONSTRAINT [PK_egs_pieceCollectionType] PRIMARY KEY CLUSTERED 
(
	[idGamePieceCollectionType] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[egs_gamePieceLanguage]    Script Date: 11/28/2015 1:31:01 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[egs_gamePieceLanguage](
	[idGamePieceLanguage] [bigint] IDENTITY(1,1) NOT NULL,
	[idLanguage] [int] NULL,
	[idGamePiece] [bigint] NULL,
	[name] [nvarchar](200) NULL,
	[description] [nvarchar](max) NULL,
 CONSTRAINT [PK_egs_gamePieceLanguage] PRIMARY KEY CLUSTERED 
(
	[idGamePieceLanguage] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[egs_gamePieceSkin]    Script Date: 11/28/2015 1:31:01 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[egs_gamePieceSkin](
	[idGamePieceSkin] [int] IDENTITY(1,1) NOT NULL,
	[idGamePiece] [bigint] NULL,
	[smallImg] [nvarchar](500) NULL,
	[bigImg] [nvarchar](500) NULL,
	[minImg] [nvarchar](500) NULL,
	[font1] [nvarchar](100) NULL,
	[font2] [nvarchar](100) NULL,
 CONSTRAINT [PK_egs_gamePieceSkin] PRIMARY KEY CLUSTERED 
(
	[idGamePieceSkin] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[egs_gamePieceType]    Script Date: 11/28/2015 1:31:01 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[egs_gamePieceType](
	[idGamePieceType] [int] IDENTITY(1,1) NOT NULL,
	[idGame] [bigint] NOT NULL,
 CONSTRAINT [PK_egs_gamePieceType] PRIMARY KEY CLUSTERED 
(
	[idGamePieceType] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[egs_gamePieceTypeLanguage]    Script Date: 11/28/2015 1:31:01 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[egs_gamePieceTypeLanguage](
	[idGamePIeceTypeLanguage] [bigint] IDENTITY(1,1) NOT NULL,
	[idGamePieceType] [int] NULL,
	[idLanguage] [int] NULL,
	[pieceType] [nvarchar](100) NULL,
 CONSTRAINT [PK_egs_gamePieceTypeLanguage] PRIMARY KEY CLUSTERED 
(
	[idGamePIeceTypeLanguage] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[egs_gameSession]    Script Date: 11/28/2015 1:31:01 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[egs_gameSession](
	[idGameSession] [bigint] IDENTITY(1,1) NOT NULL,
	[idGame] [bigint] NULL,
	[isActive] [bit] NOT NULL CONSTRAINT [DF_egs_gameSession_isActive]  DEFAULT ((0)),
	[startTime] [datetime] NULL,
	[endTime] [datetime] NULL,
	[lastTurn] [datetime] NULL,
	[thumbnail] [image] NULL,
 CONSTRAINT [PK_egs_gamesession] PRIMARY KEY CLUSTERED 
(
	[idGameSession] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[egs_gameSessionPlayer]    Script Date: 11/28/2015 1:31:01 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[egs_gameSessionPlayer](
	[idGameSessionPlayer] [bigint] IDENTITY(1,1) NOT NULL,
	[idPlayer] [bigint] NOT NULL,
	[idGameSession] [bigint] NOT NULL,
	[isCreator] [bit] NOT NULL CONSTRAINT [DF_egs_gameSessionPlayer_isCreator]  DEFAULT ((1)),
	[hasAccepted] [bit] NULL,
 CONSTRAINT [PK_egs_gameSessionPlayer] PRIMARY KEY CLUSTERED 
(
	[idGameSessionPlayer] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[egs_gameSessionPlayerCollection]    Script Date: 11/28/2015 1:31:01 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[egs_gameSessionPlayerCollection](
	[idGameSessionPlayerCollection] [bigint] IDENTITY(1,1) NOT NULL,
	[idGameSessionPlayer] [bigint] NULL,
	[idGamePieceCollectionPlayer] [bigint] NULL,
 CONSTRAINT [PK_egs_gameSessionPlayerCollection] PRIMARY KEY CLUSTERED 
(
	[idGameSessionPlayerCollection] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[egs_language]    Script Date: 11/28/2015 1:31:01 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[egs_language](
	[idLanguage] [int] IDENTITY(1,1) NOT NULL,
	[language] [nvarchar](50) NULL,
	[culture] [nvarchar](50) NULL,
 CONSTRAINT [PK_egs_language] PRIMARY KEY CLUSTERED 
(
	[idLanguage] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[egs_player]    Script Date: 11/28/2015 1:31:01 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[egs_player](
	[idUser] [nvarchar](128) NULL,
	[idPlayer] [bigint] IDENTITY(1,1) NOT NULL,
	[firstname] [varchar](50) NULL,
	[lastname] [varchar](50) NULL,
	[city] [varchar](50) NULL,
	[idRegion] [int] NULL,
	[nickName] [nvarchar](128) NULL,
 CONSTRAINT [PK_egs_player] PRIMARY KEY CLUSTERED 
(
	[idPlayer] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[egs_refreshTokens]    Script Date: 11/28/2015 1:31:01 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[egs_refreshTokens](
	[idRefeshToken] [bigint] IDENTITY(1,1) NOT NULL,
	[idAspNetUser] [nvarchar](128) NULL,
	[token] [nvarchar](4000) NULL,
	[protectedTicket] [nvarchar](4000) NULL,
	[issuedUTC] [datetime] NULL,
	[expiredUTC] [datetime] NULL,
	[clientIdentifier] [nvarchar](50) NULL,
 CONSTRAINT [PK_tngs_refreshTokens] PRIMARY KEY CLUSTERED 
(
	[idRefeshToken] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[egs_region]    Script Date: 11/28/2015 1:31:01 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[egs_region](
	[idRegion] [int] IDENTITY(1,1) NOT NULL,
	[idCountry] [int] NULL,
	[abbreviation] [varchar](10) NULL,
	[name] [varchar](50) NULL,
 CONSTRAINT [PK_egs_region] PRIMARY KEY CLUSTERED 
(
	[idRegion] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  View [dbo].[v_egs_collections]    Script Date: 11/28/2015 1:31:01 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[v_egs_collections]
AS
SELECT dbo.egs_gamePieceCollection.idGamePieceCollection, dbo.egs_gamePieceCollection.idGame, dbo.egs_gamePieceCollectionType.collectionType, dbo.egs_gamePieceCollection.size, dbo.egs_gamePieceCollectionLanguage.collectionName, 
             dbo.egs_language.culture
FROM   dbo.egs_gamePieceCollection INNER JOIN
             dbo.egs_gamePieceCollectionLanguage ON dbo.egs_gamePieceCollection.idGamePieceCollection = dbo.egs_gamePieceCollectionLanguage.idGamePieceCollection INNER JOIN
             dbo.egs_gamePieceCollectionType ON dbo.egs_gamePieceCollection.idGamePieceCollectionType = dbo.egs_gamePieceCollectionType.idGamePieceCollectionType INNER JOIN
             dbo.egs_language ON dbo.egs_gamePieceCollectionLanguage.idLanguage = dbo.egs_language.idLanguage

GO
INSERT [dbo].[AspNetRoles] ([Id], [Name]) VALUES (N'35399a7f-b8ff-4d24-81b9-145c2e645939', N'Account Admin')
INSERT [dbo].[AspNetRoles] ([Id], [Name]) VALUES (N'45bb9a6d-e988-4683-8105-35b8a8aa5b12', N'Admin')
INSERT [dbo].[AspNetRoles] ([Id], [Name]) VALUES (N'63e213d1-fdb4-4f1f-8805-21e105a62981', N'Player')
INSERT [dbo].[AspNetRoles] ([Id], [Name]) VALUES (N'65a61333-2f76-4088-a456-b0aedc9286f1', N'Service')
SET IDENTITY_INSERT [dbo].[AspNetUserClaims] ON 

INSERT [dbo].[AspNetUserClaims] ([Id], [UserId], [ClaimType], [ClaimValue]) VALUES (5, N'bef1b803-9dd5-4072-90fb-0144420e1a53', N'role', N'Admin')
INSERT [dbo].[AspNetUserClaims] ([Id], [UserId], [ClaimType], [ClaimValue]) VALUES (6, N'bef1b803-9dd5-4072-90fb-0144420e1a53', N'role', N'Player')
INSERT [dbo].[AspNetUserClaims] ([Id], [UserId], [ClaimType], [ClaimValue]) VALUES (2016, N'8ddd55d6-a5d6-4410-966f-07f837edd6b6', N'role', N'Player')
INSERT [dbo].[AspNetUserClaims] ([Id], [UserId], [ClaimType], [ClaimValue]) VALUES (2018, N'0cc13156-af17-469d-9795-2aadab6a8f8e', N'role', N'Player')
INSERT [dbo].[AspNetUserClaims] ([Id], [UserId], [ClaimType], [ClaimValue]) VALUES (3018, N'0cc13156-af17-469d-9795-2aadab6a8f8e', N'role', N'Admin')
INSERT [dbo].[AspNetUserClaims] ([Id], [UserId], [ClaimType], [ClaimValue]) VALUES (3020, N'd023c384-2059-42b9-90b5-5097fdf2ba2b', N'role', N'Player')
INSERT [dbo].[AspNetUserClaims] ([Id], [UserId], [ClaimType], [ClaimValue]) VALUES (4019, N'04d71acc-1a2e-4120-b69f-f8d61ebf62c7', N'role', N'Player')
INSERT [dbo].[AspNetUserClaims] ([Id], [UserId], [ClaimType], [ClaimValue]) VALUES (5021, N'0cc13156-af17-469d-9795-2aadab6a8f8e', N'role', N'Service')
INSERT [dbo].[AspNetUserClaims] ([Id], [UserId], [ClaimType], [ClaimValue]) VALUES (5023, N'208cf252-431d-4c4c-85c3-b0541f766026', N'role', N'Player')
SET IDENTITY_INSERT [dbo].[AspNetUserClaims] OFF
INSERT [dbo].[AspNetUserLogins] ([LoginProvider], [ProviderKey], [UserId]) VALUES (N'Google', N'116700324029851666504', N'04d71acc-1a2e-4120-b69f-f8d61ebf62c7')
INSERT [dbo].[AspNetUserLogins] ([LoginProvider], [ProviderKey], [UserId]) VALUES (N'Google', N'112044984831606854812', N'8ddd55d6-a5d6-4410-966f-07f837edd6b6')
INSERT [dbo].[AspNetUserLogins] ([LoginProvider], [ProviderKey], [UserId]) VALUES (N'Facebook', N'1611414452441567', N'd023c384-2059-42b9-90b5-5097fdf2ba2b')
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'04d71acc-1a2e-4120-b69f-f8d61ebf62c7', NULL, 0, NULL, N'8bd40855-ac58-4b07-95d4-09bceb29daae', NULL, 0, 0, NULL, 0, 0, N'gamesforeddie@gmail.com')
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'0cc13156-af17-469d-9795-2aadab6a8f8e', N'jaylisjayp@yahoo.com', 1, N'AKC7Qg3v8h26vSQVftsFlBu2Dsp6c/4Hpwm82MiX6BzLgFuBBxqcojZO0cy0fASCwg==', N'cf947ccc-6dd8-4d01-bcde-2aeb4f63f075', NULL, 0, 0, NULL, 0, 0, N'jaylisjayp@yahoo.com')
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'208cf252-431d-4c4c-85c3-b0541f766026', NULL, 0, N'AIDgTr7Fn0ANPXAh4BpkxGkF4DbvlyZxktJO0WJO8WbgD/Nmxpc1uMDCJMd+6/7pgg==', N'4da91bd9-334b-4e16-bf41-155fcc1cb258', NULL, 0, 0, NULL, 0, 0, N'eddie@zebulon.io')
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'8ddd55d6-a5d6-4410-966f-07f837edd6b6', NULL, 0, NULL, N'e7b19158-dd1a-4a0f-8f77-97748eb0aff3', NULL, 0, 0, NULL, 0, 0, N'marbesman@gmail.com')
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'bef1b803-9dd5-4072-90fb-0144420e1a53', N'marc@zebulon.io', 0, N'APXWC04BVi9w+6f05OaxIYpWQCZVmkBwG0D+L1Prqpqo4HyfRoiSEBIkjTGx6UyQHQ==', N'2da40d02-6e0c-404b-bc94-d8757ead8274', NULL, 0, 0, NULL, 0, 0, N'marc@zebulon.io')
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'd023c384-2059-42b9-90b5-5097fdf2ba2b', NULL, 0, NULL, N'3972148e-8a50-440d-a0c4-f099abe9e52f', NULL, 0, 0, NULL, 0, 0, N'tnfaxprinter55@gmail.com')
SET IDENTITY_INSERT [dbo].[egs_accounts] ON 

INSERT [dbo].[egs_accounts] ([idAccount], [idAspNetRoles], [accountName]) VALUES (1, NULL, N'Eddie Games')
SET IDENTITY_INSERT [dbo].[egs_accounts] OFF
SET IDENTITY_INSERT [dbo].[egs_applicationTypes] ON 

INSERT [dbo].[egs_applicationTypes] ([idApplicationType], [applicationType]) VALUES (1, N'Private')
INSERT [dbo].[egs_applicationTypes] ([idApplicationType], [applicationType]) VALUES (2, N'Public')
SET IDENTITY_INSERT [dbo].[egs_applicationTypes] OFF
SET IDENTITY_INSERT [dbo].[egs_clients] ON 

INSERT [dbo].[egs_clients] ([idClient], [idAccount], [idApplicationType], [name], [secret], [description], [isActive], [refreshTokenLifetime], [allowedOrigin], [clientIdentifier]) VALUES (2, 1, 1, N'Zebulon Node Server', N'QhgiOJopEW6w90p3Xz73WA1oXDgxaSnB0llXSeTzti4=', NULL, 1, 14400, N'*', N'zeblon_node')
INSERT [dbo].[egs_clients] ([idClient], [idAccount], [idApplicationType], [name], [secret], [description], [isActive], [refreshTokenLifetime], [allowedOrigin], [clientIdentifier]) VALUES (3, 1, 2, N'PostMan Client', N'', NULL, 1, 7200, N'chrome-extension://fdmmgilgnpjigdojojpjoooidkmcomcm', N'postMan')
INSERT [dbo].[egs_clients] ([idClient], [idAccount], [idApplicationType], [name], [secret], [description], [isActive], [refreshTokenLifetime], [allowedOrigin], [clientIdentifier]) VALUES (7, 1, 2, N'Project Neptune Client', N'', NULL, 1, 7200, N'http://localhost:1120', N'webApp')
INSERT [dbo].[egs_clients] ([idClient], [idAccount], [idApplicationType], [name], [secret], [description], [isActive], [refreshTokenLifetime], [allowedOrigin], [clientIdentifier]) VALUES (11, 1, 2, N'Zebulon External Authentication', N' ', NULL, 1, 7200, N'http://api.zebulon.io', N'webApp')
INSERT [dbo].[egs_clients] ([idClient], [idAccount], [idApplicationType], [name], [secret], [description], [isActive], [refreshTokenLifetime], [allowedOrigin], [clientIdentifier]) VALUES (12, 1, 2, N'Project Neptune Client Dev', N' ', NULL, 1, 7200, N'http://local-api.zebulon.io', N'webApp')
SET IDENTITY_INSERT [dbo].[egs_clients] OFF
SET IDENTITY_INSERT [dbo].[egs_counter] ON 

INSERT [dbo].[egs_counter] ([idCounter], [idGame], [counterStart], [counterEnd]) VALUES (1, 1, 0, 10)
INSERT [dbo].[egs_counter] ([idCounter], [idGame], [counterStart], [counterEnd]) VALUES (2, 1, 0, 10)
INSERT [dbo].[egs_counter] ([idCounter], [idGame], [counterStart], [counterEnd]) VALUES (3, 1, 0, 10)
INSERT [dbo].[egs_counter] ([idCounter], [idGame], [counterStart], [counterEnd]) VALUES (4, 1, 0, 10)
SET IDENTITY_INSERT [dbo].[egs_counter] OFF
SET IDENTITY_INSERT [dbo].[egs_counterLanguage] ON 

INSERT [dbo].[egs_counterLanguage] ([idCounterLanguage], [idLanuage], [idCounter], [counterName]) VALUES (1, 1, 1, N'Crew')
INSERT [dbo].[egs_counterLanguage] ([idCounterLanguage], [idLanuage], [idCounter], [counterName]) VALUES (2, 1, 2, N'Power')
INSERT [dbo].[egs_counterLanguage] ([idCounterLanguage], [idLanuage], [idCounter], [counterName]) VALUES (3, 1, 3, N'Leadership')
INSERT [dbo].[egs_counterLanguage] ([idCounterLanguage], [idLanuage], [idCounter], [counterName]) VALUES (4, 1, 4, N'Fleet Command')
SET IDENTITY_INSERT [dbo].[egs_counterLanguage] OFF
SET IDENTITY_INSERT [dbo].[egs_country] ON 

INSERT [dbo].[egs_country] ([idCountry], [name]) VALUES (1, N'United States')
SET IDENTITY_INSERT [dbo].[egs_country] OFF
SET IDENTITY_INSERT [dbo].[egs_game] ON 

INSERT [dbo].[egs_game] ([idGame], [idAccount], [name], [description]) VALUES (1, 1, N'Project Neptune', N'The project neptune online card game')
SET IDENTITY_INSERT [dbo].[egs_game] OFF
SET IDENTITY_INSERT [dbo].[egs_gamePiece] ON 

INSERT [dbo].[egs_gamePiece] ([idGamePiece], [idGame], [idGamePieceType], [createdDate], [updatedDate], [isActive], [createdBy]) VALUES (150, 1, 1, CAST(N'2015-10-24 20:44:14.137' AS DateTime), NULL, 1, N'bef1b803-9dd5-4072-90fb-0144420e1a53')
INSERT [dbo].[egs_gamePiece] ([idGamePiece], [idGame], [idGamePieceType], [createdDate], [updatedDate], [isActive], [createdBy]) VALUES (151, 1, 1, CAST(N'2015-10-24 20:44:14.483' AS DateTime), NULL, 1, N'bef1b803-9dd5-4072-90fb-0144420e1a53')
INSERT [dbo].[egs_gamePiece] ([idGamePiece], [idGame], [idGamePieceType], [createdDate], [updatedDate], [isActive], [createdBy]) VALUES (152, 1, 1, CAST(N'2015-10-24 20:44:14.503' AS DateTime), NULL, 1, N'bef1b803-9dd5-4072-90fb-0144420e1a53')
INSERT [dbo].[egs_gamePiece] ([idGamePiece], [idGame], [idGamePieceType], [createdDate], [updatedDate], [isActive], [createdBy]) VALUES (153, 1, 1, CAST(N'2015-10-24 20:44:14.523' AS DateTime), NULL, 1, N'bef1b803-9dd5-4072-90fb-0144420e1a53')
INSERT [dbo].[egs_gamePiece] ([idGamePiece], [idGame], [idGamePieceType], [createdDate], [updatedDate], [isActive], [createdBy]) VALUES (154, 1, 1, CAST(N'2015-10-24 20:44:14.543' AS DateTime), NULL, 1, N'bef1b803-9dd5-4072-90fb-0144420e1a53')
INSERT [dbo].[egs_gamePiece] ([idGamePiece], [idGame], [idGamePieceType], [createdDate], [updatedDate], [isActive], [createdBy]) VALUES (155, 1, 1, CAST(N'2015-10-24 20:44:14.557' AS DateTime), NULL, 1, N'bef1b803-9dd5-4072-90fb-0144420e1a53')
INSERT [dbo].[egs_gamePiece] ([idGamePiece], [idGame], [idGamePieceType], [createdDate], [updatedDate], [isActive], [createdBy]) VALUES (156, 1, 1, CAST(N'2015-10-24 20:44:14.577' AS DateTime), NULL, 1, N'bef1b803-9dd5-4072-90fb-0144420e1a53')
INSERT [dbo].[egs_gamePiece] ([idGamePiece], [idGame], [idGamePieceType], [createdDate], [updatedDate], [isActive], [createdBy]) VALUES (157, 1, 1, CAST(N'2015-10-24 20:44:14.597' AS DateTime), NULL, 1, N'bef1b803-9dd5-4072-90fb-0144420e1a53')
INSERT [dbo].[egs_gamePiece] ([idGamePiece], [idGame], [idGamePieceType], [createdDate], [updatedDate], [isActive], [createdBy]) VALUES (158, 1, 1, CAST(N'2015-10-24 20:44:14.613' AS DateTime), NULL, 1, N'bef1b803-9dd5-4072-90fb-0144420e1a53')
INSERT [dbo].[egs_gamePiece] ([idGamePiece], [idGame], [idGamePieceType], [createdDate], [updatedDate], [isActive], [createdBy]) VALUES (159, 1, 1, CAST(N'2015-10-24 20:44:14.633' AS DateTime), NULL, 1, N'bef1b803-9dd5-4072-90fb-0144420e1a53')
INSERT [dbo].[egs_gamePiece] ([idGamePiece], [idGame], [idGamePieceType], [createdDate], [updatedDate], [isActive], [createdBy]) VALUES (160, 1, 1, CAST(N'2015-10-24 20:44:14.657' AS DateTime), NULL, 1, N'bef1b803-9dd5-4072-90fb-0144420e1a53')
INSERT [dbo].[egs_gamePiece] ([idGamePiece], [idGame], [idGamePieceType], [createdDate], [updatedDate], [isActive], [createdBy]) VALUES (161, 1, 1, CAST(N'2015-10-24 20:44:14.677' AS DateTime), NULL, 1, N'bef1b803-9dd5-4072-90fb-0144420e1a53')
INSERT [dbo].[egs_gamePiece] ([idGamePiece], [idGame], [idGamePieceType], [createdDate], [updatedDate], [isActive], [createdBy]) VALUES (162, 1, 1, CAST(N'2015-10-24 20:44:14.697' AS DateTime), NULL, 1, N'bef1b803-9dd5-4072-90fb-0144420e1a53')
INSERT [dbo].[egs_gamePiece] ([idGamePiece], [idGame], [idGamePieceType], [createdDate], [updatedDate], [isActive], [createdBy]) VALUES (163, 1, 1, CAST(N'2015-10-24 20:44:14.723' AS DateTime), NULL, 1, N'bef1b803-9dd5-4072-90fb-0144420e1a53')
INSERT [dbo].[egs_gamePiece] ([idGamePiece], [idGame], [idGamePieceType], [createdDate], [updatedDate], [isActive], [createdBy]) VALUES (164, 1, 1, CAST(N'2015-10-24 20:44:14.747' AS DateTime), NULL, 1, N'bef1b803-9dd5-4072-90fb-0144420e1a53')
INSERT [dbo].[egs_gamePiece] ([idGamePiece], [idGame], [idGamePieceType], [createdDate], [updatedDate], [isActive], [createdBy]) VALUES (165, 1, 1, CAST(N'2015-10-24 20:44:14.770' AS DateTime), NULL, 1, N'bef1b803-9dd5-4072-90fb-0144420e1a53')
INSERT [dbo].[egs_gamePiece] ([idGamePiece], [idGame], [idGamePieceType], [createdDate], [updatedDate], [isActive], [createdBy]) VALUES (166, 1, 1, CAST(N'2015-10-24 20:44:14.797' AS DateTime), NULL, 1, N'bef1b803-9dd5-4072-90fb-0144420e1a53')
INSERT [dbo].[egs_gamePiece] ([idGamePiece], [idGame], [idGamePieceType], [createdDate], [updatedDate], [isActive], [createdBy]) VALUES (167, 1, 1, CAST(N'2015-10-24 20:44:14.817' AS DateTime), NULL, 1, N'bef1b803-9dd5-4072-90fb-0144420e1a53')
INSERT [dbo].[egs_gamePiece] ([idGamePiece], [idGame], [idGamePieceType], [createdDate], [updatedDate], [isActive], [createdBy]) VALUES (168, 1, 1, CAST(N'2015-10-24 20:44:14.843' AS DateTime), NULL, 1, N'bef1b803-9dd5-4072-90fb-0144420e1a53')
INSERT [dbo].[egs_gamePiece] ([idGamePiece], [idGame], [idGamePieceType], [createdDate], [updatedDate], [isActive], [createdBy]) VALUES (169, 1, 1, CAST(N'2015-10-24 20:44:14.870' AS DateTime), NULL, 1, N'bef1b803-9dd5-4072-90fb-0144420e1a53')
INSERT [dbo].[egs_gamePiece] ([idGamePiece], [idGame], [idGamePieceType], [createdDate], [updatedDate], [isActive], [createdBy]) VALUES (170, 1, 1, CAST(N'2015-10-24 20:44:14.897' AS DateTime), NULL, 1, N'bef1b803-9dd5-4072-90fb-0144420e1a53')
INSERT [dbo].[egs_gamePiece] ([idGamePiece], [idGame], [idGamePieceType], [createdDate], [updatedDate], [isActive], [createdBy]) VALUES (171, 1, 1, CAST(N'2015-10-24 20:44:14.927' AS DateTime), NULL, 1, N'bef1b803-9dd5-4072-90fb-0144420e1a53')
INSERT [dbo].[egs_gamePiece] ([idGamePiece], [idGame], [idGamePieceType], [createdDate], [updatedDate], [isActive], [createdBy]) VALUES (172, 1, 1, CAST(N'2015-10-24 20:44:14.953' AS DateTime), NULL, 1, N'bef1b803-9dd5-4072-90fb-0144420e1a53')
INSERT [dbo].[egs_gamePiece] ([idGamePiece], [idGame], [idGamePieceType], [createdDate], [updatedDate], [isActive], [createdBy]) VALUES (173, 1, 1, CAST(N'2015-10-24 20:44:14.983' AS DateTime), NULL, 1, N'bef1b803-9dd5-4072-90fb-0144420e1a53')
INSERT [dbo].[egs_gamePiece] ([idGamePiece], [idGame], [idGamePieceType], [createdDate], [updatedDate], [isActive], [createdBy]) VALUES (174, 1, 1, CAST(N'2015-10-24 20:44:15.013' AS DateTime), NULL, 1, N'bef1b803-9dd5-4072-90fb-0144420e1a53')
INSERT [dbo].[egs_gamePiece] ([idGamePiece], [idGame], [idGamePieceType], [createdDate], [updatedDate], [isActive], [createdBy]) VALUES (175, 1, 1, CAST(N'2015-10-24 20:44:15.047' AS DateTime), NULL, 1, N'bef1b803-9dd5-4072-90fb-0144420e1a53')
INSERT [dbo].[egs_gamePiece] ([idGamePiece], [idGame], [idGamePieceType], [createdDate], [updatedDate], [isActive], [createdBy]) VALUES (176, 1, 1, CAST(N'2015-10-24 20:44:15.077' AS DateTime), NULL, 1, N'bef1b803-9dd5-4072-90fb-0144420e1a53')
INSERT [dbo].[egs_gamePiece] ([idGamePiece], [idGame], [idGamePieceType], [createdDate], [updatedDate], [isActive], [createdBy]) VALUES (177, 1, 1, CAST(N'2015-10-24 20:44:15.107' AS DateTime), NULL, 1, N'bef1b803-9dd5-4072-90fb-0144420e1a53')
INSERT [dbo].[egs_gamePiece] ([idGamePiece], [idGame], [idGamePieceType], [createdDate], [updatedDate], [isActive], [createdBy]) VALUES (178, 1, 1, CAST(N'2015-10-24 20:44:15.140' AS DateTime), NULL, 1, N'bef1b803-9dd5-4072-90fb-0144420e1a53')
INSERT [dbo].[egs_gamePiece] ([idGamePiece], [idGame], [idGamePieceType], [createdDate], [updatedDate], [isActive], [createdBy]) VALUES (179, 1, 1, CAST(N'2015-10-24 20:44:15.170' AS DateTime), NULL, 1, N'bef1b803-9dd5-4072-90fb-0144420e1a53')
INSERT [dbo].[egs_gamePiece] ([idGamePiece], [idGame], [idGamePieceType], [createdDate], [updatedDate], [isActive], [createdBy]) VALUES (180, 1, 1, CAST(N'2015-10-24 20:44:15.207' AS DateTime), NULL, 1, N'bef1b803-9dd5-4072-90fb-0144420e1a53')
INSERT [dbo].[egs_gamePiece] ([idGamePiece], [idGame], [idGamePieceType], [createdDate], [updatedDate], [isActive], [createdBy]) VALUES (181, 1, 1, CAST(N'2015-10-24 20:44:15.240' AS DateTime), NULL, 1, N'bef1b803-9dd5-4072-90fb-0144420e1a53')
INSERT [dbo].[egs_gamePiece] ([idGamePiece], [idGame], [idGamePieceType], [createdDate], [updatedDate], [isActive], [createdBy]) VALUES (182, 1, 1, CAST(N'2015-10-24 20:44:15.273' AS DateTime), NULL, 1, N'bef1b803-9dd5-4072-90fb-0144420e1a53')
INSERT [dbo].[egs_gamePiece] ([idGamePiece], [idGame], [idGamePieceType], [createdDate], [updatedDate], [isActive], [createdBy]) VALUES (183, 1, 1, CAST(N'2015-10-24 20:44:15.337' AS DateTime), NULL, 1, N'bef1b803-9dd5-4072-90fb-0144420e1a53')
INSERT [dbo].[egs_gamePiece] ([idGamePiece], [idGame], [idGamePieceType], [createdDate], [updatedDate], [isActive], [createdBy]) VALUES (184, 1, 1, CAST(N'2015-10-24 20:44:15.370' AS DateTime), NULL, 1, N'bef1b803-9dd5-4072-90fb-0144420e1a53')
INSERT [dbo].[egs_gamePiece] ([idGamePiece], [idGame], [idGamePieceType], [createdDate], [updatedDate], [isActive], [createdBy]) VALUES (185, 1, 1, CAST(N'2015-10-24 20:44:15.400' AS DateTime), NULL, 1, N'bef1b803-9dd5-4072-90fb-0144420e1a53')
INSERT [dbo].[egs_gamePiece] ([idGamePiece], [idGame], [idGamePieceType], [createdDate], [updatedDate], [isActive], [createdBy]) VALUES (186, 1, 1, CAST(N'2015-10-24 20:44:15.437' AS DateTime), NULL, 1, N'bef1b803-9dd5-4072-90fb-0144420e1a53')
INSERT [dbo].[egs_gamePiece] ([idGamePiece], [idGame], [idGamePieceType], [createdDate], [updatedDate], [isActive], [createdBy]) VALUES (187, 1, 1, CAST(N'2015-10-24 20:44:15.480' AS DateTime), NULL, 1, N'bef1b803-9dd5-4072-90fb-0144420e1a53')
INSERT [dbo].[egs_gamePiece] ([idGamePiece], [idGame], [idGamePieceType], [createdDate], [updatedDate], [isActive], [createdBy]) VALUES (188, 1, 1, CAST(N'2015-10-24 20:44:15.520' AS DateTime), NULL, 1, N'bef1b803-9dd5-4072-90fb-0144420e1a53')
INSERT [dbo].[egs_gamePiece] ([idGamePiece], [idGame], [idGamePieceType], [createdDate], [updatedDate], [isActive], [createdBy]) VALUES (189, 1, 1, CAST(N'2015-10-24 20:44:15.557' AS DateTime), NULL, 1, N'bef1b803-9dd5-4072-90fb-0144420e1a53')
INSERT [dbo].[egs_gamePiece] ([idGamePiece], [idGame], [idGamePieceType], [createdDate], [updatedDate], [isActive], [createdBy]) VALUES (190, 1, 1, CAST(N'2015-10-24 20:44:15.593' AS DateTime), NULL, 1, N'bef1b803-9dd5-4072-90fb-0144420e1a53')
INSERT [dbo].[egs_gamePiece] ([idGamePiece], [idGame], [idGamePieceType], [createdDate], [updatedDate], [isActive], [createdBy]) VALUES (191, 1, 1, CAST(N'2015-10-24 20:44:15.627' AS DateTime), NULL, 1, N'bef1b803-9dd5-4072-90fb-0144420e1a53')
INSERT [dbo].[egs_gamePiece] ([idGamePiece], [idGame], [idGamePieceType], [createdDate], [updatedDate], [isActive], [createdBy]) VALUES (192, 1, 1, CAST(N'2015-10-24 20:44:15.660' AS DateTime), NULL, 1, N'bef1b803-9dd5-4072-90fb-0144420e1a53')
INSERT [dbo].[egs_gamePiece] ([idGamePiece], [idGame], [idGamePieceType], [createdDate], [updatedDate], [isActive], [createdBy]) VALUES (193, 1, 1, CAST(N'2015-10-24 20:44:15.697' AS DateTime), NULL, 1, N'bef1b803-9dd5-4072-90fb-0144420e1a53')
INSERT [dbo].[egs_gamePiece] ([idGamePiece], [idGame], [idGamePieceType], [createdDate], [updatedDate], [isActive], [createdBy]) VALUES (194, 1, 6, CAST(N'2015-10-24 20:44:15.737' AS DateTime), NULL, 1, N'bef1b803-9dd5-4072-90fb-0144420e1a53')
INSERT [dbo].[egs_gamePiece] ([idGamePiece], [idGame], [idGamePieceType], [createdDate], [updatedDate], [isActive], [createdBy]) VALUES (195, 1, 6, CAST(N'2015-10-24 20:44:15.777' AS DateTime), NULL, 1, N'bef1b803-9dd5-4072-90fb-0144420e1a53')
INSERT [dbo].[egs_gamePiece] ([idGamePiece], [idGame], [idGamePieceType], [createdDate], [updatedDate], [isActive], [createdBy]) VALUES (196, 1, 6, CAST(N'2015-10-24 20:44:15.817' AS DateTime), NULL, 1, N'bef1b803-9dd5-4072-90fb-0144420e1a53')
INSERT [dbo].[egs_gamePiece] ([idGamePiece], [idGame], [idGamePieceType], [createdDate], [updatedDate], [isActive], [createdBy]) VALUES (197, 1, 6, CAST(N'2015-10-24 20:44:15.860' AS DateTime), NULL, 1, N'bef1b803-9dd5-4072-90fb-0144420e1a53')
INSERT [dbo].[egs_gamePiece] ([idGamePiece], [idGame], [idGamePieceType], [createdDate], [updatedDate], [isActive], [createdBy]) VALUES (198, 1, 6, CAST(N'2015-10-24 20:44:15.897' AS DateTime), NULL, 1, N'bef1b803-9dd5-4072-90fb-0144420e1a53')
INSERT [dbo].[egs_gamePiece] ([idGamePiece], [idGame], [idGamePieceType], [createdDate], [updatedDate], [isActive], [createdBy]) VALUES (199, 1, 6, CAST(N'2015-10-24 20:44:15.940' AS DateTime), NULL, 1, N'bef1b803-9dd5-4072-90fb-0144420e1a53')
INSERT [dbo].[egs_gamePiece] ([idGamePiece], [idGame], [idGamePieceType], [createdDate], [updatedDate], [isActive], [createdBy]) VALUES (200, 1, 6, CAST(N'2015-10-24 20:44:15.980' AS DateTime), NULL, 1, N'bef1b803-9dd5-4072-90fb-0144420e1a53')
INSERT [dbo].[egs_gamePiece] ([idGamePiece], [idGame], [idGamePieceType], [createdDate], [updatedDate], [isActive], [createdBy]) VALUES (201, 1, 6, CAST(N'2015-10-24 20:44:16.020' AS DateTime), NULL, 1, N'bef1b803-9dd5-4072-90fb-0144420e1a53')
INSERT [dbo].[egs_gamePiece] ([idGamePiece], [idGame], [idGamePieceType], [createdDate], [updatedDate], [isActive], [createdBy]) VALUES (202, 1, 6, CAST(N'2015-10-24 20:44:16.067' AS DateTime), NULL, 1, N'bef1b803-9dd5-4072-90fb-0144420e1a53')
INSERT [dbo].[egs_gamePiece] ([idGamePiece], [idGame], [idGamePieceType], [createdDate], [updatedDate], [isActive], [createdBy]) VALUES (203, 1, 6, CAST(N'2015-10-24 20:44:16.110' AS DateTime), NULL, 1, N'bef1b803-9dd5-4072-90fb-0144420e1a53')
INSERT [dbo].[egs_gamePiece] ([idGamePiece], [idGame], [idGamePieceType], [createdDate], [updatedDate], [isActive], [createdBy]) VALUES (204, 1, 6, CAST(N'2015-10-24 20:44:16.180' AS DateTime), NULL, 1, N'bef1b803-9dd5-4072-90fb-0144420e1a53')
INSERT [dbo].[egs_gamePiece] ([idGamePiece], [idGame], [idGamePieceType], [createdDate], [updatedDate], [isActive], [createdBy]) VALUES (205, 1, 6, CAST(N'2015-10-24 20:44:16.270' AS DateTime), NULL, 1, N'bef1b803-9dd5-4072-90fb-0144420e1a53')
INSERT [dbo].[egs_gamePiece] ([idGamePiece], [idGame], [idGamePieceType], [createdDate], [updatedDate], [isActive], [createdBy]) VALUES (206, 1, 6, CAST(N'2015-10-24 20:44:16.360' AS DateTime), NULL, 1, N'bef1b803-9dd5-4072-90fb-0144420e1a53')
INSERT [dbo].[egs_gamePiece] ([idGamePiece], [idGame], [idGamePieceType], [createdDate], [updatedDate], [isActive], [createdBy]) VALUES (207, 1, 6, CAST(N'2015-10-24 20:44:16.453' AS DateTime), NULL, 1, N'bef1b803-9dd5-4072-90fb-0144420e1a53')
INSERT [dbo].[egs_gamePiece] ([idGamePiece], [idGame], [idGamePieceType], [createdDate], [updatedDate], [isActive], [createdBy]) VALUES (208, 1, 6, CAST(N'2015-10-24 20:44:16.553' AS DateTime), NULL, 1, N'bef1b803-9dd5-4072-90fb-0144420e1a53')
INSERT [dbo].[egs_gamePiece] ([idGamePiece], [idGame], [idGamePieceType], [createdDate], [updatedDate], [isActive], [createdBy]) VALUES (209, 1, 6, CAST(N'2015-10-24 20:44:16.650' AS DateTime), NULL, 1, N'bef1b803-9dd5-4072-90fb-0144420e1a53')
INSERT [dbo].[egs_gamePiece] ([idGamePiece], [idGame], [idGamePieceType], [createdDate], [updatedDate], [isActive], [createdBy]) VALUES (210, 1, 6, CAST(N'2015-10-24 20:44:16.753' AS DateTime), NULL, 1, N'bef1b803-9dd5-4072-90fb-0144420e1a53')
INSERT [dbo].[egs_gamePiece] ([idGamePiece], [idGame], [idGamePieceType], [createdDate], [updatedDate], [isActive], [createdBy]) VALUES (211, 1, 5, CAST(N'2015-10-24 20:44:16.857' AS DateTime), NULL, 1, N'bef1b803-9dd5-4072-90fb-0144420e1a53')
INSERT [dbo].[egs_gamePiece] ([idGamePiece], [idGame], [idGamePieceType], [createdDate], [updatedDate], [isActive], [createdBy]) VALUES (212, 1, 5, CAST(N'2015-10-24 20:44:16.960' AS DateTime), NULL, 1, N'bef1b803-9dd5-4072-90fb-0144420e1a53')
INSERT [dbo].[egs_gamePiece] ([idGamePiece], [idGame], [idGamePieceType], [createdDate], [updatedDate], [isActive], [createdBy]) VALUES (213, 1, 5, CAST(N'2015-10-24 20:44:17.070' AS DateTime), NULL, 1, N'bef1b803-9dd5-4072-90fb-0144420e1a53')
INSERT [dbo].[egs_gamePiece] ([idGamePiece], [idGame], [idGamePieceType], [createdDate], [updatedDate], [isActive], [createdBy]) VALUES (214, 1, 5, CAST(N'2015-10-24 20:44:17.160' AS DateTime), NULL, 1, N'bef1b803-9dd5-4072-90fb-0144420e1a53')
INSERT [dbo].[egs_gamePiece] ([idGamePiece], [idGame], [idGamePieceType], [createdDate], [updatedDate], [isActive], [createdBy]) VALUES (215, 1, 5, CAST(N'2015-10-24 20:44:17.220' AS DateTime), NULL, 1, N'bef1b803-9dd5-4072-90fb-0144420e1a53')
INSERT [dbo].[egs_gamePiece] ([idGamePiece], [idGame], [idGamePieceType], [createdDate], [updatedDate], [isActive], [createdBy]) VALUES (216, 1, 5, CAST(N'2015-10-24 20:44:17.270' AS DateTime), NULL, 1, N'bef1b803-9dd5-4072-90fb-0144420e1a53')
INSERT [dbo].[egs_gamePiece] ([idGamePiece], [idGame], [idGamePieceType], [createdDate], [updatedDate], [isActive], [createdBy]) VALUES (217, 1, 3, CAST(N'2015-10-24 20:44:17.317' AS DateTime), NULL, 1, N'bef1b803-9dd5-4072-90fb-0144420e1a53')
INSERT [dbo].[egs_gamePiece] ([idGamePiece], [idGame], [idGamePieceType], [createdDate], [updatedDate], [isActive], [createdBy]) VALUES (218, 1, 2, CAST(N'2015-10-24 20:44:17.370' AS DateTime), NULL, 1, N'bef1b803-9dd5-4072-90fb-0144420e1a53')
INSERT [dbo].[egs_gamePiece] ([idGamePiece], [idGame], [idGamePieceType], [createdDate], [updatedDate], [isActive], [createdBy]) VALUES (219, 1, 4, CAST(N'2015-10-24 20:44:17.423' AS DateTime), NULL, 1, N'bef1b803-9dd5-4072-90fb-0144420e1a53')
INSERT [dbo].[egs_gamePiece] ([idGamePiece], [idGame], [idGamePieceType], [createdDate], [updatedDate], [isActive], [createdBy]) VALUES (220, 1, 3, CAST(N'2015-10-24 20:44:17.470' AS DateTime), NULL, 1, N'bef1b803-9dd5-4072-90fb-0144420e1a53')
INSERT [dbo].[egs_gamePiece] ([idGamePiece], [idGame], [idGamePieceType], [createdDate], [updatedDate], [isActive], [createdBy]) VALUES (221, 1, 3, CAST(N'2015-10-24 20:44:17.523' AS DateTime), NULL, 1, N'bef1b803-9dd5-4072-90fb-0144420e1a53')
INSERT [dbo].[egs_gamePiece] ([idGamePiece], [idGame], [idGamePieceType], [createdDate], [updatedDate], [isActive], [createdBy]) VALUES (222, 1, 3, CAST(N'2015-10-24 20:44:17.573' AS DateTime), NULL, 1, N'bef1b803-9dd5-4072-90fb-0144420e1a53')
INSERT [dbo].[egs_gamePiece] ([idGamePiece], [idGame], [idGamePieceType], [createdDate], [updatedDate], [isActive], [createdBy]) VALUES (223, 1, 2, CAST(N'2015-10-24 20:44:17.627' AS DateTime), NULL, 1, N'bef1b803-9dd5-4072-90fb-0144420e1a53')
INSERT [dbo].[egs_gamePiece] ([idGamePiece], [idGame], [idGamePieceType], [createdDate], [updatedDate], [isActive], [createdBy]) VALUES (224, 1, 2, CAST(N'2015-10-24 20:44:17.683' AS DateTime), NULL, 1, N'bef1b803-9dd5-4072-90fb-0144420e1a53')
INSERT [dbo].[egs_gamePiece] ([idGamePiece], [idGame], [idGamePieceType], [createdDate], [updatedDate], [isActive], [createdBy]) VALUES (225, 1, 2, CAST(N'2015-10-24 20:44:17.737' AS DateTime), NULL, 1, N'bef1b803-9dd5-4072-90fb-0144420e1a53')
INSERT [dbo].[egs_gamePiece] ([idGamePiece], [idGame], [idGamePieceType], [createdDate], [updatedDate], [isActive], [createdBy]) VALUES (226, 1, 2, CAST(N'2015-10-24 20:44:17.793' AS DateTime), NULL, 1, N'bef1b803-9dd5-4072-90fb-0144420e1a53')
INSERT [dbo].[egs_gamePiece] ([idGamePiece], [idGame], [idGamePieceType], [createdDate], [updatedDate], [isActive], [createdBy]) VALUES (227, 1, 2, CAST(N'2015-10-24 20:44:17.850' AS DateTime), NULL, 1, N'bef1b803-9dd5-4072-90fb-0144420e1a53')
INSERT [dbo].[egs_gamePiece] ([idGamePiece], [idGame], [idGamePieceType], [createdDate], [updatedDate], [isActive], [createdBy]) VALUES (228, 1, 2, CAST(N'2015-10-24 20:44:17.910' AS DateTime), NULL, 1, N'bef1b803-9dd5-4072-90fb-0144420e1a53')
INSERT [dbo].[egs_gamePiece] ([idGamePiece], [idGame], [idGamePieceType], [createdDate], [updatedDate], [isActive], [createdBy]) VALUES (229, 1, 4, CAST(N'2015-10-24 20:44:17.967' AS DateTime), NULL, 1, N'bef1b803-9dd5-4072-90fb-0144420e1a53')
INSERT [dbo].[egs_gamePiece] ([idGamePiece], [idGame], [idGamePieceType], [createdDate], [updatedDate], [isActive], [createdBy]) VALUES (230, 1, 4, CAST(N'2015-10-24 20:44:18.027' AS DateTime), NULL, 1, N'bef1b803-9dd5-4072-90fb-0144420e1a53')
INSERT [dbo].[egs_gamePiece] ([idGamePiece], [idGame], [idGamePieceType], [createdDate], [updatedDate], [isActive], [createdBy]) VALUES (231, 1, 4, CAST(N'2015-10-24 20:44:18.097' AS DateTime), NULL, 1, N'bef1b803-9dd5-4072-90fb-0144420e1a53')
INSERT [dbo].[egs_gamePiece] ([idGamePiece], [idGame], [idGamePieceType], [createdDate], [updatedDate], [isActive], [createdBy]) VALUES (232, 1, 4, CAST(N'2015-10-24 20:44:18.203' AS DateTime), NULL, 1, N'bef1b803-9dd5-4072-90fb-0144420e1a53')
INSERT [dbo].[egs_gamePiece] ([idGamePiece], [idGame], [idGamePieceType], [createdDate], [updatedDate], [isActive], [createdBy]) VALUES (233, 1, 4, CAST(N'2015-10-24 20:44:18.260' AS DateTime), NULL, 1, N'bef1b803-9dd5-4072-90fb-0144420e1a53')
INSERT [dbo].[egs_gamePiece] ([idGamePiece], [idGame], [idGamePieceType], [createdDate], [updatedDate], [isActive], [createdBy]) VALUES (239, 1, 8, CAST(N'2015-11-10 09:54:46.170' AS DateTime), NULL, 1, N'bef1b803-9dd5-4072-90fb-0144420e1a53')
INSERT [dbo].[egs_gamePiece] ([idGamePiece], [idGame], [idGamePieceType], [createdDate], [updatedDate], [isActive], [createdBy]) VALUES (240, 1, 7, CAST(N'2015-11-10 10:00:43.870' AS DateTime), NULL, 0, N'bef1b803-9dd5-4072-90fb-0144420e1a53')
INSERT [dbo].[egs_gamePiece] ([idGamePiece], [idGame], [idGamePieceType], [createdDate], [updatedDate], [isActive], [createdBy]) VALUES (241, 1, 7, CAST(N'2015-11-10 10:00:43.900' AS DateTime), NULL, 0, N'bef1b803-9dd5-4072-90fb-0144420e1a53')
INSERT [dbo].[egs_gamePiece] ([idGamePiece], [idGame], [idGamePieceType], [createdDate], [updatedDate], [isActive], [createdBy]) VALUES (242, 1, 10, CAST(N'2015-11-18 21:36:31.197' AS DateTime), CAST(N'2015-11-18 21:36:31.197' AS DateTime), 0, N'bef1b803-9dd5-4072-90fb-0144420e1a53')
SET IDENTITY_INSERT [dbo].[egs_gamePiece] OFF
SET IDENTITY_INSERT [dbo].[egs_gamePieceAttribute] ON 

INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (816, 150, 2, N'')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (817, 150, 3, N'Autocannon')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (818, 150, 4, N'7')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (819, 150, 5, N'6')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (820, 150, 6, N'3')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (821, 150, 7, N'3')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (822, 150, 8, N'2')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (823, 150, 9, N'3')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (824, 150, 10, N'5')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (825, 150, 11, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (826, 150, 12, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (827, 150, 13, N'FALSE')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (828, 150, 14, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (829, 150, 15, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (830, 151, 2, N'')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (831, 151, 3, N'Autocannon')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (832, 151, 4, N'7')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (833, 151, 5, N'6')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (834, 151, 6, N'4')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (835, 151, 7, N'3')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (836, 151, 8, N'2')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (837, 151, 9, N'2')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (838, 151, 10, N'5')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (839, 151, 11, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (840, 151, 12, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (841, 151, 13, N'FALSE')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (842, 151, 14, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (843, 151, 15, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (844, 152, 2, N'')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (845, 152, 3, N'Autocannon')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (846, 152, 4, N'8')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (847, 152, 5, N'7')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (848, 152, 6, N'3')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (849, 152, 7, N'2')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (850, 152, 8, N'2')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (851, 152, 9, N'3')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (852, 152, 10, N'5')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (853, 152, 11, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (854, 152, 12, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (855, 152, 13, N'FALSE')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (856, 152, 14, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (857, 152, 15, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (858, 153, 2, N'')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (859, 153, 3, N'Autocannon')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (860, 153, 4, N'7')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (861, 153, 5, N'6')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (862, 153, 6, N'5')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (863, 153, 7, N'3')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (864, 153, 8, N'3')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (865, 153, 9, N'3')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (866, 153, 10, N'5')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (867, 153, 11, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (868, 153, 12, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (869, 153, 13, N'FALSE')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (870, 153, 14, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (871, 153, 15, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (872, 154, 2, N'')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (873, 154, 3, N'Autocannon')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (874, 154, 4, N'6')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (875, 154, 5, N'6')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (876, 154, 6, N'4')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (877, 154, 7, N'4')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (878, 154, 8, N'2')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (879, 154, 9, N'4')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (880, 154, 10, N'5')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (881, 154, 11, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (882, 154, 12, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (883, 154, 13, N'FALSE')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (884, 154, 14, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (885, 154, 15, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (886, 155, 2, N'')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (887, 155, 3, N'Autocannon')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (888, 155, 4, N'7')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (889, 155, 5, N'6')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (890, 155, 6, N'7')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (891, 155, 7, N'2')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (892, 155, 8, N'4')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (893, 155, 9, N'5')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (894, 155, 10, N'5')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (895, 155, 11, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (896, 155, 12, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (897, 155, 13, N'FALSE')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (898, 155, 14, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (899, 155, 15, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (900, 156, 2, N'')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (901, 156, 3, N'Autocannon')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (902, 156, 4, N'6')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (903, 156, 5, N'6')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (904, 156, 6, N'3')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (905, 156, 7, N'3')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (906, 156, 8, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (907, 156, 9, N'3')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (908, 156, 10, N'5')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (909, 156, 11, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (910, 156, 12, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (911, 156, 13, N'FALSE')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (912, 156, 14, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (913, 156, 15, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (914, 157, 2, N'')
GO
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (915, 157, 3, N'Autocannon')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (916, 157, 4, N'7')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (917, 157, 5, N'6')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (918, 157, 6, N'5')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (919, 157, 7, N'3')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (920, 157, 8, N'2')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (921, 157, 9, N'3')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (922, 157, 10, N'5')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (923, 157, 11, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (924, 157, 12, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (925, 157, 13, N'FALSE')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (926, 157, 14, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (927, 157, 15, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (928, 158, 2, N'')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (929, 158, 3, N'Autocannon')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (930, 158, 4, N'7')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (931, 158, 5, N'6')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (932, 158, 6, N'6')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (933, 158, 7, N'3')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (934, 158, 8, N'1')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (935, 158, 9, N'4')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (936, 158, 10, N'5')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (937, 158, 11, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (938, 158, 12, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (939, 158, 13, N'FALSE')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (940, 158, 14, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (941, 158, 15, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (942, 159, 2, N'')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (943, 159, 3, N'Autocannon')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (944, 159, 4, N'7')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (945, 159, 5, N'6')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (946, 159, 6, N'9')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (947, 159, 7, N'3')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (948, 159, 8, N'1')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (949, 159, 9, N'4')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (950, 159, 10, N'5')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (951, 159, 11, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (952, 159, 12, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (953, 159, 13, N'FALSE')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (954, 159, 14, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (955, 159, 15, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (956, 160, 2, N'')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (957, 160, 3, N'Autocannon')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (958, 160, 4, N'7')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (959, 160, 5, N'5')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (960, 160, 6, N'2')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (961, 160, 7, N'3')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (962, 160, 8, N'3')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (963, 160, 9, N'3')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (964, 160, 10, N'5')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (965, 160, 11, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (966, 160, 12, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (967, 160, 13, N'FALSE')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (968, 160, 14, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (969, 160, 15, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (970, 161, 2, N'')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (971, 161, 3, N'Autocannon')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (972, 161, 4, N'7')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (973, 161, 5, N'7')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (974, 161, 6, N'4')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (975, 161, 7, N'3')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (976, 161, 8, N'2')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (977, 161, 9, N'3')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (978, 161, 10, N'5')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (979, 161, 11, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (980, 161, 12, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (981, 161, 13, N'FALSE')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (982, 161, 14, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (983, 161, 15, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (984, 162, 2, N'')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (985, 162, 3, N'Autocannon')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (986, 162, 4, N'8')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (987, 162, 5, N'6')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (988, 162, 6, N'4')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (989, 162, 7, N'3')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (990, 162, 8, N'1')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (991, 162, 9, N'2')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (992, 162, 10, N'5')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (993, 162, 11, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (994, 162, 12, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (995, 162, 13, N'FALSE')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (996, 162, 14, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (997, 162, 15, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (998, 163, 2, N'')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (999, 163, 3, N'Autocannon')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1000, 163, 4, N'7')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1001, 163, 5, N'6')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1002, 163, 6, N'4')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1003, 163, 7, N'3')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1004, 163, 8, N'1')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1005, 163, 9, N'2')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1006, 163, 10, N'5')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1007, 163, 11, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1008, 163, 12, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1009, 163, 13, N'FALSE')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1010, 163, 14, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1011, 163, 15, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1012, 164, 2, N'')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1013, 164, 3, N'Autocannon')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1014, 164, 4, N'7')
GO
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1015, 164, 5, N'6')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1016, 164, 6, N'4')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1017, 164, 7, N'3')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1018, 164, 8, N'1')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1019, 164, 9, N'2')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1020, 164, 10, N'5')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1021, 164, 11, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1022, 164, 12, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1023, 164, 13, N'FALSE')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1024, 164, 14, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1025, 164, 15, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1026, 165, 2, N'')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1027, 165, 3, N'Autocannon')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1028, 165, 4, N'7')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1029, 165, 5, N'6')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1030, 165, 6, N'4')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1031, 165, 7, N'2')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1032, 165, 8, N'1')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1033, 165, 9, N'4')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1034, 165, 10, N'5')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1035, 165, 11, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1036, 165, 12, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1037, 165, 13, N'FALSE')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1038, 165, 14, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1039, 165, 15, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1040, 166, 2, N'')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1041, 166, 3, N'Autocannon')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1042, 166, 4, N'7')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1043, 166, 5, N'6')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1044, 166, 6, N'6')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1045, 166, 7, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1046, 166, 8, N'4')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1047, 166, 9, N'4')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1048, 166, 10, N'5')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1049, 166, 11, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1050, 166, 12, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1051, 166, 13, N'FALSE')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1052, 166, 14, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1053, 166, 15, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1054, 167, 2, N'')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1055, 167, 3, N'Autocannon')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1056, 167, 4, N'7')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1057, 167, 5, N'6')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1058, 167, 6, N'4')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1059, 167, 7, N'3')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1060, 167, 8, N'2')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1061, 167, 9, N'3')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1062, 167, 10, N'5')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1063, 167, 11, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1064, 167, 12, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1065, 167, 13, N'FALSE')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1066, 167, 14, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1067, 167, 15, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1068, 168, 2, N'')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1069, 168, 3, N'Autocannon')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1070, 168, 4, N'7')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1071, 168, 5, N'6')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1072, 168, 6, N'5')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1073, 168, 7, N'3')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1074, 168, 8, N'2')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1075, 168, 9, N'3')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1076, 168, 10, N'5')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1077, 168, 11, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1078, 168, 12, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1079, 168, 13, N'FALSE')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1080, 168, 14, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1081, 168, 15, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1082, 169, 2, N'')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1083, 169, 3, N'Autocannon')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1084, 169, 4, N'7')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1085, 169, 5, N'6')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1086, 169, 6, N'5')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1087, 169, 7, N'3')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1088, 169, 8, N'2')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1089, 169, 9, N'4')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1090, 169, 10, N'5')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1091, 169, 11, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1092, 169, 12, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1093, 169, 13, N'FALSE')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1094, 169, 14, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1095, 169, 15, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1096, 170, 2, N'')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1097, 170, 3, N'Autocannon')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1098, 170, 4, N'7')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1099, 170, 5, N'6')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1100, 170, 6, N'4')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1101, 170, 7, N'3')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1102, 170, 8, N'3')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1103, 170, 9, N'3')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1104, 170, 10, N'5')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1105, 170, 11, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1106, 170, 12, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1107, 170, 13, N'FALSE')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1108, 170, 14, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1109, 170, 15, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1110, 171, 2, N'')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1111, 171, 3, N'Battery')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1112, 171, 4, N'7')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1113, 171, 5, N'6')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1114, 171, 6, N'4')
GO
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1115, 171, 7, N'3')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1116, 171, 8, N'3')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1117, 171, 9, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1118, 171, 10, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1119, 171, 11, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1120, 171, 12, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1121, 171, 13, N'FALSE')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1122, 171, 14, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1123, 171, 15, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1124, 172, 2, N'')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1125, 172, 3, N'Battery')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1126, 172, 4, N'7')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1127, 172, 5, N'6')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1128, 172, 6, N'5')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1129, 172, 7, N'4')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1130, 172, 8, N'3')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1131, 172, 9, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1132, 172, 10, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1133, 172, 11, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1134, 172, 12, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1135, 172, 13, N'FALSE')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1136, 172, 14, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1137, 172, 15, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1138, 173, 2, N'')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1139, 173, 3, N'Battery')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1140, 173, 4, N'8')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1141, 173, 5, N'7')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1142, 173, 6, N'4')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1143, 173, 7, N'2')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1144, 173, 8, N'3')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1145, 173, 9, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1146, 173, 10, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1147, 173, 11, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1148, 173, 12, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1149, 173, 13, N'FALSE')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1150, 173, 14, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1151, 173, 15, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1152, 174, 2, N'')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1153, 174, 3, N'Battery')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1154, 174, 4, N'7')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1155, 174, 5, N'6')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1156, 174, 6, N'6')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1157, 174, 7, N'3')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1158, 174, 8, N'4')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1159, 174, 9, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1160, 174, 10, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1161, 174, 11, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1162, 174, 12, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1163, 174, 13, N'FALSE')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1164, 174, 14, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1165, 174, 15, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1166, 175, 2, N'')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1167, 175, 3, N'Battery')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1168, 175, 4, N'6')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1169, 175, 5, N'6')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1170, 175, 6, N'6')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1171, 175, 7, N'5')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1172, 175, 8, N'3')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1173, 175, 9, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1174, 175, 10, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1175, 175, 11, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1176, 175, 12, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1177, 175, 13, N'FALSE')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1178, 175, 14, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1179, 175, 15, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1180, 176, 2, N'')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1181, 176, 3, N'Battery')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1182, 176, 4, N'8')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1183, 176, 5, N'6')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1184, 176, 6, N'10')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1185, 176, 7, N'2')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1186, 176, 8, N'6')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1187, 176, 9, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1188, 176, 10, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1189, 176, 11, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1190, 176, 12, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1191, 176, 13, N'FALSE')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1192, 176, 14, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1193, 176, 15, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1194, 177, 2, N'')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1195, 177, 3, N'Battery')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1196, 177, 4, N'6')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1197, 177, 5, N'6')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1198, 177, 6, N'3')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1199, 177, 7, N'3')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1200, 177, 8, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1201, 177, 9, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1202, 177, 10, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1203, 177, 11, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1204, 177, 12, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1205, 177, 13, N'FALSE')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1206, 177, 14, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1207, 177, 15, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1208, 178, 2, N'Twin-Linked')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1209, 178, 3, N'Turret')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1210, 178, 4, N'8')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1211, 178, 5, N'7')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1212, 178, 6, N'3')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1213, 178, 7, N'3')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1214, 178, 8, N'1')
GO
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1215, 178, 9, N'1')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1216, 178, 10, N'3')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1217, 178, 11, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1218, 178, 12, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1219, 178, 13, N'FALSE')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1220, 178, 14, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1221, 178, 15, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1222, 179, 2, N'')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1223, 179, 3, N'Reactor')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1224, 179, 4, N'9')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1225, 179, 5, N'9')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1226, 179, 6, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1227, 179, 7, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1228, 179, 8, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1229, 179, 9, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1230, 179, 10, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1231, 179, 11, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1232, 179, 12, N'14')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1233, 179, 13, N'FALSE')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1234, 179, 14, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1235, 179, 15, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1236, 180, 2, N'')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1237, 180, 3, N'Turret')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1238, 180, 4, N'8')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1239, 180, 5, N'7')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1240, 180, 6, N'2')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1241, 180, 7, N'4')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1242, 180, 8, N'1')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1243, 180, 9, N'1')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1244, 180, 10, N'4')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1245, 180, 11, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1246, 180, 12, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1247, 180, 13, N'FALSE')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1248, 180, 14, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1249, 180, 15, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1250, 181, 2, N'')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1251, 181, 3, N'Turret')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1252, 181, 4, N'8')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1253, 181, 5, N'7')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1254, 181, 6, N'1')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1255, 181, 7, N'3')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1256, 181, 8, N'1')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1257, 181, 9, N'1')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1258, 181, 10, N'3')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1259, 181, 11, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1260, 181, 12, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1261, 181, 13, N'FALSE')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1262, 181, 14, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1263, 181, 15, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1264, 182, 2, N'')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1265, 182, 3, N'Shield Generator')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1266, 182, 4, N'8')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1267, 182, 5, N'8')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1268, 182, 6, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1269, 182, 7, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1270, 182, 8, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1271, 182, 9, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1272, 182, 10, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1273, 182, 11, N'2')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1274, 182, 12, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1275, 182, 13, N'FALSE')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1276, 182, 14, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1277, 182, 15, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1278, 183, 2, N'Plasma Weapons')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1279, 183, 3, N'Turret')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1280, 183, 4, N'8')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1281, 183, 5, N'7')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1282, 183, 6, N'5')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1283, 183, 7, N'2')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1284, 183, 8, N'2')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1285, 183, 9, N'1')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1286, 183, 10, N'2')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1287, 183, 11, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1288, 183, 12, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1289, 183, 13, N'FALSE')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1290, 183, 14, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1291, 183, 15, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1292, 184, 2, N'')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1293, 184, 3, N'Autocannon')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1294, 184, 4, N'7')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1295, 184, 5, N'6')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1296, 184, 6, N'3')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1297, 184, 7, N'3')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1298, 184, 8, N'2')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1299, 184, 9, N'3')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1300, 184, 10, N'7')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1301, 184, 11, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1302, 184, 12, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1303, 184, 13, N'FALSE')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1304, 184, 14, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1305, 184, 15, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1306, 185, 2, N'')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1307, 185, 3, N'Shield Booster')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1308, 185, 4, N'7')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1309, 185, 5, N'6')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1310, 185, 6, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1311, 185, 7, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1312, 185, 8, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1313, 185, 9, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1314, 185, 10, N'0')
GO
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1315, 185, 11, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1316, 185, 12, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1317, 185, 13, N'TRUE')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1318, 185, 14, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1319, 185, 15, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1320, 186, 2, N'')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1321, 186, 3, N'Battery')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1322, 186, 4, N'7')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1323, 186, 5, N'6')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1324, 186, 6, N'5')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1325, 186, 7, N'999')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1326, 186, 8, N'1')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1327, 186, 9, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1328, 186, 10, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1329, 186, 11, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1330, 186, 12, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1331, 186, 13, N'FALSE')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1332, 186, 14, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1333, 186, 15, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1334, 187, 2, N'')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1335, 187, 3, N'Battery')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1336, 187, 4, N'7')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1337, 187, 5, N'6')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1338, 187, 6, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1339, 187, 7, N'3')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1340, 187, 8, N'3')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1341, 187, 9, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1342, 187, 10, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1343, 187, 11, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1344, 187, 12, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1345, 187, 13, N'FALSE')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1346, 187, 14, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1347, 187, 15, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1348, 188, 2, N'')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1349, 188, 3, N'Reactor Booster')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1350, 188, 4, N'7')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1351, 188, 5, N'6')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1352, 188, 6, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1353, 188, 7, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1354, 188, 8, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1355, 188, 9, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1356, 188, 10, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1357, 188, 11, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1358, 188, 12, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1359, 188, 13, N'TRUE')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1360, 188, 14, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1361, 188, 15, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1362, 189, 2, N'World Killer')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1363, 189, 3, N'Battery')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1364, 189, 4, N'8')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1365, 189, 5, N'6')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1366, 189, 6, N'10')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1367, 189, 7, N'1')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1368, 189, 8, N'1')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1369, 189, 9, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1370, 189, 10, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1371, 189, 11, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1372, 189, 12, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1373, 189, 13, N'FALSE')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1374, 189, 14, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1375, 189, 15, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1376, 190, 2, N'Plasma Weapon')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1377, 190, 3, N'Battery')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1378, 190, 4, N'7')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1379, 190, 5, N'6')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1380, 190, 6, N'6')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1381, 190, 7, N'3')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1382, 190, 8, N'3')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1383, 190, 9, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1384, 190, 10, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1385, 190, 11, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1386, 190, 12, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1387, 190, 13, N'FALSE')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1388, 190, 14, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1389, 190, 15, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1390, 191, 2, N'')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1391, 191, 3, N'Ordnance Rack')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1392, 191, 4, N'7')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1393, 191, 5, N'7')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1394, 191, 6, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1395, 191, 7, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1396, 191, 8, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1397, 191, 9, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1398, 191, 10, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1399, 191, 11, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1400, 191, 12, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1401, 191, 13, N'FALSE')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1402, 191, 14, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1403, 191, 15, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1404, 192, 2, N'')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1405, 192, 3, N'Battery')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1406, 192, 4, N'7')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1407, 192, 5, N'6')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1408, 192, 6, N'6')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1409, 192, 7, N'3')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1410, 192, 8, N'4')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1411, 192, 9, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1412, 192, 10, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1413, 192, 11, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1414, 192, 12, N'0')
GO
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1415, 192, 13, N'FALSE')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1416, 192, 14, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1417, 192, 15, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1418, 193, 2, N'')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1419, 193, 3, N'Launch Bay')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1420, 193, 4, N'7')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1421, 193, 5, N'7')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1422, 193, 6, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1423, 193, 7, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1424, 193, 8, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1425, 193, 9, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1426, 193, 10, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1427, 193, 11, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1428, 193, 12, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1429, 193, 13, N'FALSE')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1430, 193, 14, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1431, 193, 15, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1432, 194, 2, N'')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1433, 194, 3, N'Fighter')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1434, 194, 4, N'6')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1435, 194, 5, N'1')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1436, 194, 6, N'1')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1437, 194, 7, N'2')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1438, 194, 8, N'1')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1439, 194, 9, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1440, 194, 10, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1441, 194, 11, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1442, 194, 12, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1443, 194, 13, N'FALSE')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1444, 194, 14, N'1')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1445, 194, 15, N'3')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1446, 195, 2, N'Sensor Ghost')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1447, 195, 3, N'Fighter')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1448, 195, 4, N'6')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1449, 195, 5, N'1')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1450, 195, 6, N'3')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1451, 195, 7, N'2')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1452, 195, 8, N'1')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1453, 195, 9, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1454, 195, 10, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1455, 195, 11, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1456, 195, 12, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1457, 195, 13, N'FALSE')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1458, 195, 14, N'1')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1459, 195, 15, N'3')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1460, 196, 2, N'Hunters')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1461, 196, 3, N'Fighter')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1462, 196, 4, N'6')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1463, 196, 5, N'1')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1464, 196, 6, N'3')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1465, 196, 7, N'2')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1466, 196, 8, N'1')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1467, 196, 9, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1468, 196, 10, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1469, 196, 11, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1470, 196, 12, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1471, 196, 13, N'FALSE')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1472, 196, 14, N'1')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1473, 196, 15, N'3')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1474, 197, 2, N'Drones')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1475, 197, 3, N'Fighter')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1476, 197, 4, N'6')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1477, 197, 5, N'1')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1478, 197, 6, N'4')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1479, 197, 7, N'1')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1480, 197, 8, N'1')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1481, 197, 9, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1482, 197, 10, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1483, 197, 11, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1484, 197, 12, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1485, 197, 13, N'FALSE')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1486, 197, 14, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1487, 197, 15, N'3')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1488, 198, 2, N'Personal Shields')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1489, 198, 3, N'Fighter')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1490, 198, 4, N'6')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1491, 198, 5, N'1')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1492, 198, 6, N'3')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1493, 198, 7, N'2')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1494, 198, 8, N'1')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1495, 198, 9, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1496, 198, 10, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1497, 198, 11, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1498, 198, 12, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1499, 198, 13, N'FALSE')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1500, 198, 14, N'1')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1501, 198, 15, N'3')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1502, 199, 2, N'Elite and Irreplaceable')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1503, 199, 3, N'Fighter')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1504, 199, 4, N'7')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1505, 199, 5, N'3')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1506, 199, 6, N'4')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1507, 199, 7, N'3')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1508, 199, 8, N'2')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1509, 199, 9, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1510, 199, 10, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1511, 199, 11, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1512, 199, 12, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1513, 199, 13, N'FALSE')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1514, 199, 14, N'1')
GO
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1515, 199, 15, N'4')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1516, 200, 2, N'')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1517, 200, 3, N'Bomber')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1518, 200, 4, N'7')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1519, 200, 5, N'2')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1520, 200, 6, N'1')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1521, 200, 7, N'2')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1522, 200, 8, N'1')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1523, 200, 9, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1524, 200, 10, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1525, 200, 11, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1526, 200, 12, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1527, 200, 13, N'FALSE')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1528, 200, 14, N'1')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1529, 200, 15, N'2')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1530, 201, 2, N'')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1531, 201, 3, N'Bomber')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1532, 201, 4, N'7')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1533, 201, 5, N'2')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1534, 201, 6, N'2')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1535, 201, 7, N'2')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1536, 201, 8, N'2')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1537, 201, 9, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1538, 201, 10, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1539, 201, 11, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1540, 201, 12, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1541, 201, 13, N'FALSE')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1542, 201, 14, N'1')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1543, 201, 15, N'2')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1544, 202, 2, N'Plasma Weapons')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1545, 202, 3, N'Bomber')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1546, 202, 4, N'6')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1547, 202, 5, N'2')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1548, 202, 6, N'4')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1549, 202, 7, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1550, 202, 8, N'2')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1551, 202, 9, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1552, 202, 10, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1553, 202, 11, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1554, 202, 12, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1555, 202, 13, N'FALSE')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1556, 202, 14, N'1')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1557, 202, 15, N'2')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1558, 203, 2, N'Drones')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1559, 203, 3, N'Bomber')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1560, 203, 4, N'7')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1561, 203, 5, N'2')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1562, 203, 6, N'4')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1563, 203, 7, N'1')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1564, 203, 8, N'1')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1565, 203, 9, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1566, 203, 10, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1567, 203, 11, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1568, 203, 12, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1569, 203, 13, N'FALSE')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1570, 203, 14, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1571, 203, 15, N'2')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1572, 204, 2, N'')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1573, 204, 3, N'Assault Craft')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1574, 204, 4, N'7')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1575, 204, 5, N'2')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1576, 204, 6, N'1')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1577, 204, 7, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1578, 204, 8, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1579, 204, 9, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1580, 204, 10, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1581, 204, 11, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1582, 204, 12, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1583, 204, 13, N'FALSE')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1584, 204, 14, N'1')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1585, 204, 15, N'2')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1586, 205, 2, N'Sensor Ghost')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1587, 205, 3, N'Assault Craft')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1588, 205, 4, N'6')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1589, 205, 5, N'2')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1590, 205, 6, N'3')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1591, 205, 7, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1592, 205, 8, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1593, 205, 9, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1594, 205, 10, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1595, 205, 11, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1596, 205, 12, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1597, 205, 13, N'FALSE')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1598, 205, 14, N'1')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1599, 205, 15, N'2')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1600, 206, 2, N'Hardened Veterens')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1601, 206, 3, N'Assault Craft')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1602, 206, 4, N'6')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1603, 206, 5, N'2')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1604, 206, 6, N'2')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1605, 206, 7, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1606, 206, 8, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1607, 206, 9, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1608, 206, 10, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1609, 206, 11, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1610, 206, 12, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1611, 206, 13, N'FALSE')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1612, 206, 14, N'1')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1613, 206, 15, N'2')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1614, 207, 2, N'Nanite Gas')
GO
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1615, 207, 3, N'Assault Craft')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1616, 207, 4, N'6')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1617, 207, 5, N'2')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1618, 207, 6, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1619, 207, 7, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1620, 207, 8, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1621, 207, 9, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1622, 207, 10, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1623, 207, 11, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1624, 207, 12, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1625, 207, 13, N'FALSE')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1626, 207, 14, N'1')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1627, 207, 15, N'2')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1628, 208, 2, N'Tunneling')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1629, 208, 3, N'Assault Craft')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1630, 208, 4, N'6')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1631, 208, 5, N'2')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1632, 208, 6, N'2')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1633, 208, 7, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1634, 208, 8, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1635, 208, 9, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1636, 208, 10, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1637, 208, 11, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1638, 208, 12, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1639, 208, 13, N'FALSE')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1640, 208, 14, N'1')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1641, 208, 15, N'2')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1642, 209, 2, N'Data Thieves')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1643, 209, 3, N'Assault Craft')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1644, 209, 4, N'6')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1645, 209, 5, N'2')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1646, 209, 6, N'2')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1647, 209, 7, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1648, 209, 8, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1649, 209, 9, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1650, 209, 10, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1651, 209, 11, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1652, 209, 12, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1653, 209, 13, N'FALSE')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1654, 209, 14, N'1')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1655, 209, 15, N'2')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1656, 210, 2, N'Personal Shields')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1657, 210, 3, N'Bomber')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1658, 210, 4, N'7')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1659, 210, 5, N'2')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1660, 210, 6, N'3')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1661, 210, 7, N'2')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1662, 210, 8, N'1')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1663, 210, 9, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1664, 210, 10, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1665, 210, 11, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1666, 210, 12, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1667, 210, 13, N'FALSE')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1668, 210, 14, N'1')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1669, 210, 15, N'2')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1670, 211, 2, N'')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1671, 211, 3, N'Torpedoes')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1672, 211, 4, N'8')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1673, 211, 5, N'2')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1674, 211, 6, N'4')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1675, 211, 7, N'5')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1676, 211, 8, N'2')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1677, 211, 9, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1678, 211, 10, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1679, 211, 11, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1680, 211, 12, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1681, 211, 13, N'FALSE')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1682, 211, 14, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1683, 211, 15, N'3')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1684, 212, 2, N'Sensor Ghost')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1685, 212, 3, N'Torpedoes')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1686, 212, 4, N'8')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1687, 212, 5, N'1')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1688, 212, 6, N'6')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1689, 212, 7, N'5')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1690, 212, 8, N'2')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1691, 212, 9, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1692, 212, 10, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1693, 212, 11, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1694, 212, 12, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1695, 212, 13, N'FALSE')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1696, 212, 14, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1697, 212, 15, N'3')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1698, 213, 2, N'Tunneling')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1699, 213, 3, N'Torpedoes')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1700, 213, 4, N'8')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1701, 213, 5, N'2')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1702, 213, 6, N'6')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1703, 213, 7, N'4')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1704, 213, 8, N'2')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1705, 213, 9, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1706, 213, 10, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1707, 213, 11, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1708, 213, 12, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1709, 213, 13, N'FALSE')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1710, 213, 14, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1711, 213, 15, N'3')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1712, 214, 2, N'Boarding Torpedo')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1713, 214, 3, N'Torpedoes')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1714, 214, 4, N'8')
GO
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1715, 214, 5, N'2')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1716, 214, 6, N'3')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1717, 214, 7, N'4')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1718, 214, 8, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1719, 214, 9, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1720, 214, 10, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1721, 214, 11, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1722, 214, 12, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1723, 214, 13, N'FALSE')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1724, 214, 14, N'1')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1725, 214, 15, N'3')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1726, 215, 2, N'')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1727, 215, 3, N'Missiles')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1728, 215, 4, N'6')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1729, 215, 5, N'1')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1730, 215, 6, N'2')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1731, 215, 7, N'3')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1732, 215, 8, N'2')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1733, 215, 9, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1734, 215, 10, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1735, 215, 11, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1736, 215, 12, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1737, 215, 13, N'FALSE')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1738, 215, 14, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1739, 215, 15, N'3')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1740, 216, 2, N'')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1741, 216, 3, N'Missiles')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1742, 216, 4, N'8')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1743, 216, 5, N'1')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1744, 216, 6, N'3')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1745, 216, 7, N'3')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1746, 216, 8, N'2')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1747, 216, 9, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1748, 216, 10, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1749, 216, 11, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1750, 216, 12, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1751, 216, 13, N'FALSE')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1752, 216, 14, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1753, 216, 15, N'3')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1754, 217, 2, N'')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1755, 217, 3, N'Misfortune')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1756, 217, 4, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1757, 217, 5, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1758, 217, 6, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1759, 217, 7, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1760, 217, 8, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1761, 217, 9, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1762, 217, 10, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1763, 217, 11, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1764, 217, 12, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1765, 217, 13, N'FALSE')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1766, 217, 14, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1767, 217, 15, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1768, 218, 2, N'')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1769, 218, 3, N'Event')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1770, 218, 4, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1771, 218, 5, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1772, 218, 6, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1773, 218, 7, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1774, 218, 8, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1775, 218, 9, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1776, 218, 10, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1777, 218, 11, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1778, 218, 12, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1779, 218, 13, N'FALSE')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1780, 218, 14, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1781, 218, 15, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1782, 219, 2, N'')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1783, 219, 3, N'Orders')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1784, 219, 4, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1785, 219, 5, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1786, 219, 6, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1787, 219, 7, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1788, 219, 8, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1789, 219, 9, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1790, 219, 10, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1791, 219, 11, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1792, 219, 12, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1793, 219, 13, N'FALSE')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1794, 219, 14, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1795, 219, 15, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1796, 220, 2, N'')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1797, 220, 3, N'Misfortune')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1798, 220, 4, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1799, 220, 5, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1800, 220, 6, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1801, 220, 7, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1802, 220, 8, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1803, 220, 9, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1804, 220, 10, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1805, 220, 11, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1806, 220, 12, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1807, 220, 13, N'FALSE')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1808, 220, 14, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1809, 220, 15, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1810, 221, 2, N'')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1811, 221, 3, N'Misfortune')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1812, 221, 4, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1813, 221, 5, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1814, 221, 6, N'0')
GO
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1815, 221, 7, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1816, 221, 8, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1817, 221, 9, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1818, 221, 10, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1819, 221, 11, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1820, 221, 12, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1821, 221, 13, N'FALSE')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1822, 221, 14, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1823, 221, 15, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1824, 222, 2, N'')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1825, 222, 3, N'Misfortune')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1826, 222, 4, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1827, 222, 5, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1828, 222, 6, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1829, 222, 7, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1830, 222, 8, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1831, 222, 9, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1832, 222, 10, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1833, 222, 11, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1834, 222, 12, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1835, 222, 13, N'FALSE')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1836, 222, 14, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1837, 222, 15, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1838, 223, 2, N'')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1839, 223, 3, N'Event')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1840, 223, 4, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1841, 223, 5, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1842, 223, 6, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1843, 223, 7, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1844, 223, 8, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1845, 223, 9, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1846, 223, 10, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1847, 223, 11, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1848, 223, 12, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1849, 223, 13, N'FALSE')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1850, 223, 14, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1851, 223, 15, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1852, 224, 2, N'')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1853, 224, 3, N'Event')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1854, 224, 4, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1855, 224, 5, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1856, 224, 6, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1857, 224, 7, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1858, 224, 8, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1859, 224, 9, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1860, 224, 10, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1861, 224, 11, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1862, 224, 12, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1863, 224, 13, N'FALSE')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1864, 224, 14, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1865, 224, 15, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1866, 225, 2, N'')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1867, 225, 3, N'Event')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1868, 225, 4, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1869, 225, 5, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1870, 225, 6, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1871, 225, 7, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1872, 225, 8, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1873, 225, 9, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1874, 225, 10, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1875, 225, 11, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1876, 225, 12, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1877, 225, 13, N'FALSE')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1878, 225, 14, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1879, 225, 15, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1880, 226, 2, N'')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1881, 226, 3, N'Event')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1882, 226, 4, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1883, 226, 5, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1884, 226, 6, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1885, 226, 7, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1886, 226, 8, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1887, 226, 9, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1888, 226, 10, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1889, 226, 11, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1890, 226, 12, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1891, 226, 13, N'FALSE')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1892, 226, 14, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1893, 226, 15, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1894, 227, 2, N'')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1895, 227, 3, N'Event')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1896, 227, 4, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1897, 227, 5, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1898, 227, 6, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1899, 227, 7, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1900, 227, 8, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1901, 227, 9, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1902, 227, 10, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1903, 227, 11, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1904, 227, 12, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1905, 227, 13, N'FALSE')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1906, 227, 14, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1907, 227, 15, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1908, 228, 2, N'')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1909, 228, 3, N'Event')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1910, 228, 4, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1911, 228, 5, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1912, 228, 6, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1913, 228, 7, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1914, 228, 8, N'0')
GO
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1915, 228, 9, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1916, 228, 10, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1917, 228, 11, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1918, 228, 12, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1919, 228, 13, N'FALSE')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1920, 228, 14, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1921, 228, 15, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1922, 229, 2, N'')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1923, 229, 3, N'Orders')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1924, 229, 4, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1925, 229, 5, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1926, 229, 6, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1927, 229, 7, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1928, 229, 8, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1929, 229, 9, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1930, 229, 10, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1931, 229, 11, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1932, 229, 12, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1933, 229, 13, N'FALSE')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1934, 229, 14, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1935, 229, 15, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1936, 230, 2, N'')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1937, 230, 3, N'Orders')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1938, 230, 4, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1939, 230, 5, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1940, 230, 6, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1941, 230, 7, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1942, 230, 8, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1943, 230, 9, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1944, 230, 10, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1945, 230, 11, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1946, 230, 12, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1947, 230, 13, N'FALSE')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1948, 230, 14, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1949, 230, 15, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1950, 231, 2, N'')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1951, 231, 3, N'Orders')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1952, 231, 4, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1953, 231, 5, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1954, 231, 6, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1955, 231, 7, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1956, 231, 8, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1957, 231, 9, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1958, 231, 10, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1959, 231, 11, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1960, 231, 12, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1961, 231, 13, N'FALSE')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1962, 231, 14, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1963, 231, 15, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1964, 232, 2, N'')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1965, 232, 3, N'Orders')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1966, 232, 4, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1967, 232, 5, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1968, 232, 6, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1969, 232, 7, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1970, 232, 8, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1971, 232, 9, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1972, 232, 10, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1973, 232, 11, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1974, 232, 12, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1975, 232, 13, N'FALSE')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1976, 232, 14, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1977, 232, 15, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1978, 233, 2, N'')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1979, 233, 3, N'Orders')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1980, 233, 4, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1981, 233, 5, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1982, 233, 6, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1983, 233, 7, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1984, 233, 8, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1985, 233, 9, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1986, 233, 10, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1987, 233, 11, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1988, 233, 12, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1989, 233, 13, N'FALSE')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1990, 233, 14, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (1991, 233, 15, N'0')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (2011, 239, 3, N'Ship Classification')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (2012, 239, 22, N'The Ship of the Line for many of the old empires.  This design has stood the test of time.')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (2013, 239, 20, N'6')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (2014, 239, 21, N'3')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (2015, 240, 3, N'Specialist')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (2016, 240, 22, N'Booming voices and booming guns insure that your orders obeyed quickly and without question')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (2017, 241, 3, N'Specialist')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (2018, 241, 22, N'Many powerful families are willing to pay a captain unimaginable fotrunes to put one of their heirs onboard a ship')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (2021, 242, 23, N'A')
INSERT [dbo].[egs_gamePieceAttribute] ([idGamePieceAttribute], [idGamePiece], [idGamePieceAttributeType], [value]) VALUES (2022, 242, 25, N'1')
SET IDENTITY_INSERT [dbo].[egs_gamePieceAttribute] OFF
SET IDENTITY_INSERT [dbo].[egs_gamePieceAttributeType] ON 

INSERT [dbo].[egs_gamePieceAttributeType] ([idGamePieceAttributeType], [idGame], [pieceAttributeType]) VALUES (2, 1, N'subTitle')
INSERT [dbo].[egs_gamePieceAttributeType] ([idGamePieceAttributeType], [idGame], [pieceAttributeType]) VALUES (3, 1, N'cardClass')
INSERT [dbo].[egs_gamePieceAttributeType] ([idGamePieceAttributeType], [idGame], [pieceAttributeType]) VALUES (4, 1, N'armor')
INSERT [dbo].[egs_gamePieceAttributeType] ([idGamePieceAttributeType], [idGame], [pieceAttributeType]) VALUES (5, 1, N'structure')
INSERT [dbo].[egs_gamePieceAttributeType] ([idGamePieceAttributeType], [idGame], [pieceAttributeType]) VALUES (6, 1, N'power')
INSERT [dbo].[egs_gamePieceAttributeType] ([idGamePieceAttributeType], [idGame], [pieceAttributeType]) VALUES (7, 1, N'accuracy')
INSERT [dbo].[egs_gamePieceAttributeType] ([idGamePieceAttributeType], [idGame], [pieceAttributeType]) VALUES (8, 1, N'damage')
INSERT [dbo].[egs_gamePieceAttributeType] ([idGamePieceAttributeType], [idGame], [pieceAttributeType]) VALUES (9, 1, N'rangeMin')
INSERT [dbo].[egs_gamePieceAttributeType] ([idGamePieceAttributeType], [idGame], [pieceAttributeType]) VALUES (10, 1, N'rangeMax')
INSERT [dbo].[egs_gamePieceAttributeType] ([idGamePieceAttributeType], [idGame], [pieceAttributeType]) VALUES (11, 1, N'shield')
INSERT [dbo].[egs_gamePieceAttributeType] ([idGamePieceAttributeType], [idGame], [pieceAttributeType]) VALUES (12, 1, N'rest')
INSERT [dbo].[egs_gamePieceAttributeType] ([idGamePieceAttributeType], [idGame], [pieceAttributeType]) VALUES (13, 1, N'special')
INSERT [dbo].[egs_gamePieceAttributeType] ([idGamePieceAttributeType], [idGame], [pieceAttributeType]) VALUES (14, 1, N'crewCost')
INSERT [dbo].[egs_gamePieceAttributeType] ([idGamePieceAttributeType], [idGame], [pieceAttributeType]) VALUES (15, 1, N'speed')
INSERT [dbo].[egs_gamePieceAttributeType] ([idGamePieceAttributeType], [idGame], [pieceAttributeType]) VALUES (16, 1, N'smallImg')
INSERT [dbo].[egs_gamePieceAttributeType] ([idGamePieceAttributeType], [idGame], [pieceAttributeType]) VALUES (17, 1, N'largeImg')
INSERT [dbo].[egs_gamePieceAttributeType] ([idGamePieceAttributeType], [idGame], [pieceAttributeType]) VALUES (18, 1, N'minImg')
INSERT [dbo].[egs_gamePieceAttributeType] ([idGamePieceAttributeType], [idGame], [pieceAttributeType]) VALUES (20, 1, N'leadership')
INSERT [dbo].[egs_gamePieceAttributeType] ([idGamePieceAttributeType], [idGame], [pieceAttributeType]) VALUES (21, 1, N'fleetCommand')
INSERT [dbo].[egs_gamePieceAttributeType] ([idGamePieceAttributeType], [idGame], [pieceAttributeType]) VALUES (22, 1, N'flavorText')
INSERT [dbo].[egs_gamePieceAttributeType] ([idGamePieceAttributeType], [idGame], [pieceAttributeType]) VALUES (23, 1, N'row')
INSERT [dbo].[egs_gamePieceAttributeType] ([idGamePieceAttributeType], [idGame], [pieceAttributeType]) VALUES (25, 1, N'col')
SET IDENTITY_INSERT [dbo].[egs_gamePieceAttributeType] OFF
SET IDENTITY_INSERT [dbo].[egs_gamePieceAttrubuteTypeLanguage] ON 

INSERT [dbo].[egs_gamePieceAttrubuteTypeLanguage] ([idGamePieceAttributeLanguage], [idGamePieceAttributeType], [idLanguage], [name]) VALUES (1, 2, 1, N'Subtitle')
INSERT [dbo].[egs_gamePieceAttrubuteTypeLanguage] ([idGamePieceAttributeLanguage], [idGamePieceAttributeType], [idLanguage], [name]) VALUES (2, 3, 1, N'Card Class')
INSERT [dbo].[egs_gamePieceAttrubuteTypeLanguage] ([idGamePieceAttributeLanguage], [idGamePieceAttributeType], [idLanguage], [name]) VALUES (3, 4, 1, N'Armor')
INSERT [dbo].[egs_gamePieceAttrubuteTypeLanguage] ([idGamePieceAttributeLanguage], [idGamePieceAttributeType], [idLanguage], [name]) VALUES (4, 5, 1, N'Structure')
INSERT [dbo].[egs_gamePieceAttrubuteTypeLanguage] ([idGamePieceAttributeLanguage], [idGamePieceAttributeType], [idLanguage], [name]) VALUES (5, 6, 1, N'Power')
INSERT [dbo].[egs_gamePieceAttrubuteTypeLanguage] ([idGamePieceAttributeLanguage], [idGamePieceAttributeType], [idLanguage], [name]) VALUES (6, 7, 1, N'Accuracy')
INSERT [dbo].[egs_gamePieceAttrubuteTypeLanguage] ([idGamePieceAttributeLanguage], [idGamePieceAttributeType], [idLanguage], [name]) VALUES (7, 8, 1, N'Damage')
INSERT [dbo].[egs_gamePieceAttrubuteTypeLanguage] ([idGamePieceAttributeLanguage], [idGamePieceAttributeType], [idLanguage], [name]) VALUES (8, 9, 1, N'Range Min')
INSERT [dbo].[egs_gamePieceAttrubuteTypeLanguage] ([idGamePieceAttributeLanguage], [idGamePieceAttributeType], [idLanguage], [name]) VALUES (9, 10, 1, N'Range Max')
INSERT [dbo].[egs_gamePieceAttrubuteTypeLanguage] ([idGamePieceAttributeLanguage], [idGamePieceAttributeType], [idLanguage], [name]) VALUES (10, 11, 1, N'Shield')
INSERT [dbo].[egs_gamePieceAttrubuteTypeLanguage] ([idGamePieceAttributeLanguage], [idGamePieceAttributeType], [idLanguage], [name]) VALUES (11, 12, 1, N'Rest')
INSERT [dbo].[egs_gamePieceAttrubuteTypeLanguage] ([idGamePieceAttributeLanguage], [idGamePieceAttributeType], [idLanguage], [name]) VALUES (12, 13, 1, N'Special')
INSERT [dbo].[egs_gamePieceAttrubuteTypeLanguage] ([idGamePieceAttributeLanguage], [idGamePieceAttributeType], [idLanguage], [name]) VALUES (13, 14, 1, N'Crew Cost')
INSERT [dbo].[egs_gamePieceAttrubuteTypeLanguage] ([idGamePieceAttributeLanguage], [idGamePieceAttributeType], [idLanguage], [name]) VALUES (14, 15, 1, N'Speed')
INSERT [dbo].[egs_gamePieceAttrubuteTypeLanguage] ([idGamePieceAttributeLanguage], [idGamePieceAttributeType], [idLanguage], [name]) VALUES (15, 16, 1, N'Small Image')
INSERT [dbo].[egs_gamePieceAttrubuteTypeLanguage] ([idGamePieceAttributeLanguage], [idGamePieceAttributeType], [idLanguage], [name]) VALUES (16, 17, 1, N'Large Image')
INSERT [dbo].[egs_gamePieceAttrubuteTypeLanguage] ([idGamePieceAttributeLanguage], [idGamePieceAttributeType], [idLanguage], [name]) VALUES (17, 18, 1, N'Min Image')
INSERT [dbo].[egs_gamePieceAttrubuteTypeLanguage] ([idGamePieceAttributeLanguage], [idGamePieceAttributeType], [idLanguage], [name]) VALUES (18, 20, 1, N'Leadership')
INSERT [dbo].[egs_gamePieceAttrubuteTypeLanguage] ([idGamePieceAttributeLanguage], [idGamePieceAttributeType], [idLanguage], [name]) VALUES (19, 21, 1, N'Fleet Command')
INSERT [dbo].[egs_gamePieceAttrubuteTypeLanguage] ([idGamePieceAttributeLanguage], [idGamePieceAttributeType], [idLanguage], [name]) VALUES (20, 22, 1, N'Flavor Text')
INSERT [dbo].[egs_gamePieceAttrubuteTypeLanguage] ([idGamePieceAttributeLanguage], [idGamePieceAttributeType], [idLanguage], [name]) VALUES (21, 23, 1, N'Row Location')
INSERT [dbo].[egs_gamePieceAttrubuteTypeLanguage] ([idGamePieceAttributeLanguage], [idGamePieceAttributeType], [idLanguage], [name]) VALUES (23, 25, 1, N'Column Location')
SET IDENTITY_INSERT [dbo].[egs_gamePieceAttrubuteTypeLanguage] OFF
SET IDENTITY_INSERT [dbo].[egs_gamePieceCollection] ON 

INSERT [dbo].[egs_gamePieceCollection] ([idGamePieceCollection], [idGame], [idGamePieceCollectionType], [size]) VALUES (1, 1, 1, 50)
INSERT [dbo].[egs_gamePieceCollection] ([idGamePieceCollection], [idGame], [idGamePieceCollectionType], [size]) VALUES (2, 1, 1, 6)
INSERT [dbo].[egs_gamePieceCollection] ([idGamePieceCollection], [idGame], [idGamePieceCollectionType], [size]) VALUES (3, 1, 1, 0)
INSERT [dbo].[egs_gamePieceCollection] ([idGamePieceCollection], [idGame], [idGamePieceCollectionType], [size]) VALUES (4, 1, 2, 3)
INSERT [dbo].[egs_gamePieceCollection] ([idGamePieceCollection], [idGame], [idGamePieceCollectionType], [size]) VALUES (5, 1, 2, 12)
INSERT [dbo].[egs_gamePieceCollection] ([idGamePieceCollection], [idGame], [idGamePieceCollectionType], [size]) VALUES (6, 1, 4, 18)
SET IDENTITY_INSERT [dbo].[egs_gamePieceCollection] OFF
SET IDENTITY_INSERT [dbo].[egs_gamePieceCollectionAttribute] ON 

INSERT [dbo].[egs_gamePieceCollectionAttribute] ([idGamePieceCollectionAttribute], [idGamePieceCollection], [value], [dataName]) VALUES (1, 6, N'9', N'numRows')
INSERT [dbo].[egs_gamePieceCollectionAttribute] ([idGamePieceCollectionAttribute], [idGamePieceCollection], [value], [dataName]) VALUES (2, 6, N'6', N'numCols')
SET IDENTITY_INSERT [dbo].[egs_gamePieceCollectionAttribute] OFF
SET IDENTITY_INSERT [dbo].[egs_gamePieceCollectionAttributeLanguage] ON 

INSERT [dbo].[egs_gamePieceCollectionAttributeLanguage] ([idGamePieceCollectionAttributeLanguage], [idGamePIeceCollectionAttribute], [idLanguage], [name]) VALUES (1, 1, 1, N'Num Rows')
INSERT [dbo].[egs_gamePieceCollectionAttributeLanguage] ([idGamePieceCollectionAttributeLanguage], [idGamePIeceCollectionAttribute], [idLanguage], [name]) VALUES (2, 2, 1, N'Num Cols')
SET IDENTITY_INSERT [dbo].[egs_gamePieceCollectionAttributeLanguage] OFF
SET IDENTITY_INSERT [dbo].[egs_gamePieceCollectionGame] ON 

INSERT [dbo].[egs_gamePieceCollectionGame] ([idGamePieceCollectionGame], [idGamePieceCollection], [idGamePiece]) VALUES (1, 6, 242)
SET IDENTITY_INSERT [dbo].[egs_gamePieceCollectionGame] OFF
SET IDENTITY_INSERT [dbo].[egs_gamePieceCollectionLanguage] ON 

INSERT [dbo].[egs_gamePieceCollectionLanguage] ([idGamePieceCollectionLanguage], [idGamePieceCollection], [idLanguage], [collectionName]) VALUES (1, 1, 1, N'Conflict')
INSERT [dbo].[egs_gamePieceCollectionLanguage] ([idGamePieceCollectionLanguage], [idGamePieceCollection], [idLanguage], [collectionName]) VALUES (2, 2, 1, N'Player Hand')
INSERT [dbo].[egs_gamePieceCollectionLanguage] ([idGamePieceCollectionLanguage], [idGamePieceCollection], [idLanguage], [collectionName]) VALUES (3, 3, 1, N'Locker')
INSERT [dbo].[egs_gamePieceCollectionLanguage] ([idGamePieceCollectionLanguage], [idGamePieceCollection], [idLanguage], [collectionName]) VALUES (4, 4, 1, N'Specials')
INSERT [dbo].[egs_gamePieceCollectionLanguage] ([idGamePieceCollectionLanguage], [idGamePieceCollection], [idLanguage], [collectionName]) VALUES (7, 5, 1, N'Ship')
INSERT [dbo].[egs_gamePieceCollectionLanguage] ([idGamePieceCollectionLanguage], [idGamePieceCollection], [idLanguage], [collectionName]) VALUES (8, 6, 1, N'Game Board')
SET IDENTITY_INSERT [dbo].[egs_gamePieceCollectionLanguage] OFF
SET IDENTITY_INSERT [dbo].[egs_gamePieceCollectionPlayer] ON 

INSERT [dbo].[egs_gamePieceCollectionPlayer] ([idGamePieceCollectionPlayer], [idGamePieceCollection], [idPlayer], [isDefault], [dateCreated], [dateUpdated]) VALUES (1, 1, 1, 1, CAST(N'2015-11-16 20:21:48.453' AS DateTime), CAST(N'2015-11-16 20:21:48.453' AS DateTime))
INSERT [dbo].[egs_gamePieceCollectionPlayer] ([idGamePieceCollectionPlayer], [idGamePieceCollection], [idPlayer], [isDefault], [dateCreated], [dateUpdated]) VALUES (2, 4, 1, 1, CAST(N'2015-11-16 20:21:48.453' AS DateTime), CAST(N'2015-11-16 20:21:48.453' AS DateTime))
INSERT [dbo].[egs_gamePieceCollectionPlayer] ([idGamePieceCollectionPlayer], [idGamePieceCollection], [idPlayer], [isDefault], [dateCreated], [dateUpdated]) VALUES (3, 5, 1, 1, CAST(N'2015-11-16 20:21:48.453' AS DateTime), CAST(N'2015-11-16 20:21:48.453' AS DateTime))
INSERT [dbo].[egs_gamePieceCollectionPlayer] ([idGamePieceCollectionPlayer], [idGamePieceCollection], [idPlayer], [isDefault], [dateCreated], [dateUpdated]) VALUES (4, 1, 2, 1, CAST(N'2015-11-16 20:21:48.453' AS DateTime), CAST(N'2015-11-16 20:21:48.453' AS DateTime))
INSERT [dbo].[egs_gamePieceCollectionPlayer] ([idGamePieceCollectionPlayer], [idGamePieceCollection], [idPlayer], [isDefault], [dateCreated], [dateUpdated]) VALUES (5, 4, 2, 1, CAST(N'2015-11-16 20:21:48.453' AS DateTime), CAST(N'2015-11-16 20:21:48.453' AS DateTime))
INSERT [dbo].[egs_gamePieceCollectionPlayer] ([idGamePieceCollectionPlayer], [idGamePieceCollection], [idPlayer], [isDefault], [dateCreated], [dateUpdated]) VALUES (6, 5, 2, 1, CAST(N'2015-11-16 20:21:48.453' AS DateTime), CAST(N'2015-11-16 20:21:48.453' AS DateTime))
SET IDENTITY_INSERT [dbo].[egs_gamePieceCollectionPlayer] OFF
SET IDENTITY_INSERT [dbo].[egs_gamePieceCollectionPlayerPiece] ON 

INSERT [dbo].[egs_gamePieceCollectionPlayerPiece] ([idGamePieceCollectionPlayerPiece], [idGamePIeceCollectionPlayer], [idGamePiece]) VALUES (1, 3, 184)
INSERT [dbo].[egs_gamePieceCollectionPlayerPiece] ([idGamePieceCollectionPlayerPiece], [idGamePIeceCollectionPlayer], [idGamePiece]) VALUES (2, 3, 187)
INSERT [dbo].[egs_gamePieceCollectionPlayerPiece] ([idGamePieceCollectionPlayerPiece], [idGamePIeceCollectionPlayer], [idGamePiece]) VALUES (3, 3, 190)
INSERT [dbo].[egs_gamePieceCollectionPlayerPiece] ([idGamePieceCollectionPlayerPiece], [idGamePIeceCollectionPlayer], [idGamePiece]) VALUES (4, 3, 193)
INSERT [dbo].[egs_gamePieceCollectionPlayerPiece] ([idGamePieceCollectionPlayerPiece], [idGamePIeceCollectionPlayer], [idGamePiece]) VALUES (5, 3, 191)
INSERT [dbo].[egs_gamePieceCollectionPlayerPiece] ([idGamePieceCollectionPlayerPiece], [idGamePIeceCollectionPlayer], [idGamePiece]) VALUES (6, 3, 179)
INSERT [dbo].[egs_gamePieceCollectionPlayerPiece] ([idGamePieceCollectionPlayerPiece], [idGamePIeceCollectionPlayer], [idGamePiece]) VALUES (7, 3, 188)
INSERT [dbo].[egs_gamePieceCollectionPlayerPiece] ([idGamePieceCollectionPlayerPiece], [idGamePIeceCollectionPlayer], [idGamePiece]) VALUES (8, 3, 185)
INSERT [dbo].[egs_gamePieceCollectionPlayerPiece] ([idGamePieceCollectionPlayerPiece], [idGamePIeceCollectionPlayer], [idGamePiece]) VALUES (9, 3, 182)
INSERT [dbo].[egs_gamePieceCollectionPlayerPiece] ([idGamePieceCollectionPlayerPiece], [idGamePIeceCollectionPlayer], [idGamePiece]) VALUES (10, 3, 178)
INSERT [dbo].[egs_gamePieceCollectionPlayerPiece] ([idGamePieceCollectionPlayerPiece], [idGamePIeceCollectionPlayer], [idGamePiece]) VALUES (11, 3, 181)
INSERT [dbo].[egs_gamePieceCollectionPlayerPiece] ([idGamePieceCollectionPlayerPiece], [idGamePIeceCollectionPlayer], [idGamePiece]) VALUES (12, 3, 193)
INSERT [dbo].[egs_gamePieceCollectionPlayerPiece] ([idGamePieceCollectionPlayerPiece], [idGamePIeceCollectionPlayer], [idGamePiece]) VALUES (13, 1, 194)
INSERT [dbo].[egs_gamePieceCollectionPlayerPiece] ([idGamePieceCollectionPlayerPiece], [idGamePIeceCollectionPlayer], [idGamePiece]) VALUES (14, 1, 195)
INSERT [dbo].[egs_gamePieceCollectionPlayerPiece] ([idGamePieceCollectionPlayerPiece], [idGamePIeceCollectionPlayer], [idGamePiece]) VALUES (15, 1, 196)
INSERT [dbo].[egs_gamePieceCollectionPlayerPiece] ([idGamePieceCollectionPlayerPiece], [idGamePIeceCollectionPlayer], [idGamePiece]) VALUES (16, 1, 197)
INSERT [dbo].[egs_gamePieceCollectionPlayerPiece] ([idGamePieceCollectionPlayerPiece], [idGamePIeceCollectionPlayer], [idGamePiece]) VALUES (17, 1, 198)
INSERT [dbo].[egs_gamePieceCollectionPlayerPiece] ([idGamePieceCollectionPlayerPiece], [idGamePIeceCollectionPlayer], [idGamePiece]) VALUES (18, 1, 199)
INSERT [dbo].[egs_gamePieceCollectionPlayerPiece] ([idGamePieceCollectionPlayerPiece], [idGamePIeceCollectionPlayer], [idGamePiece]) VALUES (19, 1, 200)
INSERT [dbo].[egs_gamePieceCollectionPlayerPiece] ([idGamePieceCollectionPlayerPiece], [idGamePIeceCollectionPlayer], [idGamePiece]) VALUES (20, 1, 201)
INSERT [dbo].[egs_gamePieceCollectionPlayerPiece] ([idGamePieceCollectionPlayerPiece], [idGamePIeceCollectionPlayer], [idGamePiece]) VALUES (21, 1, 202)
INSERT [dbo].[egs_gamePieceCollectionPlayerPiece] ([idGamePieceCollectionPlayerPiece], [idGamePIeceCollectionPlayer], [idGamePiece]) VALUES (22, 1, 203)
INSERT [dbo].[egs_gamePieceCollectionPlayerPiece] ([idGamePieceCollectionPlayerPiece], [idGamePIeceCollectionPlayer], [idGamePiece]) VALUES (23, 1, 204)
INSERT [dbo].[egs_gamePieceCollectionPlayerPiece] ([idGamePieceCollectionPlayerPiece], [idGamePIeceCollectionPlayer], [idGamePiece]) VALUES (24, 1, 211)
INSERT [dbo].[egs_gamePieceCollectionPlayerPiece] ([idGamePieceCollectionPlayerPiece], [idGamePIeceCollectionPlayer], [idGamePiece]) VALUES (25, 1, 212)
INSERT [dbo].[egs_gamePieceCollectionPlayerPiece] ([idGamePieceCollectionPlayerPiece], [idGamePIeceCollectionPlayer], [idGamePiece]) VALUES (26, 1, 213)
INSERT [dbo].[egs_gamePieceCollectionPlayerPiece] ([idGamePieceCollectionPlayerPiece], [idGamePIeceCollectionPlayer], [idGamePiece]) VALUES (27, 1, 214)
INSERT [dbo].[egs_gamePieceCollectionPlayerPiece] ([idGamePieceCollectionPlayerPiece], [idGamePIeceCollectionPlayer], [idGamePiece]) VALUES (28, 1, 215)
INSERT [dbo].[egs_gamePieceCollectionPlayerPiece] ([idGamePieceCollectionPlayerPiece], [idGamePIeceCollectionPlayer], [idGamePiece]) VALUES (29, 1, 216)
INSERT [dbo].[egs_gamePieceCollectionPlayerPiece] ([idGamePieceCollectionPlayerPiece], [idGamePIeceCollectionPlayer], [idGamePiece]) VALUES (30, 1, 217)
INSERT [dbo].[egs_gamePieceCollectionPlayerPiece] ([idGamePieceCollectionPlayerPiece], [idGamePIeceCollectionPlayer], [idGamePiece]) VALUES (31, 1, 218)
INSERT [dbo].[egs_gamePieceCollectionPlayerPiece] ([idGamePieceCollectionPlayerPiece], [idGamePIeceCollectionPlayer], [idGamePiece]) VALUES (32, 1, 219)
INSERT [dbo].[egs_gamePieceCollectionPlayerPiece] ([idGamePieceCollectionPlayerPiece], [idGamePIeceCollectionPlayer], [idGamePiece]) VALUES (33, 1, 220)
INSERT [dbo].[egs_gamePieceCollectionPlayerPiece] ([idGamePieceCollectionPlayerPiece], [idGamePIeceCollectionPlayer], [idGamePiece]) VALUES (34, 1, 221)
INSERT [dbo].[egs_gamePieceCollectionPlayerPiece] ([idGamePieceCollectionPlayerPiece], [idGamePIeceCollectionPlayer], [idGamePiece]) VALUES (35, 1, 222)
INSERT [dbo].[egs_gamePieceCollectionPlayerPiece] ([idGamePieceCollectionPlayerPiece], [idGamePIeceCollectionPlayer], [idGamePiece]) VALUES (36, 1, 223)
INSERT [dbo].[egs_gamePieceCollectionPlayerPiece] ([idGamePieceCollectionPlayerPiece], [idGamePIeceCollectionPlayer], [idGamePiece]) VALUES (37, 1, 224)
INSERT [dbo].[egs_gamePieceCollectionPlayerPiece] ([idGamePieceCollectionPlayerPiece], [idGamePIeceCollectionPlayer], [idGamePiece]) VALUES (38, 1, 233)
INSERT [dbo].[egs_gamePieceCollectionPlayerPiece] ([idGamePieceCollectionPlayerPiece], [idGamePIeceCollectionPlayer], [idGamePiece]) VALUES (39, 1, 225)
INSERT [dbo].[egs_gamePieceCollectionPlayerPiece] ([idGamePieceCollectionPlayerPiece], [idGamePIeceCollectionPlayer], [idGamePiece]) VALUES (40, 1, 226)
INSERT [dbo].[egs_gamePieceCollectionPlayerPiece] ([idGamePieceCollectionPlayerPiece], [idGamePIeceCollectionPlayer], [idGamePiece]) VALUES (41, 1, 227)
INSERT [dbo].[egs_gamePieceCollectionPlayerPiece] ([idGamePieceCollectionPlayerPiece], [idGamePIeceCollectionPlayer], [idGamePiece]) VALUES (42, 1, 228)
INSERT [dbo].[egs_gamePieceCollectionPlayerPiece] ([idGamePieceCollectionPlayerPiece], [idGamePIeceCollectionPlayer], [idGamePiece]) VALUES (43, 1, 229)
INSERT [dbo].[egs_gamePieceCollectionPlayerPiece] ([idGamePieceCollectionPlayerPiece], [idGamePIeceCollectionPlayer], [idGamePiece]) VALUES (44, 1, 230)
INSERT [dbo].[egs_gamePieceCollectionPlayerPiece] ([idGamePieceCollectionPlayerPiece], [idGamePIeceCollectionPlayer], [idGamePiece]) VALUES (45, 1, 231)
INSERT [dbo].[egs_gamePieceCollectionPlayerPiece] ([idGamePieceCollectionPlayerPiece], [idGamePIeceCollectionPlayer], [idGamePiece]) VALUES (46, 1, 232)
INSERT [dbo].[egs_gamePieceCollectionPlayerPiece] ([idGamePieceCollectionPlayerPiece], [idGamePIeceCollectionPlayer], [idGamePiece]) VALUES (47, 2, 239)
INSERT [dbo].[egs_gamePieceCollectionPlayerPiece] ([idGamePieceCollectionPlayerPiece], [idGamePIeceCollectionPlayer], [idGamePiece]) VALUES (48, 2, 240)
INSERT [dbo].[egs_gamePieceCollectionPlayerPiece] ([idGamePieceCollectionPlayerPiece], [idGamePIeceCollectionPlayer], [idGamePiece]) VALUES (49, 2, 241)
INSERT [dbo].[egs_gamePieceCollectionPlayerPiece] ([idGamePieceCollectionPlayerPiece], [idGamePIeceCollectionPlayer], [idGamePiece]) VALUES (50, 4, 194)
INSERT [dbo].[egs_gamePieceCollectionPlayerPiece] ([idGamePieceCollectionPlayerPiece], [idGamePIeceCollectionPlayer], [idGamePiece]) VALUES (51, 4, 195)
INSERT [dbo].[egs_gamePieceCollectionPlayerPiece] ([idGamePieceCollectionPlayerPiece], [idGamePIeceCollectionPlayer], [idGamePiece]) VALUES (52, 4, 196)
INSERT [dbo].[egs_gamePieceCollectionPlayerPiece] ([idGamePieceCollectionPlayerPiece], [idGamePIeceCollectionPlayer], [idGamePiece]) VALUES (53, 4, 197)
INSERT [dbo].[egs_gamePieceCollectionPlayerPiece] ([idGamePieceCollectionPlayerPiece], [idGamePIeceCollectionPlayer], [idGamePiece]) VALUES (54, 4, 198)
INSERT [dbo].[egs_gamePieceCollectionPlayerPiece] ([idGamePieceCollectionPlayerPiece], [idGamePIeceCollectionPlayer], [idGamePiece]) VALUES (55, 4, 199)
INSERT [dbo].[egs_gamePieceCollectionPlayerPiece] ([idGamePieceCollectionPlayerPiece], [idGamePIeceCollectionPlayer], [idGamePiece]) VALUES (56, 4, 200)
INSERT [dbo].[egs_gamePieceCollectionPlayerPiece] ([idGamePieceCollectionPlayerPiece], [idGamePIeceCollectionPlayer], [idGamePiece]) VALUES (57, 4, 201)
INSERT [dbo].[egs_gamePieceCollectionPlayerPiece] ([idGamePieceCollectionPlayerPiece], [idGamePIeceCollectionPlayer], [idGamePiece]) VALUES (58, 4, 202)
INSERT [dbo].[egs_gamePieceCollectionPlayerPiece] ([idGamePieceCollectionPlayerPiece], [idGamePIeceCollectionPlayer], [idGamePiece]) VALUES (59, 4, 203)
INSERT [dbo].[egs_gamePieceCollectionPlayerPiece] ([idGamePieceCollectionPlayerPiece], [idGamePIeceCollectionPlayer], [idGamePiece]) VALUES (60, 4, 204)
INSERT [dbo].[egs_gamePieceCollectionPlayerPiece] ([idGamePieceCollectionPlayerPiece], [idGamePIeceCollectionPlayer], [idGamePiece]) VALUES (61, 4, 211)
INSERT [dbo].[egs_gamePieceCollectionPlayerPiece] ([idGamePieceCollectionPlayerPiece], [idGamePIeceCollectionPlayer], [idGamePiece]) VALUES (62, 4, 212)
INSERT [dbo].[egs_gamePieceCollectionPlayerPiece] ([idGamePieceCollectionPlayerPiece], [idGamePIeceCollectionPlayer], [idGamePiece]) VALUES (63, 4, 213)
INSERT [dbo].[egs_gamePieceCollectionPlayerPiece] ([idGamePieceCollectionPlayerPiece], [idGamePIeceCollectionPlayer], [idGamePiece]) VALUES (64, 4, 214)
INSERT [dbo].[egs_gamePieceCollectionPlayerPiece] ([idGamePieceCollectionPlayerPiece], [idGamePIeceCollectionPlayer], [idGamePiece]) VALUES (65, 4, 215)
INSERT [dbo].[egs_gamePieceCollectionPlayerPiece] ([idGamePieceCollectionPlayerPiece], [idGamePIeceCollectionPlayer], [idGamePiece]) VALUES (66, 4, 216)
INSERT [dbo].[egs_gamePieceCollectionPlayerPiece] ([idGamePieceCollectionPlayerPiece], [idGamePIeceCollectionPlayer], [idGamePiece]) VALUES (67, 4, 217)
INSERT [dbo].[egs_gamePieceCollectionPlayerPiece] ([idGamePieceCollectionPlayerPiece], [idGamePIeceCollectionPlayer], [idGamePiece]) VALUES (68, 4, 218)
INSERT [dbo].[egs_gamePieceCollectionPlayerPiece] ([idGamePieceCollectionPlayerPiece], [idGamePIeceCollectionPlayer], [idGamePiece]) VALUES (69, 4, 219)
INSERT [dbo].[egs_gamePieceCollectionPlayerPiece] ([idGamePieceCollectionPlayerPiece], [idGamePIeceCollectionPlayer], [idGamePiece]) VALUES (70, 4, 220)
INSERT [dbo].[egs_gamePieceCollectionPlayerPiece] ([idGamePieceCollectionPlayerPiece], [idGamePIeceCollectionPlayer], [idGamePiece]) VALUES (71, 4, 221)
INSERT [dbo].[egs_gamePieceCollectionPlayerPiece] ([idGamePieceCollectionPlayerPiece], [idGamePIeceCollectionPlayer], [idGamePiece]) VALUES (72, 4, 222)
INSERT [dbo].[egs_gamePieceCollectionPlayerPiece] ([idGamePieceCollectionPlayerPiece], [idGamePIeceCollectionPlayer], [idGamePiece]) VALUES (73, 4, 223)
INSERT [dbo].[egs_gamePieceCollectionPlayerPiece] ([idGamePieceCollectionPlayerPiece], [idGamePIeceCollectionPlayer], [idGamePiece]) VALUES (74, 4, 224)
INSERT [dbo].[egs_gamePieceCollectionPlayerPiece] ([idGamePieceCollectionPlayerPiece], [idGamePIeceCollectionPlayer], [idGamePiece]) VALUES (75, 4, 233)
INSERT [dbo].[egs_gamePieceCollectionPlayerPiece] ([idGamePieceCollectionPlayerPiece], [idGamePIeceCollectionPlayer], [idGamePiece]) VALUES (76, 4, 225)
INSERT [dbo].[egs_gamePieceCollectionPlayerPiece] ([idGamePieceCollectionPlayerPiece], [idGamePIeceCollectionPlayer], [idGamePiece]) VALUES (77, 4, 226)
INSERT [dbo].[egs_gamePieceCollectionPlayerPiece] ([idGamePieceCollectionPlayerPiece], [idGamePIeceCollectionPlayer], [idGamePiece]) VALUES (78, 4, 227)
INSERT [dbo].[egs_gamePieceCollectionPlayerPiece] ([idGamePieceCollectionPlayerPiece], [idGamePIeceCollectionPlayer], [idGamePiece]) VALUES (79, 4, 228)
INSERT [dbo].[egs_gamePieceCollectionPlayerPiece] ([idGamePieceCollectionPlayerPiece], [idGamePIeceCollectionPlayer], [idGamePiece]) VALUES (80, 4, 229)
INSERT [dbo].[egs_gamePieceCollectionPlayerPiece] ([idGamePieceCollectionPlayerPiece], [idGamePIeceCollectionPlayer], [idGamePiece]) VALUES (81, 4, 230)
INSERT [dbo].[egs_gamePieceCollectionPlayerPiece] ([idGamePieceCollectionPlayerPiece], [idGamePIeceCollectionPlayer], [idGamePiece]) VALUES (82, 4, 231)
INSERT [dbo].[egs_gamePieceCollectionPlayerPiece] ([idGamePieceCollectionPlayerPiece], [idGamePIeceCollectionPlayer], [idGamePiece]) VALUES (83, 4, 232)
INSERT [dbo].[egs_gamePieceCollectionPlayerPiece] ([idGamePieceCollectionPlayerPiece], [idGamePIeceCollectionPlayer], [idGamePiece]) VALUES (84, 5, 239)
INSERT [dbo].[egs_gamePieceCollectionPlayerPiece] ([idGamePieceCollectionPlayerPiece], [idGamePIeceCollectionPlayer], [idGamePiece]) VALUES (85, 5, 240)
INSERT [dbo].[egs_gamePieceCollectionPlayerPiece] ([idGamePieceCollectionPlayerPiece], [idGamePIeceCollectionPlayer], [idGamePiece]) VALUES (86, 5, 241)
INSERT [dbo].[egs_gamePieceCollectionPlayerPiece] ([idGamePieceCollectionPlayerPiece], [idGamePIeceCollectionPlayer], [idGamePiece]) VALUES (87, 6, 184)
INSERT [dbo].[egs_gamePieceCollectionPlayerPiece] ([idGamePieceCollectionPlayerPiece], [idGamePIeceCollectionPlayer], [idGamePiece]) VALUES (88, 6, 187)
INSERT [dbo].[egs_gamePieceCollectionPlayerPiece] ([idGamePieceCollectionPlayerPiece], [idGamePIeceCollectionPlayer], [idGamePiece]) VALUES (89, 6, 190)
INSERT [dbo].[egs_gamePieceCollectionPlayerPiece] ([idGamePieceCollectionPlayerPiece], [idGamePIeceCollectionPlayer], [idGamePiece]) VALUES (90, 6, 193)
INSERT [dbo].[egs_gamePieceCollectionPlayerPiece] ([idGamePieceCollectionPlayerPiece], [idGamePIeceCollectionPlayer], [idGamePiece]) VALUES (91, 6, 191)
INSERT [dbo].[egs_gamePieceCollectionPlayerPiece] ([idGamePieceCollectionPlayerPiece], [idGamePIeceCollectionPlayer], [idGamePiece]) VALUES (92, 6, 179)
INSERT [dbo].[egs_gamePieceCollectionPlayerPiece] ([idGamePieceCollectionPlayerPiece], [idGamePIeceCollectionPlayer], [idGamePiece]) VALUES (93, 6, 188)
INSERT [dbo].[egs_gamePieceCollectionPlayerPiece] ([idGamePieceCollectionPlayerPiece], [idGamePIeceCollectionPlayer], [idGamePiece]) VALUES (94, 6, 185)
INSERT [dbo].[egs_gamePieceCollectionPlayerPiece] ([idGamePieceCollectionPlayerPiece], [idGamePIeceCollectionPlayer], [idGamePiece]) VALUES (95, 6, 182)
INSERT [dbo].[egs_gamePieceCollectionPlayerPiece] ([idGamePieceCollectionPlayerPiece], [idGamePIeceCollectionPlayer], [idGamePiece]) VALUES (96, 6, 178)
INSERT [dbo].[egs_gamePieceCollectionPlayerPiece] ([idGamePieceCollectionPlayerPiece], [idGamePIeceCollectionPlayer], [idGamePiece]) VALUES (97, 6, 181)
INSERT [dbo].[egs_gamePieceCollectionPlayerPiece] ([idGamePieceCollectionPlayerPiece], [idGamePIeceCollectionPlayer], [idGamePiece]) VALUES (98, 6, 193)
SET IDENTITY_INSERT [dbo].[egs_gamePieceCollectionPlayerPiece] OFF
GO
SET IDENTITY_INSERT [dbo].[egs_gamePieceCollectionType] ON 

INSERT [dbo].[egs_gamePieceCollectionType] ([idGamePieceCollectionType], [idGame], [collectionType]) VALUES (1, 1, N'playerDeck')
INSERT [dbo].[egs_gamePieceCollectionType] ([idGamePieceCollectionType], [idGame], [collectionType]) VALUES (2, 1, N'playerZone')
INSERT [dbo].[egs_gamePieceCollectionType] ([idGamePieceCollectionType], [idGame], [collectionType]) VALUES (4, 1, N'gameBoard')
SET IDENTITY_INSERT [dbo].[egs_gamePieceCollectionType] OFF
SET IDENTITY_INSERT [dbo].[egs_gamePieceLanguage] ON 

INSERT [dbo].[egs_gamePieceLanguage] ([idGamePieceLanguage], [idLanguage], [idGamePiece], [name], [description]) VALUES (142, 1, 150, N'Vanilla', N'')
INSERT [dbo].[egs_gamePieceLanguage] ([idGamePieceLanguage], [idLanguage], [idGamePiece], [name], [description]) VALUES (143, 1, 151, N'Close', N'')
INSERT [dbo].[egs_gamePieceLanguage] ([idGamePieceLanguage], [idLanguage], [idGamePiece], [name], [description]) VALUES (144, 1, 152, N'Hardened', N'')
INSERT [dbo].[egs_gamePieceLanguage] ([idGamePieceLanguage], [idLanguage], [idGamePiece], [name], [description]) VALUES (145, 1, 153, N'Heavy', N'')
INSERT [dbo].[egs_gamePieceLanguage] ([idGamePieceLanguage], [idLanguage], [idGamePiece], [name], [description]) VALUES (146, 1, 154, N'Accurate', N'')
INSERT [dbo].[egs_gamePieceLanguage] ([idGamePieceLanguage], [idLanguage], [idGamePiece], [name], [description]) VALUES (147, 1, 155, N'Punishing', N'')
INSERT [dbo].[egs_gamePieceLanguage] ([idGamePieceLanguage], [idLanguage], [idGamePiece], [name], [description]) VALUES (148, 1, 156, N'CNI', N'Damage: The next battery, autocannon, or turret to target the {} or support craft rolls 2d10 and keeps the best')
INSERT [dbo].[egs_gamePieceLanguage] ([idGamePieceLanguage], [idLanguage], [idGamePiece], [name], [description]) VALUES (149, 1, 157, N'Twin-linked', N'Roll 2d10 to hit and take best')
INSERT [dbo].[egs_gamePieceLanguage] ([idGamePieceLanguage], [idLanguage], [idGamePiece], [name], [description]) VALUES (150, 1, 158, N'Bracketing', N'Fire twice at same target, if both shots hit deal 1 extra damage.')
INSERT [dbo].[egs_gamePieceLanguage] ([idGamePieceLanguage], [idLanguage], [idGamePiece], [name], [description]) VALUES (151, 1, 159, N'Big Bracketing', N'Fire three times at same target, if all three shots hit deal 1 extra damage.')
INSERT [dbo].[egs_gamePieceLanguage] ([idGamePieceLanguage], [idLanguage], [idGamePiece], [name], [description]) VALUES (152, 1, 160, N'Exotic', N'Cannot be REPAIRED')
INSERT [dbo].[egs_gamePieceLanguage] ([idGamePieceLanguage], [idLanguage], [idGamePiece], [name], [description]) VALUES (153, 1, 161, N'Repairing', N'Repairs one SV during the REPAIR STEP')
INSERT [dbo].[egs_gamePieceLanguage] ([idGamePieceLanguage], [idLanguage], [idGamePiece], [name], [description]) VALUES (154, 1, 162, N'A-Bomber', N'Accuracy & Damage:  +1 accy and +1 damage vs bombers')
INSERT [dbo].[egs_gamePieceLanguage] ([idGamePieceLanguage], [idLanguage], [idGamePiece], [name], [description]) VALUES (155, 1, 163, N'A-Fighter', N'+1 accy and +1 damage vs fighters')
INSERT [dbo].[egs_gamePieceLanguage] ([idGamePieceLanguage], [idLanguage], [idGamePiece], [name], [description]) VALUES (156, 1, 164, N'A-Ordnance', N'+1 accy and +1 damage vs assault craft')
INSERT [dbo].[egs_gamePieceLanguage] ([idGamePieceLanguage], [idLanguage], [idGamePiece], [name], [description]) VALUES (157, 1, 165, N'Fast Firing', N'Fire again at target with 1 accy, then again at 0 accy.')
INSERT [dbo].[egs_gamePieceLanguage] ([idGamePieceLanguage], [idLanguage], [idGamePiece], [name], [description]) VALUES (158, 1, 166, N'RBG', N'Cannot be fired more than once a turn.')
INSERT [dbo].[egs_gamePieceLanguage] ([idGamePieceLanguage], [idLanguage], [idGamePiece], [name], [description]) VALUES (159, 1, 167, N'Deep', N'Bombers cannot attack or damage this.')
INSERT [dbo].[egs_gamePieceLanguage] ([idGamePieceLanguage], [idLanguage], [idGamePiece], [name], [description]) VALUES (160, 1, 168, N'Multi-target', N'On a hit, you may take a second shot against a second legal target within one space of the first target')
INSERT [dbo].[egs_gamePieceLanguage] ([idGamePieceLanguage], [idLanguage], [idGamePiece], [name], [description]) VALUES (161, 1, 169, N'Plasma Thrower', N'Deals 1 damage on a miss.')
INSERT [dbo].[egs_gamePieceLanguage] ([idGamePieceLanguage], [idLanguage], [idGamePiece], [name], [description]) VALUES (162, 1, 170, N'Savage', N'Deals 2 extra damage on a critical hit.')
INSERT [dbo].[egs_gamePieceLanguage] ([idGamePieceLanguage], [idLanguage], [idGamePiece], [name], [description]) VALUES (163, 1, 171, N'Vanilla', N'')
INSERT [dbo].[egs_gamePieceLanguage] ([idGamePieceLanguage], [idLanguage], [idGamePiece], [name], [description]) VALUES (164, 1, 172, N'Accurate', N'')
INSERT [dbo].[egs_gamePieceLanguage] ([idGamePieceLanguage], [idLanguage], [idGamePiece], [name], [description]) VALUES (165, 1, 173, N'Hardened', N'')
INSERT [dbo].[egs_gamePieceLanguage] ([idGamePieceLanguage], [idLanguage], [idGamePiece], [name], [description]) VALUES (166, 1, 174, N'Heavy', N'')
INSERT [dbo].[egs_gamePieceLanguage] ([idGamePieceLanguage], [idLanguage], [idGamePiece], [name], [description]) VALUES (167, 1, 175, N'Precise', N'')
INSERT [dbo].[egs_gamePieceLanguage] ([idGamePieceLanguage], [idLanguage], [idGamePiece], [name], [description]) VALUES (168, 1, 176, N'Really Big', N'')
INSERT [dbo].[egs_gamePieceLanguage] ([idGamePieceLanguage], [idLanguage], [idGamePiece], [name], [description]) VALUES (169, 1, 177, N'CNI', N'Next battery or autocannon to fire on the struck {} rolls 2d10 and takes the best')
INSERT [dbo].[egs_gamePieceLanguage] ([idGamePieceLanguage], [idLanguage], [idGamePiece], [name], [description]) VALUES (170, 1, 178, N'Hummingbird', N'When rolling to hit, roll two dice and take the better result')
INSERT [dbo].[egs_gamePieceLanguage] ([idGamePieceLanguage], [idLanguage], [idGamePiece], [name], [description]) VALUES (171, 1, 179, N'Demon', N'')
INSERT [dbo].[egs_gamePieceLanguage] ([idGamePieceLanguage], [idLanguage], [idGamePiece], [name], [description]) VALUES (172, 1, 180, N'Sniper', N'')
INSERT [dbo].[egs_gamePieceLanguage] ([idGamePieceLanguage], [idLanguage], [idGamePiece], [name], [description]) VALUES (173, 1, 181, N'Castle', N'')
INSERT [dbo].[egs_gamePieceLanguage] ([idGamePieceLanguage], [idLanguage], [idGamePiece], [name], [description]) VALUES (174, 1, 182, N'Angel', N'Restores up to two shields each turn')
INSERT [dbo].[egs_gamePieceLanguage] ([idGamePieceLanguage], [idLanguage], [idGamePiece], [name], [description]) VALUES (175, 1, 183, N'Incinerator', N'This turret still deals one damage on a missed attack.')
INSERT [dbo].[egs_gamePieceLanguage] ([idGamePieceLanguage], [idLanguage], [idGamePiece], [name], [description]) VALUES (176, 1, 184, N'Hellthorn', N'')
INSERT [dbo].[egs_gamePieceLanguage] ([idGamePieceLanguage], [idLanguage], [idGamePiece], [name], [description]) VALUES (177, 1, 185, N'Cherub', N'Increase your maximum shields by one')
INSERT [dbo].[egs_gamePieceLanguage] ([idGamePieceLanguage], [idLanguage], [idGamePiece], [name], [description]) VALUES (178, 1, 186, N'Horizon Mining Laser', N'The Horizon can only be fired a unshielded compartments but automatically hits.  Do not roll to hit.')
INSERT [dbo].[egs_gamePieceLanguage] ([idGamePieceLanguage], [idLanguage], [idGamePiece], [name], [description]) VALUES (179, 1, 187, N'Hammer', N'')
INSERT [dbo].[egs_gamePieceLanguage] ([idGamePieceLanguage], [idLanguage], [idGamePiece], [name], [description]) VALUES (180, 1, 188, N'Imp', N'Your reactor generates an additional two power each turn')
INSERT [dbo].[egs_gamePieceLanguage] ([idGamePieceLanguage], [idLanguage], [idGamePiece], [name], [description]) VALUES (181, 1, 189, N'Geddon Cannon', N'Attacks EVERY compartment.  Roll to hit each compartment separately.')
INSERT [dbo].[egs_gamePieceLanguage] ([idGamePieceLanguage], [idLanguage], [idGamePiece], [name], [description]) VALUES (182, 1, 190, N'Wildfire Cannon', N'This battery still deals one damage on a missed attack.')
INSERT [dbo].[egs_gamePieceLanguage] ([idGamePieceLanguage], [idLanguage], [idGamePiece], [name], [description]) VALUES (183, 1, 191, N'Ordnance Rack', N'')
INSERT [dbo].[egs_gamePieceLanguage] ([idGamePieceLanguage], [idLanguage], [idGamePiece], [name], [description]) VALUES (184, 1, 192, N'Sledgehammer', N'')
INSERT [dbo].[egs_gamePieceLanguage] ([idGamePieceLanguage], [idLanguage], [idGamePiece], [name], [description]) VALUES (185, 1, 193, N'Launch Bay', N'')
INSERT [dbo].[egs_gamePieceLanguage] ([idGamePieceLanguage], [idLanguage], [idGamePiece], [name], [description]) VALUES (186, 1, 194, N'Raptor', N'')
INSERT [dbo].[egs_gamePieceLanguage] ([idGamePieceLanguage], [idLanguage], [idGamePiece], [name], [description]) VALUES (187, 1, 195, N'Banshee', N'May not be attacked by turrets or autocannon')
INSERT [dbo].[egs_gamePieceLanguage] ([idGamePieceLanguage], [idLanguage], [idGamePiece], [name], [description]) VALUES (188, 1, 196, N'Sabre', N'Opponents may not counterattack')
INSERT [dbo].[egs_gamePieceLanguage] ([idGamePieceLanguage], [idLanguage], [idGamePiece], [name], [description]) VALUES (189, 1, 197, N'Wasp', N'When this fighter is destroyed, put it back in your hand')
INSERT [dbo].[egs_gamePieceLanguage] ([idGamePieceLanguage], [idLanguage], [idGamePiece], [name], [description]) VALUES (190, 1, 198, N'Aegis', N'The Aegis has one point of shields.  Restore them during the refresh phase of your turn.')
INSERT [dbo].[egs_gamePieceLanguage] ([idGamePieceLanguage], [idLanguage], [idGamePiece], [name], [description]) VALUES (191, 1, 199, N'Athena', N'If this card is placed in your locker deck for any reason, remove it from the game.')
INSERT [dbo].[egs_gamePieceLanguage] ([idGamePieceLanguage], [idLanguage], [idGamePiece], [name], [description]) VALUES (192, 1, 200, N'Hawk', N'')
INSERT [dbo].[egs_gamePieceLanguage] ([idGamePieceLanguage], [idLanguage], [idGamePiece], [name], [description]) VALUES (193, 1, 201, N'Broadsword', N'')
INSERT [dbo].[egs_gamePieceLanguage] ([idGamePieceLanguage], [idLanguage], [idGamePiece], [name], [description]) VALUES (194, 1, 202, N'Tarantula', N'A missed attack still deals one damage')
INSERT [dbo].[egs_gamePieceLanguage] ([idGamePieceLanguage], [idLanguage], [idGamePiece], [name], [description]) VALUES (195, 1, 203, N'Hornet', N'When this bomber is destroyed, put it back in your hand')
INSERT [dbo].[egs_gamePieceLanguage] ([idGamePieceLanguage], [idLanguage], [idGamePiece], [name], [description]) VALUES (196, 1, 204, N'Pack', N'')
INSERT [dbo].[egs_gamePieceLanguage] ([idGamePieceLanguage], [idLanguage], [idGamePiece], [name], [description]) VALUES (197, 1, 205, N'Wraith', N'May not be attacked by turrets or autocannon')
INSERT [dbo].[egs_gamePieceLanguage] ([idGamePieceLanguage], [idLanguage], [idGamePiece], [name], [description]) VALUES (198, 1, 206, N'Cutlass', N'Place two raiders in the same compartment')
INSERT [dbo].[egs_gamePieceLanguage] ([idGamePieceLanguage], [idLanguage], [idGamePiece], [name], [description]) VALUES (199, 1, 207, N'Host', N'Deal one damage to the compartment you deliver raiders to')
INSERT [dbo].[egs_gamePieceLanguage] ([idGamePieceLanguage], [idLanguage], [idGamePiece], [name], [description]) VALUES (200, 1, 208, N'Grindstone', N'When you deliver raiders, place one raider in both')
INSERT [dbo].[egs_gamePieceLanguage] ([idGamePieceLanguage], [idLanguage], [idGamePiece], [name], [description]) VALUES (201, 1, 209, N'Raccoon', N'When you deliver raiders your opponent must show you their hand.  Choose one card from it and remove it for the game')
INSERT [dbo].[egs_gamePieceLanguage] ([idGamePieceLanguage], [idLanguage], [idGamePiece], [name], [description]) VALUES (202, 1, 210, N'Bulwark', N'The Bulwark has one point of shields.  Restore them during the refresh phase of your team')
INSERT [dbo].[egs_gamePieceLanguage] ([idGamePieceLanguage], [idLanguage], [idGamePiece], [name], [description]) VALUES (203, 1, 211, N'Raven', N'')
INSERT [dbo].[egs_gamePieceLanguage] ([idGamePieceLanguage], [idLanguage], [idGamePiece], [name], [description]) VALUES (204, 1, 212, N'Phantom', N'May not be attacked by turrets or autocannon')
INSERT [dbo].[egs_gamePieceLanguage] ([idGamePieceLanguage], [idLanguage], [idGamePiece], [name], [description]) VALUES (205, 1, 213, N'Scar', N'Roll a separate attack against both compartments in the lane')
INSERT [dbo].[egs_gamePieceLanguage] ([idGamePieceLanguage], [idLanguage], [idGamePiece], [name], [description]) VALUES (206, 1, 214, N'Galleon', N'On a hit place a raider in the compartment that was struck.')
INSERT [dbo].[egs_gamePieceLanguage] ([idGamePieceLanguage], [idLanguage], [idGamePiece], [name], [description]) VALUES (207, 1, 215, N'Crow', N'')
INSERT [dbo].[egs_gamePieceLanguage] ([idGamePieceLanguage], [idLanguage], [idGamePiece], [name], [description]) VALUES (208, 1, 216, N'Gambit', N'')
INSERT [dbo].[egs_gamePieceLanguage] ([idGamePieceLanguage], [idLanguage], [idGamePiece], [name], [description]) VALUES (209, 1, 217, N'Indecision', N'Opponent loses one leadership')
INSERT [dbo].[egs_gamePieceLanguage] ([idGamePieceLanguage], [idLanguage], [idGamePiece], [name], [description]) VALUES (210, 1, 218, N'Infiltraition', N'Move one of your raiders to a different compartment')
INSERT [dbo].[egs_gamePieceLanguage] ([idGamePieceLanguage], [idLanguage], [idGamePiece], [name], [description]) VALUES (211, 1, 219, N'Launch! Launch! Launch!', N'Support craft require 2 less power to launch this turn')
INSERT [dbo].[egs_gamePieceLanguage] ([idGamePieceLanguage], [idLanguage], [idGamePiece], [name], [description]) VALUES (212, 1, 220, N'Bad Luck', N'Your opponent''s next roll this turn is at -3')
INSERT [dbo].[egs_gamePieceLanguage] ([idGamePieceLanguage], [idLanguage], [idGamePiece], [name], [description]) VALUES (213, 1, 221, N'Power Fluctuations', N'Opponent loses 2 power')
INSERT [dbo].[egs_gamePieceLanguage] ([idGamePieceLanguage], [idLanguage], [idGamePiece], [name], [description]) VALUES (214, 1, 222, N'Return to Refuel', N'Opponent must exhaust all fighters or all bombers')
INSERT [dbo].[egs_gamePieceLanguage] ([idGamePieceLanguage], [idLanguage], [idGamePiece], [name], [description]) VALUES (215, 1, 223, N'Ultimate Authority', N'You gain 2 leadership this turn. Remove this card from the game when played')
INSERT [dbo].[egs_gamePieceLanguage] ([idGamePieceLanguage], [idLanguage], [idGamePiece], [name], [description]) VALUES (216, 1, 224, N'Power Surge', N'You gain four power this turn.  Remove this card from the game when played')
INSERT [dbo].[egs_gamePieceLanguage] ([idGamePieceLanguage], [idLanguage], [idGamePiece], [name], [description]) VALUES (217, 1, 225, N'Emergency Repair', N'Remove one damage from any non-destroyed compartment')
INSERT [dbo].[egs_gamePieceLanguage] ([idGamePieceLanguage], [idLanguage], [idGamePiece], [name], [description]) VALUES (218, 1, 226, N'Relentless Pursuit', N'Choose an exhausted fighter.  It may move and attack again')
INSERT [dbo].[egs_gamePieceLanguage] ([idGamePieceLanguage], [idLanguage], [idGamePiece], [name], [description]) VALUES (219, 1, 227, N'Target Locked', N'Play on a fighter or bomber.  That craft gains one accuracy this turn')
INSERT [dbo].[egs_gamePieceLanguage] ([idGamePieceLanguage], [idLanguage], [idGamePiece], [name], [description]) VALUES (220, 1, 228, N'Experimental Payload', N'Play on a bomber.  That bomber deals one bonus damage on a hit but suffers one damage if it misses its attack')
INSERT [dbo].[egs_gamePieceLanguage] ([idGamePieceLanguage], [idLanguage], [idGamePiece], [name], [description]) VALUES (221, 1, 229, N'Cripple Them!', N'Pick a compartment.  Your Batteries and Autocannon gain +1 accuracy vs that Compartment this turn.')
INSERT [dbo].[egs_gamePieceLanguage] ([idGamePieceLanguage], [idLanguage], [idGamePiece], [name], [description]) VALUES (222, 1, 230, N'Give them a Broadside!', N'Your next Battery shot this turn has +2 accuracy')
INSERT [dbo].[egs_gamePieceLanguage] ([idGamePieceLanguage], [idLanguage], [idGamePiece], [name], [description]) VALUES (223, 1, 231, N'Vent the Compartment!', N'Kill a raider of your choice and expend one crew')
INSERT [dbo].[egs_gamePieceLanguage] ([idGamePieceLanguage], [idLanguage], [idGamePiece], [name], [description]) VALUES (224, 1, 232, N'Draw Power from Our Reserves!', N'Your next Battery or Autocannon shot cost 2 less power to fire')
INSERT [dbo].[egs_gamePieceLanguage] ([idGamePieceLanguage], [idLanguage], [idGamePiece], [name], [description]) VALUES (225, 1, 233, N'Overcharge the Main Battery!', N'')
INSERT [dbo].[egs_gamePieceLanguage] ([idGamePieceLanguage], [idLanguage], [idGamePiece], [name], [description]) VALUES (234, 1, 239, N'Neptune Class', N'The batteries of the Neptune class ship have +1')
INSERT [dbo].[egs_gamePieceLanguage] ([idGamePieceLanguage], [idLanguage], [idGamePiece], [name], [description]) VALUES (235, 1, 240, N'Political Officers', N'Whenever you play an Order you may draw a card.')
INSERT [dbo].[egs_gamePieceLanguage] ([idGamePieceLanguage], [idLanguage], [idGamePiece], [name], [description]) VALUES (236, 1, 241, N'Minor Noble', N'Your hand size increased by one')
INSERT [dbo].[egs_gamePieceLanguage] ([idGamePieceLanguage], [idLanguage], [idGamePiece], [name], [description]) VALUES (237, 1, 242, N'Game Board Tile', N'')
SET IDENTITY_INSERT [dbo].[egs_gamePieceLanguage] OFF
SET IDENTITY_INSERT [dbo].[egs_gamePieceSkin] ON 

INSERT [dbo].[egs_gamePieceSkin] ([idGamePieceSkin], [idGamePiece], [smallImg], [bigImg], [minImg], [font1], [font2]) VALUES (142, 150, N'autocannon_small.png', N'autocannon.png', N'autocannon_min.png', N'font1.wot', N'font2.ttf')
INSERT [dbo].[egs_gamePieceSkin] ([idGamePieceSkin], [idGamePiece], [smallImg], [bigImg], [minImg], [font1], [font2]) VALUES (143, 151, N'autocannon_small.png', N'autocannon.png', N'autocannon_min.png', N'font1.wot', N'font2.ttf')
INSERT [dbo].[egs_gamePieceSkin] ([idGamePieceSkin], [idGamePiece], [smallImg], [bigImg], [minImg], [font1], [font2]) VALUES (144, 152, N'autocannon_small.png', N'autocannon.png', N'autocannon_min.png', N'font1.wot', N'font2.ttf')
INSERT [dbo].[egs_gamePieceSkin] ([idGamePieceSkin], [idGamePiece], [smallImg], [bigImg], [minImg], [font1], [font2]) VALUES (145, 153, N'autocannon_small.png', N'autocannon.png', N'autocannon_min.png', N'font1.wot', N'font2.ttf')
INSERT [dbo].[egs_gamePieceSkin] ([idGamePieceSkin], [idGamePiece], [smallImg], [bigImg], [minImg], [font1], [font2]) VALUES (146, 154, N'autocannon_small.png', N'autocannon.png', N'autocannon_min.png', N'font1.wot', N'font2.ttf')
INSERT [dbo].[egs_gamePieceSkin] ([idGamePieceSkin], [idGamePiece], [smallImg], [bigImg], [minImg], [font1], [font2]) VALUES (147, 155, N'autocannon_small.png', N'autocannon.png', N'autocannon_min.png', N'font1.wot', N'font2.ttf')
INSERT [dbo].[egs_gamePieceSkin] ([idGamePieceSkin], [idGamePiece], [smallImg], [bigImg], [minImg], [font1], [font2]) VALUES (148, 156, N'autocannon_small.png', N'autocannon.png', N'autocannon_min.png', N'font1.wot', N'font2.ttf')
INSERT [dbo].[egs_gamePieceSkin] ([idGamePieceSkin], [idGamePiece], [smallImg], [bigImg], [minImg], [font1], [font2]) VALUES (149, 157, N'autocannon_small.png', N'autocannon.png', N'autocannon_min.png', N'font1.wot', N'font2.ttf')
INSERT [dbo].[egs_gamePieceSkin] ([idGamePieceSkin], [idGamePiece], [smallImg], [bigImg], [minImg], [font1], [font2]) VALUES (150, 158, N'autocannon_small.png', N'autocannon.png', N'autocannon_min.png', N'font1.wot', N'font2.ttf')
INSERT [dbo].[egs_gamePieceSkin] ([idGamePieceSkin], [idGamePiece], [smallImg], [bigImg], [minImg], [font1], [font2]) VALUES (151, 159, N'autocannon_small.png', N'autocannon.png', N'autocannon_min.png', N'font1.wot', N'font2.ttf')
INSERT [dbo].[egs_gamePieceSkin] ([idGamePieceSkin], [idGamePiece], [smallImg], [bigImg], [minImg], [font1], [font2]) VALUES (152, 160, N'autocannon_small.png', N'autocannon.png', N'autocannon_min.png', N'font1.wot', N'font2.ttf')
INSERT [dbo].[egs_gamePieceSkin] ([idGamePieceSkin], [idGamePiece], [smallImg], [bigImg], [minImg], [font1], [font2]) VALUES (153, 161, N'autocannon_small.png', N'autocannon.png', N'autocannon_min.png', N'font1.wot', N'font2.ttf')
INSERT [dbo].[egs_gamePieceSkin] ([idGamePieceSkin], [idGamePiece], [smallImg], [bigImg], [minImg], [font1], [font2]) VALUES (154, 162, N'autocannon_small.png', N'autocannon.png', N'autocannon_min.png', N'font1.wot', N'font2.ttf')
INSERT [dbo].[egs_gamePieceSkin] ([idGamePieceSkin], [idGamePiece], [smallImg], [bigImg], [minImg], [font1], [font2]) VALUES (155, 163, N'autocannon_small.png', N'autocannon.png', N'autocannon_min.png', N'font1.wot', N'font2.ttf')
INSERT [dbo].[egs_gamePieceSkin] ([idGamePieceSkin], [idGamePiece], [smallImg], [bigImg], [minImg], [font1], [font2]) VALUES (156, 164, N'autocannon_small.png', N'autocannon.png', N'autocannon_min.png', N'font1.wot', N'font2.ttf')
INSERT [dbo].[egs_gamePieceSkin] ([idGamePieceSkin], [idGamePiece], [smallImg], [bigImg], [minImg], [font1], [font2]) VALUES (157, 165, N'autocannon_small.png', N'autocannon.png', N'autocannon_min.png', N'font1.wot', N'font2.ttf')
INSERT [dbo].[egs_gamePieceSkin] ([idGamePieceSkin], [idGamePiece], [smallImg], [bigImg], [minImg], [font1], [font2]) VALUES (158, 166, N'autocannon_small.png', N'autocannon.png', N'autocannon_min.png', N'font1.wot', N'font2.ttf')
INSERT [dbo].[egs_gamePieceSkin] ([idGamePieceSkin], [idGamePiece], [smallImg], [bigImg], [minImg], [font1], [font2]) VALUES (159, 167, N'autocannon_small.png', N'autocannon.png', N'autocannon_min.png', N'font1.wot', N'font2.ttf')
INSERT [dbo].[egs_gamePieceSkin] ([idGamePieceSkin], [idGamePiece], [smallImg], [bigImg], [minImg], [font1], [font2]) VALUES (160, 168, N'autocannon_small.png', N'autocannon.png', N'autocannon_min.png', N'font1.wot', N'font2.ttf')
INSERT [dbo].[egs_gamePieceSkin] ([idGamePieceSkin], [idGamePiece], [smallImg], [bigImg], [minImg], [font1], [font2]) VALUES (161, 169, N'autocannon_small.png', N'autocannon.png', N'autocannon_min.png', N'font1.wot', N'font2.ttf')
INSERT [dbo].[egs_gamePieceSkin] ([idGamePieceSkin], [idGamePiece], [smallImg], [bigImg], [minImg], [font1], [font2]) VALUES (162, 170, N'autocannon_small.png', N'autocannon.png', N'autocannon_min.png', N'font1.wot', N'font2.ttf')
INSERT [dbo].[egs_gamePieceSkin] ([idGamePieceSkin], [idGamePiece], [smallImg], [bigImg], [minImg], [font1], [font2]) VALUES (163, 171, N'autocannon_small.png', N'autocannon.png', N'autocannon_min.png', N'font1.wot', N'font2.ttf')
INSERT [dbo].[egs_gamePieceSkin] ([idGamePieceSkin], [idGamePiece], [smallImg], [bigImg], [minImg], [font1], [font2]) VALUES (164, 172, N'autocannon_small.png', N'autocannon.png', N'autocannon_min.png', N'font1.wot', N'font2.ttf')
INSERT [dbo].[egs_gamePieceSkin] ([idGamePieceSkin], [idGamePiece], [smallImg], [bigImg], [minImg], [font1], [font2]) VALUES (165, 173, N'autocannon_small.png', N'autocannon.png', N'autocannon_min.png', N'font1.wot', N'font2.ttf')
INSERT [dbo].[egs_gamePieceSkin] ([idGamePieceSkin], [idGamePiece], [smallImg], [bigImg], [minImg], [font1], [font2]) VALUES (166, 174, N'autocannon_small.png', N'autocannon.png', N'autocannon_min.png', N'font1.wot', N'font2.ttf')
INSERT [dbo].[egs_gamePieceSkin] ([idGamePieceSkin], [idGamePiece], [smallImg], [bigImg], [minImg], [font1], [font2]) VALUES (167, 175, N'autocannon_small.png', N'autocannon.png', N'autocannon_min.png', N'font1.wot', N'font2.ttf')
INSERT [dbo].[egs_gamePieceSkin] ([idGamePieceSkin], [idGamePiece], [smallImg], [bigImg], [minImg], [font1], [font2]) VALUES (168, 176, N'autocannon_small.png', N'autocannon.png', N'autocannon_min.png', N'font1.wot', N'font2.ttf')
INSERT [dbo].[egs_gamePieceSkin] ([idGamePieceSkin], [idGamePiece], [smallImg], [bigImg], [minImg], [font1], [font2]) VALUES (169, 177, N'autocannon_small.png', N'autocannon.png', N'autocannon_min.png', N'font1.wot', N'font2.ttf')
INSERT [dbo].[egs_gamePieceSkin] ([idGamePieceSkin], [idGamePiece], [smallImg], [bigImg], [minImg], [font1], [font2]) VALUES (170, 178, N'autocannon_small.png', N'autocannon.png', N'autocannon_min.png', N'font1.wot', N'font2.ttf')
INSERT [dbo].[egs_gamePieceSkin] ([idGamePieceSkin], [idGamePiece], [smallImg], [bigImg], [minImg], [font1], [font2]) VALUES (171, 179, N'autocannon_small.png', N'autocannon.png', N'autocannon_min.png', N'font1.wot', N'font2.ttf')
INSERT [dbo].[egs_gamePieceSkin] ([idGamePieceSkin], [idGamePiece], [smallImg], [bigImg], [minImg], [font1], [font2]) VALUES (172, 180, N'autocannon_small.png', N'autocannon.png', N'autocannon_min.png', N'font1.wot', N'font2.ttf')
INSERT [dbo].[egs_gamePieceSkin] ([idGamePieceSkin], [idGamePiece], [smallImg], [bigImg], [minImg], [font1], [font2]) VALUES (173, 181, N'autocannon_small.png', N'autocannon.png', N'autocannon_min.png', N'font1.wot', N'font2.ttf')
INSERT [dbo].[egs_gamePieceSkin] ([idGamePieceSkin], [idGamePiece], [smallImg], [bigImg], [minImg], [font1], [font2]) VALUES (174, 182, N'autocannon_small.png', N'autocannon.png', N'autocannon_min.png', N'font1.wot', N'font2.ttf')
INSERT [dbo].[egs_gamePieceSkin] ([idGamePieceSkin], [idGamePiece], [smallImg], [bigImg], [minImg], [font1], [font2]) VALUES (175, 183, N'autocannon_small.png', N'autocannon.png', N'autocannon_min.png', N'font1.wot', N'font2.ttf')
INSERT [dbo].[egs_gamePieceSkin] ([idGamePieceSkin], [idGamePiece], [smallImg], [bigImg], [minImg], [font1], [font2]) VALUES (176, 184, N'autocannon_small.png', N'autocannon.png', N'autocannon_min.png', N'font1.wot', N'font2.ttf')
INSERT [dbo].[egs_gamePieceSkin] ([idGamePieceSkin], [idGamePiece], [smallImg], [bigImg], [minImg], [font1], [font2]) VALUES (177, 185, N'autocannon_small.png', N'autocannon.png', N'autocannon_min.png', N'font1.wot', N'font2.ttf')
INSERT [dbo].[egs_gamePieceSkin] ([idGamePieceSkin], [idGamePiece], [smallImg], [bigImg], [minImg], [font1], [font2]) VALUES (178, 186, N'autocannon_small.png', N'autocannon.png', N'autocannon_min.png', N'font1.wot', N'font2.ttf')
INSERT [dbo].[egs_gamePieceSkin] ([idGamePieceSkin], [idGamePiece], [smallImg], [bigImg], [minImg], [font1], [font2]) VALUES (179, 187, N'autocannon_small.png', N'autocannon.png', N'autocannon_min.png', N'font1.wot', N'font2.ttf')
INSERT [dbo].[egs_gamePieceSkin] ([idGamePieceSkin], [idGamePiece], [smallImg], [bigImg], [minImg], [font1], [font2]) VALUES (180, 188, N'autocannon_small.png', N'autocannon.png', N'autocannon_min.png', N'font1.wot', N'font2.ttf')
INSERT [dbo].[egs_gamePieceSkin] ([idGamePieceSkin], [idGamePiece], [smallImg], [bigImg], [minImg], [font1], [font2]) VALUES (181, 189, N'autocannon_small.png', N'autocannon.png', N'autocannon_min.png', N'font1.wot', N'font2.ttf')
INSERT [dbo].[egs_gamePieceSkin] ([idGamePieceSkin], [idGamePiece], [smallImg], [bigImg], [minImg], [font1], [font2]) VALUES (182, 190, N'autocannon_small.png', N'autocannon.png', N'autocannon_min.png', N'font1.wot', N'font2.ttf')
INSERT [dbo].[egs_gamePieceSkin] ([idGamePieceSkin], [idGamePiece], [smallImg], [bigImg], [minImg], [font1], [font2]) VALUES (183, 191, N'autocannon_small.png', N'autocannon.png', N'autocannon_min.png', N'font1.wot', N'font2.ttf')
INSERT [dbo].[egs_gamePieceSkin] ([idGamePieceSkin], [idGamePiece], [smallImg], [bigImg], [minImg], [font1], [font2]) VALUES (184, 192, N'autocannon_small.png', N'autocannon.png', N'autocannon_min.png', N'font1.wot', N'font2.ttf')
INSERT [dbo].[egs_gamePieceSkin] ([idGamePieceSkin], [idGamePiece], [smallImg], [bigImg], [minImg], [font1], [font2]) VALUES (185, 193, N'autocannon_small.png', N'autocannon.png', N'autocannon_min.png', N'font1.wot', N'font2.ttf')
INSERT [dbo].[egs_gamePieceSkin] ([idGamePieceSkin], [idGamePiece], [smallImg], [bigImg], [minImg], [font1], [font2]) VALUES (186, 194, N'autocannon_small.png', N'autocannon.png', N'autocannon_min.png', N'font1.wot', N'font2.ttf')
INSERT [dbo].[egs_gamePieceSkin] ([idGamePieceSkin], [idGamePiece], [smallImg], [bigImg], [minImg], [font1], [font2]) VALUES (187, 195, N'autocannon_small.png', N'autocannon.png', N'autocannon_min.png', N'font1.wot', N'font2.ttf')
INSERT [dbo].[egs_gamePieceSkin] ([idGamePieceSkin], [idGamePiece], [smallImg], [bigImg], [minImg], [font1], [font2]) VALUES (188, 196, N'autocannon_small.png', N'autocannon.png', N'autocannon_min.png', N'font1.wot', N'font2.ttf')
INSERT [dbo].[egs_gamePieceSkin] ([idGamePieceSkin], [idGamePiece], [smallImg], [bigImg], [minImg], [font1], [font2]) VALUES (189, 197, N'autocannon_small.png', N'autocannon.png', N'autocannon_min.png', N'font1.wot', N'font2.ttf')
INSERT [dbo].[egs_gamePieceSkin] ([idGamePieceSkin], [idGamePiece], [smallImg], [bigImg], [minImg], [font1], [font2]) VALUES (190, 198, N'autocannon_small.png', N'autocannon.png', N'autocannon_min.png', N'font1.wot', N'font2.ttf')
INSERT [dbo].[egs_gamePieceSkin] ([idGamePieceSkin], [idGamePiece], [smallImg], [bigImg], [minImg], [font1], [font2]) VALUES (191, 199, N'autocannon_small.png', N'autocannon.png', N'autocannon_min.png', N'font1.wot', N'font2.ttf')
INSERT [dbo].[egs_gamePieceSkin] ([idGamePieceSkin], [idGamePiece], [smallImg], [bigImg], [minImg], [font1], [font2]) VALUES (192, 200, N'autocannon_small.png', N'autocannon.png', N'autocannon_min.png', N'font1.wot', N'font2.ttf')
INSERT [dbo].[egs_gamePieceSkin] ([idGamePieceSkin], [idGamePiece], [smallImg], [bigImg], [minImg], [font1], [font2]) VALUES (193, 201, N'autocannon_small.png', N'autocannon.png', N'autocannon_min.png', N'font1.wot', N'font2.ttf')
INSERT [dbo].[egs_gamePieceSkin] ([idGamePieceSkin], [idGamePiece], [smallImg], [bigImg], [minImg], [font1], [font2]) VALUES (194, 202, N'autocannon_small.png', N'autocannon.png', N'autocannon_min.png', N'font1.wot', N'font2.ttf')
INSERT [dbo].[egs_gamePieceSkin] ([idGamePieceSkin], [idGamePiece], [smallImg], [bigImg], [minImg], [font1], [font2]) VALUES (195, 203, N'autocannon_small.png', N'autocannon.png', N'autocannon_min.png', N'font1.wot', N'font2.ttf')
INSERT [dbo].[egs_gamePieceSkin] ([idGamePieceSkin], [idGamePiece], [smallImg], [bigImg], [minImg], [font1], [font2]) VALUES (196, 204, N'autocannon_small.png', N'autocannon.png', N'autocannon_min.png', N'font1.wot', N'font2.ttf')
INSERT [dbo].[egs_gamePieceSkin] ([idGamePieceSkin], [idGamePiece], [smallImg], [bigImg], [minImg], [font1], [font2]) VALUES (197, 205, N'autocannon_small.png', N'autocannon.png', N'autocannon_min.png', N'font1.wot', N'font2.ttf')
INSERT [dbo].[egs_gamePieceSkin] ([idGamePieceSkin], [idGamePiece], [smallImg], [bigImg], [minImg], [font1], [font2]) VALUES (198, 206, N'autocannon_small.png', N'autocannon.png', N'autocannon_min.png', N'font1.wot', N'font2.ttf')
INSERT [dbo].[egs_gamePieceSkin] ([idGamePieceSkin], [idGamePiece], [smallImg], [bigImg], [minImg], [font1], [font2]) VALUES (199, 207, N'autocannon_small.png', N'autocannon.png', N'autocannon_min.png', N'font1.wot', N'font2.ttf')
INSERT [dbo].[egs_gamePieceSkin] ([idGamePieceSkin], [idGamePiece], [smallImg], [bigImg], [minImg], [font1], [font2]) VALUES (200, 208, N'autocannon_small.png', N'autocannon.png', N'autocannon_min.png', N'font1.wot', N'font2.ttf')
INSERT [dbo].[egs_gamePieceSkin] ([idGamePieceSkin], [idGamePiece], [smallImg], [bigImg], [minImg], [font1], [font2]) VALUES (201, 209, N'autocannon_small.png', N'autocannon.png', N'autocannon_min.png', N'font1.wot', N'font2.ttf')
INSERT [dbo].[egs_gamePieceSkin] ([idGamePieceSkin], [idGamePiece], [smallImg], [bigImg], [minImg], [font1], [font2]) VALUES (202, 210, N'autocannon_small.png', N'autocannon.png', N'autocannon_min.png', N'font1.wot', N'font2.ttf')
INSERT [dbo].[egs_gamePieceSkin] ([idGamePieceSkin], [idGamePiece], [smallImg], [bigImg], [minImg], [font1], [font2]) VALUES (203, 211, N'autocannon_small.png', N'autocannon.png', N'autocannon_min.png', N'font1.wot', N'font2.ttf')
INSERT [dbo].[egs_gamePieceSkin] ([idGamePieceSkin], [idGamePiece], [smallImg], [bigImg], [minImg], [font1], [font2]) VALUES (204, 212, N'autocannon_small.png', N'autocannon.png', N'autocannon_min.png', N'font1.wot', N'font2.ttf')
INSERT [dbo].[egs_gamePieceSkin] ([idGamePieceSkin], [idGamePiece], [smallImg], [bigImg], [minImg], [font1], [font2]) VALUES (205, 213, N'autocannon_small.png', N'autocannon.png', N'autocannon_min.png', N'font1.wot', N'font2.ttf')
INSERT [dbo].[egs_gamePieceSkin] ([idGamePieceSkin], [idGamePiece], [smallImg], [bigImg], [minImg], [font1], [font2]) VALUES (206, 214, N'autocannon_small.png', N'autocannon.png', N'autocannon_min.png', N'font1.wot', N'font2.ttf')
INSERT [dbo].[egs_gamePieceSkin] ([idGamePieceSkin], [idGamePiece], [smallImg], [bigImg], [minImg], [font1], [font2]) VALUES (207, 215, N'autocannon_small.png', N'autocannon.png', N'autocannon_min.png', N'font1.wot', N'font2.ttf')
INSERT [dbo].[egs_gamePieceSkin] ([idGamePieceSkin], [idGamePiece], [smallImg], [bigImg], [minImg], [font1], [font2]) VALUES (208, 216, N'autocannon_small.png', N'autocannon.png', N'autocannon_min.png', N'font1.wot', N'font2.ttf')
INSERT [dbo].[egs_gamePieceSkin] ([idGamePieceSkin], [idGamePiece], [smallImg], [bigImg], [minImg], [font1], [font2]) VALUES (209, 217, N'autocannon_small.png', N'autocannon.png', N'autocannon_min.png', N'font1.wot', N'font2.ttf')
INSERT [dbo].[egs_gamePieceSkin] ([idGamePieceSkin], [idGamePiece], [smallImg], [bigImg], [minImg], [font1], [font2]) VALUES (210, 218, N'autocannon_small.png', N'autocannon.png', N'autocannon_min.png', N'font1.wot', N'font2.ttf')
INSERT [dbo].[egs_gamePieceSkin] ([idGamePieceSkin], [idGamePiece], [smallImg], [bigImg], [minImg], [font1], [font2]) VALUES (211, 219, N'autocannon_small.png', N'autocannon.png', N'autocannon_min.png', N'font1.wot', N'font2.ttf')
INSERT [dbo].[egs_gamePieceSkin] ([idGamePieceSkin], [idGamePiece], [smallImg], [bigImg], [minImg], [font1], [font2]) VALUES (212, 220, N'autocannon_small.png', N'autocannon.png', N'autocannon_min.png', N'font1.wot', N'font2.ttf')
INSERT [dbo].[egs_gamePieceSkin] ([idGamePieceSkin], [idGamePiece], [smallImg], [bigImg], [minImg], [font1], [font2]) VALUES (213, 221, N'autocannon_small.png', N'autocannon.png', N'autocannon_min.png', N'font1.wot', N'font2.ttf')
INSERT [dbo].[egs_gamePieceSkin] ([idGamePieceSkin], [idGamePiece], [smallImg], [bigImg], [minImg], [font1], [font2]) VALUES (214, 222, N'autocannon_small.png', N'autocannon.png', N'autocannon_min.png', N'font1.wot', N'font2.ttf')
INSERT [dbo].[egs_gamePieceSkin] ([idGamePieceSkin], [idGamePiece], [smallImg], [bigImg], [minImg], [font1], [font2]) VALUES (215, 223, N'autocannon_small.png', N'autocannon.png', N'autocannon_min.png', N'font1.wot', N'font2.ttf')
INSERT [dbo].[egs_gamePieceSkin] ([idGamePieceSkin], [idGamePiece], [smallImg], [bigImg], [minImg], [font1], [font2]) VALUES (216, 224, N'autocannon_small.png', N'autocannon.png', N'autocannon_min.png', N'font1.wot', N'font2.ttf')
INSERT [dbo].[egs_gamePieceSkin] ([idGamePieceSkin], [idGamePiece], [smallImg], [bigImg], [minImg], [font1], [font2]) VALUES (217, 225, N'autocannon_small.png', N'autocannon.png', N'autocannon_min.png', N'font1.wot', N'font2.ttf')
INSERT [dbo].[egs_gamePieceSkin] ([idGamePieceSkin], [idGamePiece], [smallImg], [bigImg], [minImg], [font1], [font2]) VALUES (218, 226, N'autocannon_small.png', N'autocannon.png', N'autocannon_min.png', N'font1.wot', N'font2.ttf')
INSERT [dbo].[egs_gamePieceSkin] ([idGamePieceSkin], [idGamePiece], [smallImg], [bigImg], [minImg], [font1], [font2]) VALUES (219, 227, N'autocannon_small.png', N'autocannon.png', N'autocannon_min.png', N'font1.wot', N'font2.ttf')
INSERT [dbo].[egs_gamePieceSkin] ([idGamePieceSkin], [idGamePiece], [smallImg], [bigImg], [minImg], [font1], [font2]) VALUES (220, 228, N'autocannon_small.png', N'autocannon.png', N'autocannon_min.png', N'font1.wot', N'font2.ttf')
INSERT [dbo].[egs_gamePieceSkin] ([idGamePieceSkin], [idGamePiece], [smallImg], [bigImg], [minImg], [font1], [font2]) VALUES (221, 229, N'autocannon_small.png', N'autocannon.png', N'autocannon_min.png', N'font1.wot', N'font2.ttf')
INSERT [dbo].[egs_gamePieceSkin] ([idGamePieceSkin], [idGamePiece], [smallImg], [bigImg], [minImg], [font1], [font2]) VALUES (222, 230, N'autocannon_small.png', N'autocannon.png', N'autocannon_min.png', N'font1.wot', N'font2.ttf')
INSERT [dbo].[egs_gamePieceSkin] ([idGamePieceSkin], [idGamePiece], [smallImg], [bigImg], [minImg], [font1], [font2]) VALUES (223, 231, N'autocannon_small.png', N'autocannon.png', N'autocannon_min.png', N'font1.wot', N'font2.ttf')
INSERT [dbo].[egs_gamePieceSkin] ([idGamePieceSkin], [idGamePiece], [smallImg], [bigImg], [minImg], [font1], [font2]) VALUES (224, 232, N'autocannon_small.png', N'autocannon.png', N'autocannon_min.png', N'font1.wot', N'font2.ttf')
INSERT [dbo].[egs_gamePieceSkin] ([idGamePieceSkin], [idGamePiece], [smallImg], [bigImg], [minImg], [font1], [font2]) VALUES (225, 233, N'autocannon_small.png', N'autocannon.png', N'autocannon_min.png', N'font1.wot', N'font2.ttf')
INSERT [dbo].[egs_gamePieceSkin] ([idGamePieceSkin], [idGamePiece], [smallImg], [bigImg], [minImg], [font1], [font2]) VALUES (234, 239, N'ship-classification-icon.png', N'ship-class-neptune.png', NULL, N'font1.wot', N'font2.ttf')
INSERT [dbo].[egs_gamePieceSkin] ([idGamePieceSkin], [idGamePiece], [smallImg], [bigImg], [minImg], [font1], [font2]) VALUES (235, 240, N'specialists-icon.png', N'political_officer.png', NULL, N'font1.wot', N'font2.ttf')
INSERT [dbo].[egs_gamePieceSkin] ([idGamePieceSkin], [idGamePiece], [smallImg], [bigImg], [minImg], [font1], [font2]) VALUES (236, 241, N'specialists-icon.png', N'minor_noble.png', NULL, N'font1.wot', N'font2.ttf')
INSERT [dbo].[egs_gamePieceSkin] ([idGamePieceSkin], [idGamePiece], [smallImg], [bigImg], [minImg], [font1], [font2]) VALUES (237, 242, N'boardTile_small.png', N'boardTile.png', NULL, N'font1.wot', N'font2.ttf')
SET IDENTITY_INSERT [dbo].[egs_gamePieceSkin] OFF
SET IDENTITY_INSERT [dbo].[egs_gamePieceType] ON 

INSERT [dbo].[egs_gamePieceType] ([idGamePieceType], [idGame]) VALUES (1, 1)
INSERT [dbo].[egs_gamePieceType] ([idGamePieceType], [idGame]) VALUES (2, 1)
INSERT [dbo].[egs_gamePieceType] ([idGamePieceType], [idGame]) VALUES (3, 1)
INSERT [dbo].[egs_gamePieceType] ([idGamePieceType], [idGame]) VALUES (4, 1)
INSERT [dbo].[egs_gamePieceType] ([idGamePieceType], [idGame]) VALUES (5, 1)
INSERT [dbo].[egs_gamePieceType] ([idGamePieceType], [idGame]) VALUES (6, 1)
INSERT [dbo].[egs_gamePieceType] ([idGamePieceType], [idGame]) VALUES (7, 1)
INSERT [dbo].[egs_gamePieceType] ([idGamePieceType], [idGame]) VALUES (8, 1)
INSERT [dbo].[egs_gamePieceType] ([idGamePieceType], [idGame]) VALUES (10, 1)
SET IDENTITY_INSERT [dbo].[egs_gamePieceType] OFF
SET IDENTITY_INSERT [dbo].[egs_gamePieceTypeLanguage] ON 

INSERT [dbo].[egs_gamePieceTypeLanguage] ([idGamePIeceTypeLanguage], [idGamePieceType], [idLanguage], [pieceType]) VALUES (1, 1, 1, N'Compartment')
INSERT [dbo].[egs_gamePieceTypeLanguage] ([idGamePIeceTypeLanguage], [idGamePieceType], [idLanguage], [pieceType]) VALUES (2, 2, 1, N'Event')
INSERT [dbo].[egs_gamePieceTypeLanguage] ([idGamePIeceTypeLanguage], [idGamePieceType], [idLanguage], [pieceType]) VALUES (3, 3, 1, N'Misfortune')
INSERT [dbo].[egs_gamePieceTypeLanguage] ([idGamePIeceTypeLanguage], [idGamePieceType], [idLanguage], [pieceType]) VALUES (4, 4, 1, N'Order')
INSERT [dbo].[egs_gamePieceTypeLanguage] ([idGamePIeceTypeLanguage], [idGamePieceType], [idLanguage], [pieceType]) VALUES (5, 5, 1, N'Ordnance')
INSERT [dbo].[egs_gamePieceTypeLanguage] ([idGamePIeceTypeLanguage], [idGamePieceType], [idLanguage], [pieceType]) VALUES (6, 6, 1, N'Support Craft')
INSERT [dbo].[egs_gamePieceTypeLanguage] ([idGamePIeceTypeLanguage], [idGamePieceType], [idLanguage], [pieceType]) VALUES (7, 7, 1, N'Modifier')
INSERT [dbo].[egs_gamePieceTypeLanguage] ([idGamePIeceTypeLanguage], [idGamePieceType], [idLanguage], [pieceType]) VALUES (8, 8, 1, N'Ship Classification')
INSERT [dbo].[egs_gamePieceTypeLanguage] ([idGamePIeceTypeLanguage], [idGamePieceType], [idLanguage], [pieceType]) VALUES (11, 10, 1, N'Game Board Tile')
SET IDENTITY_INSERT [dbo].[egs_gamePieceTypeLanguage] OFF
SET IDENTITY_INSERT [dbo].[egs_gameSession] ON 

INSERT [dbo].[egs_gameSession] ([idGameSession], [idGame], [isActive], [startTime], [endTime], [lastTurn], [thumbnail]) VALUES (10015, 1, 1, CAST(N'2015-11-17 21:24:29.527' AS DateTime), NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[egs_gameSession] OFF
SET IDENTITY_INSERT [dbo].[egs_gameSessionPlayer] ON 

INSERT [dbo].[egs_gameSessionPlayer] ([idGameSessionPlayer], [idPlayer], [idGameSession], [isCreator], [hasAccepted]) VALUES (7, 1, 10015, 1, 1)
INSERT [dbo].[egs_gameSessionPlayer] ([idGameSessionPlayer], [idPlayer], [idGameSession], [isCreator], [hasAccepted]) VALUES (8, 2, 10015, 0, 1)
SET IDENTITY_INSERT [dbo].[egs_gameSessionPlayer] OFF
SET IDENTITY_INSERT [dbo].[egs_gameSessionPlayerCollection] ON 

INSERT [dbo].[egs_gameSessionPlayerCollection] ([idGameSessionPlayerCollection], [idGameSessionPlayer], [idGamePieceCollectionPlayer]) VALUES (22, 7, 1)
INSERT [dbo].[egs_gameSessionPlayerCollection] ([idGameSessionPlayerCollection], [idGameSessionPlayer], [idGamePieceCollectionPlayer]) VALUES (23, 7, 2)
INSERT [dbo].[egs_gameSessionPlayerCollection] ([idGameSessionPlayerCollection], [idGameSessionPlayer], [idGamePieceCollectionPlayer]) VALUES (24, 7, 3)
INSERT [dbo].[egs_gameSessionPlayerCollection] ([idGameSessionPlayerCollection], [idGameSessionPlayer], [idGamePieceCollectionPlayer]) VALUES (25, 8, 4)
INSERT [dbo].[egs_gameSessionPlayerCollection] ([idGameSessionPlayerCollection], [idGameSessionPlayer], [idGamePieceCollectionPlayer]) VALUES (26, 8, 5)
INSERT [dbo].[egs_gameSessionPlayerCollection] ([idGameSessionPlayerCollection], [idGameSessionPlayer], [idGamePieceCollectionPlayer]) VALUES (27, 8, 6)
SET IDENTITY_INSERT [dbo].[egs_gameSessionPlayerCollection] OFF
SET IDENTITY_INSERT [dbo].[egs_language] ON 

INSERT [dbo].[egs_language] ([idLanguage], [language], [culture]) VALUES (1, N'English', N'en-US')
INSERT [dbo].[egs_language] ([idLanguage], [language], [culture]) VALUES (2, N'Spanish', N'es')
INSERT [dbo].[egs_language] ([idLanguage], [language], [culture]) VALUES (3, N'German', N'de')
INSERT [dbo].[egs_language] ([idLanguage], [language], [culture]) VALUES (4, N'Chinese', N'zh-CHS')
INSERT [dbo].[egs_language] ([idLanguage], [language], [culture]) VALUES (5, N'French', N'fr')
SET IDENTITY_INSERT [dbo].[egs_language] OFF
SET IDENTITY_INSERT [dbo].[egs_player] ON 

INSERT [dbo].[egs_player] ([idUser], [idPlayer], [firstname], [lastname], [city], [idRegion], [nickName]) VALUES (N'bef1b803-9dd5-4072-90fb-0144420e1a53', 1, N'Marc', N'Arbesman', N'Marblehead', 21, N'willcode4food')
INSERT [dbo].[egs_player] ([idUser], [idPlayer], [firstname], [lastname], [city], [idRegion], [nickName]) VALUES (N'0cc13156-af17-469d-9795-2aadab6a8f8e', 2, N'Josh', N'Pearl', N'West Palm Beach', 9, N'Frig9000')
INSERT [dbo].[egs_player] ([idUser], [idPlayer], [firstname], [lastname], [city], [idRegion], [nickName]) VALUES (N'8ddd55d6-a5d6-4410-966f-07f837edd6b6', 10004, N'Anonymous', N'User', NULL, 21, N'Action Frank')
INSERT [dbo].[egs_player] ([idUser], [idPlayer], [firstname], [lastname], [city], [idRegion], [nickName]) VALUES (N'd023c384-2059-42b9-90b5-5097fdf2ba2b', 10005, N'Harvey', N'Steinbergthalman', N'Houston', 21, N'BillyDee420')
SET IDENTITY_INSERT [dbo].[egs_player] OFF
SET IDENTITY_INSERT [dbo].[egs_refreshTokens] ON 

INSERT [dbo].[egs_refreshTokens] ([idRefeshToken], [idAspNetUser], [token], [protectedTicket], [issuedUTC], [expiredUTC], [clientIdentifier]) VALUES (300673, N'bef1b803-9dd5-4072-90fb-0144420e1a53', N'r/JyVOUBZt8pH2tD2X7sv7pjfTxUlN+ZGOsg9CXiCQ4=', N'_EcaDipIIuCAanhEmoMrtLDbCC0Df5mQmuVThBBUn-w0LOftNBl_ULebKcEiS7G308e098cAuI-nADXIiTxbntlyV0CbdtvcyJGJSIKSGWf0OIIikCIMR58xScQP1sBT9X4HzRPkxxypUSxOT29tu5BKHRjDfFU3VfGnUpJyIAxxW8I7-c4No0vAJuyKxdx8cctKkUYby1Knzca-nqFF7f-lFhHuNLnfBfEeM7ijnNqdkwwlNPpMgVUb5VNxvkNuIsyBzLIn4QX-Eav0m-5yqb3hxIqnY6-icle7LdnS7xCbOrXt14GtA5S4e03zWRI6br3uRq_5lKjSDdHM0WnxnLRN-1IXU8D2yZNq7fo7d3sMgvHIkov02lLdyFgqAeVdzxGFMBLPUoVKx4ifV7XjZiBZ7XX9RCVK0jj_8jxd18oJTd0n8-2tb2RoVF2r2HoO', CAST(N'2015-10-02 19:00:27.140' AS DateTime), CAST(N'2015-10-12 19:00:27.140' AS DateTime), N'2')
INSERT [dbo].[egs_refreshTokens] ([idRefeshToken], [idAspNetUser], [token], [protectedTicket], [issuedUTC], [expiredUTC], [clientIdentifier]) VALUES (300710, NULL, N'2DWOuj25Q+hK9e5h6aPntt/T4G4vFf8D6Sp/+irxaqQ=', N'YrnTl4P5QGWe2D5F6DAV-rAERGqeMqDkyyESVM1m5tvdctSf4ZRX-L9qjDcaez_i1bU-mQFTJfEBU3v_C3xpLnyrhB8twxUq0quHRzoQ9AMkZ4jyZy5rLC_DlDC_cZAM3dISudoEItq-rZopjEjCQdlRvYIYQOojQQjlJyUJWEm4k1Rv83VfeUG4Z7JlOmKkOzMLyx2JxDUyklCT-fSEG8ZL2kRcTFoXyYU7U_KDwGXxAi5qA5d7S4Qk5IDccvJXZwvWP2YXTUBP4BYVCWgW5QvAGsQdBUcT23UJJpZuHbCblpqfbZpRG8JfdCKo9apUJRN1xe_iDrya4QJ7nqO8rg', CAST(N'2015-10-09 02:08:23.280' AS DateTime), CAST(N'2015-10-19 02:08:23.280' AS DateTime), N'2')
INSERT [dbo].[egs_refreshTokens] ([idRefeshToken], [idAspNetUser], [token], [protectedTicket], [issuedUTC], [expiredUTC], [clientIdentifier]) VALUES (300731, N'208cf252-431d-4c4c-85c3-b0541f766026', N'jvQaqb6mksn9xi630GyU1An7AHe0/7UR5Mr3hjn4aUA=', N'eowkjn3sMY5def0hZNgRueU9W2v0B1-BrdQIhLDeMWwoSaKMLIP-RLAmSzz7_pgWrQtvPqqq-Hy0fEeDxBX9aGyCiqTxEzrc2AD_-zXqWeDenvz0c5huRW8ClVfg5d-uF8TiyGJQulbngIXDKsGqeVeKsagQhUPXmS-TLuzV8UCKfJzo_NhWEHFNubQSR02AynwKtQWLBeY_rKAbvwi9LGTNBSXVF3I1QEtO5kzdtuzhc4-MAYJOrwYZybmAcMBsp3wbkkYcrveDH7lsAyWSYeMD3TBkoDzYLN5Yx0eL5Kuxk7956RguTTghoKih_SmncEqk5_HomUlG-9zTqdRGWMIwDPz79hfSCke6qD_xGkR6pMv-pr_4KcuCGcc-S6ftZ8A3vAgZTcaWv-q5gVvLPsow29P5-h8fSY_3ArFEKVBY4yT61XWwVxH3oh2eTA7l', CAST(N'2015-11-01 16:25:16.523' AS DateTime), CAST(N'2015-11-06 16:25:16.523' AS DateTime), N'3')
INSERT [dbo].[egs_refreshTokens] ([idRefeshToken], [idAspNetUser], [token], [protectedTicket], [issuedUTC], [expiredUTC], [clientIdentifier]) VALUES (300742, N'bef1b803-9dd5-4072-90fb-0144420e1a53', N'n8fRk9GxB3WFnToOlTXmaEPq10tD2YPXxQDgyOs4ghE=', N'ts41zuGp7eOa1WDZxqo5O82NcL63I8TNBuJM5zCooymJmJdp2vl5rrLs7jAoC5NCe6MyL45CvuqL3X7hvAnSNV3uYjGrKG-R3QCKoRptdqHYEYoeBUiiZoS1Xw__VqM5GHE5buMJCT2Y42QjlSxiBDb1phTJBlbRGlqXuYbncdmUT6LTb0Z7JavJlouC8lox-JMvpFGLykUxFHu8_VSdQ5A44xAFDnjT9uWNs-055bhe9ZlHsdI_AhBE3NsQuTNoQy6Tp3uojad6K3zpvYuqX5ZyOhAw6JlLafS72r6LJELsU8f6cBBpmsuor1GjlTeN0WOCyEtGQ_q2X-6NEELwXPZWMhIiYJdJlGLHP-J5DxSeCiB1mIdSJbRIRSOkRuNny4dBUZk6wY7457EWzEv-22rg2Bw4yq8_jcG4V2BF52FhbYrw-i1ESeG2YHDZlSLIfHInUG7ibKCIz-dAT1l5gg', CAST(N'2015-11-19 02:35:37.207' AS DateTime), CAST(N'2015-11-24 02:35:37.207' AS DateTime), N'3')
INSERT [dbo].[egs_refreshTokens] ([idRefeshToken], [idAspNetUser], [token], [protectedTicket], [issuedUTC], [expiredUTC], [clientIdentifier]) VALUES (300744, N'0cc13156-af17-469d-9795-2aadab6a8f8e', N'wsXQBH33cbMFAhV/l2bKTsWet8EpN2wyMpT0X5i2FG8=', N'Vd367ZAzwrvYhkLyP3wGdLNCmHlSzfBu-WsO0u9n-j32t_AwmmdG2RQYRk2vL1Ub_QT4daxXJm50s7J61u-pV8-teFL5VuM5t3av02WsQIGOERZdyrHwQ2-CtqnsuocM9o4btXk7t452ihNC2vSzZvr9s8-mwJlkIRujXWcgbgehC5zdtbAl5uodWx6soOuaYvmG1VWA4dcV2FYyzA9f7zhXnqWAC-MMgg0aXtxvvrcSKg3AFJhbyoFZS_xcPYCSLk8R_JJyyZRbH6G8m829DV3eI9JzJfrl_cS6IKYPPSlmVXABSXsNbQMZoH4287SEC9RbQn66x4G1e2ab4hYdfVA5fDK3NOcwiXALHG5TF-oXus6wyhLVDOqgknn-tUwPW_lu9oJjALQdgzhYDBFOhVx-_RG8vbA2kUUNeAr6QafpT73F3C1ANAlKLzLyMRlwAhRXhIA_5aQ-aSCJIXYC9kWptxcsR8BUbIUP3oWDIXo', CAST(N'2015-11-27 00:57:24.913' AS DateTime), CAST(N'2015-12-02 00:57:24.913' AS DateTime), N'3')
SET IDENTITY_INSERT [dbo].[egs_refreshTokens] OFF
SET IDENTITY_INSERT [dbo].[egs_region] ON 

INSERT [dbo].[egs_region] ([idRegion], [idCountry], [abbreviation], [name]) VALUES (1, 1, N'AL', N'Alabama')
INSERT [dbo].[egs_region] ([idRegion], [idCountry], [abbreviation], [name]) VALUES (2, 1, N'AK', N'Alaska')
INSERT [dbo].[egs_region] ([idRegion], [idCountry], [abbreviation], [name]) VALUES (3, 1, N'AZ', N'Arizona')
INSERT [dbo].[egs_region] ([idRegion], [idCountry], [abbreviation], [name]) VALUES (4, 1, N'AR', N'Arkansas')
INSERT [dbo].[egs_region] ([idRegion], [idCountry], [abbreviation], [name]) VALUES (5, 1, N'CA', N'California')
INSERT [dbo].[egs_region] ([idRegion], [idCountry], [abbreviation], [name]) VALUES (6, 1, N'CO', N'Colorado')
INSERT [dbo].[egs_region] ([idRegion], [idCountry], [abbreviation], [name]) VALUES (7, 1, N'CT', N'Connecticut')
INSERT [dbo].[egs_region] ([idRegion], [idCountry], [abbreviation], [name]) VALUES (8, 1, N'DE', N'Delaware')
INSERT [dbo].[egs_region] ([idRegion], [idCountry], [abbreviation], [name]) VALUES (9, 1, N'FL', N'Florida')
INSERT [dbo].[egs_region] ([idRegion], [idCountry], [abbreviation], [name]) VALUES (10, 1, N'GA', N'Georgia')
INSERT [dbo].[egs_region] ([idRegion], [idCountry], [abbreviation], [name]) VALUES (11, 1, N'HI', N'Hawaii')
INSERT [dbo].[egs_region] ([idRegion], [idCountry], [abbreviation], [name]) VALUES (12, 1, N'ID', N'Idaho')
INSERT [dbo].[egs_region] ([idRegion], [idCountry], [abbreviation], [name]) VALUES (13, 1, N'IL', N'Illinois')
INSERT [dbo].[egs_region] ([idRegion], [idCountry], [abbreviation], [name]) VALUES (14, 1, N'IN', N'Indiana')
INSERT [dbo].[egs_region] ([idRegion], [idCountry], [abbreviation], [name]) VALUES (15, 1, N'IA', N'Iowa')
INSERT [dbo].[egs_region] ([idRegion], [idCountry], [abbreviation], [name]) VALUES (16, 1, N'KS', N'Kansas')
INSERT [dbo].[egs_region] ([idRegion], [idCountry], [abbreviation], [name]) VALUES (17, 1, N'KY', N'Kentucky')
INSERT [dbo].[egs_region] ([idRegion], [idCountry], [abbreviation], [name]) VALUES (18, 1, N'LA', N'Louisiana')
INSERT [dbo].[egs_region] ([idRegion], [idCountry], [abbreviation], [name]) VALUES (19, 1, N'ME', N'Maine')
INSERT [dbo].[egs_region] ([idRegion], [idCountry], [abbreviation], [name]) VALUES (20, 1, N'MD', N'Maryland')
INSERT [dbo].[egs_region] ([idRegion], [idCountry], [abbreviation], [name]) VALUES (21, 1, N'MA', N'Massachusetts')
INSERT [dbo].[egs_region] ([idRegion], [idCountry], [abbreviation], [name]) VALUES (22, 1, N'MI', N'Michigan')
INSERT [dbo].[egs_region] ([idRegion], [idCountry], [abbreviation], [name]) VALUES (23, 1, N'MN', N'Minnesota')
INSERT [dbo].[egs_region] ([idRegion], [idCountry], [abbreviation], [name]) VALUES (24, 1, N'MS', N'Mississippi')
INSERT [dbo].[egs_region] ([idRegion], [idCountry], [abbreviation], [name]) VALUES (25, 1, N'MO', N'Missouri')
INSERT [dbo].[egs_region] ([idRegion], [idCountry], [abbreviation], [name]) VALUES (26, 1, N'MT', N'Montana')
INSERT [dbo].[egs_region] ([idRegion], [idCountry], [abbreviation], [name]) VALUES (27, 1, N'NE', N'Nebraska')
INSERT [dbo].[egs_region] ([idRegion], [idCountry], [abbreviation], [name]) VALUES (28, 1, N'NV', N'Nevada')
INSERT [dbo].[egs_region] ([idRegion], [idCountry], [abbreviation], [name]) VALUES (29, 1, N'NH', N'New Hampshire')
INSERT [dbo].[egs_region] ([idRegion], [idCountry], [abbreviation], [name]) VALUES (30, 1, N'NJ', N'New Jersey')
INSERT [dbo].[egs_region] ([idRegion], [idCountry], [abbreviation], [name]) VALUES (31, 1, N'NM', N'New Mexico')
INSERT [dbo].[egs_region] ([idRegion], [idCountry], [abbreviation], [name]) VALUES (32, 1, N'NY', N'New York')
INSERT [dbo].[egs_region] ([idRegion], [idCountry], [abbreviation], [name]) VALUES (33, 1, N'NC', N'North Carolina')
INSERT [dbo].[egs_region] ([idRegion], [idCountry], [abbreviation], [name]) VALUES (34, 1, N'ND', N'North Dakota')
INSERT [dbo].[egs_region] ([idRegion], [idCountry], [abbreviation], [name]) VALUES (35, 1, N'OH', N'Ohio')
INSERT [dbo].[egs_region] ([idRegion], [idCountry], [abbreviation], [name]) VALUES (36, 1, N'OK', N'Oklahoma')
INSERT [dbo].[egs_region] ([idRegion], [idCountry], [abbreviation], [name]) VALUES (37, 1, N'OR', N'Oregon')
INSERT [dbo].[egs_region] ([idRegion], [idCountry], [abbreviation], [name]) VALUES (38, 1, N'PA', N'Pennsylvania')
INSERT [dbo].[egs_region] ([idRegion], [idCountry], [abbreviation], [name]) VALUES (39, 1, N'RI', N'Rhode Island')
INSERT [dbo].[egs_region] ([idRegion], [idCountry], [abbreviation], [name]) VALUES (40, 1, N'SC', N'South Carolina')
INSERT [dbo].[egs_region] ([idRegion], [idCountry], [abbreviation], [name]) VALUES (41, 1, N'SD', N'South Dakota')
INSERT [dbo].[egs_region] ([idRegion], [idCountry], [abbreviation], [name]) VALUES (42, 1, N'TN', N'Tennessee')
INSERT [dbo].[egs_region] ([idRegion], [idCountry], [abbreviation], [name]) VALUES (43, 1, N'TX', N'Texas')
INSERT [dbo].[egs_region] ([idRegion], [idCountry], [abbreviation], [name]) VALUES (44, 1, N'UT', N'Utah')
INSERT [dbo].[egs_region] ([idRegion], [idCountry], [abbreviation], [name]) VALUES (45, 1, N'VT', N'Vermont')
INSERT [dbo].[egs_region] ([idRegion], [idCountry], [abbreviation], [name]) VALUES (46, 1, N'VA', N'Virginia')
INSERT [dbo].[egs_region] ([idRegion], [idCountry], [abbreviation], [name]) VALUES (47, 1, N'WA', N'Washington')
INSERT [dbo].[egs_region] ([idRegion], [idCountry], [abbreviation], [name]) VALUES (48, 1, N'WV', N'West Virginia')
INSERT [dbo].[egs_region] ([idRegion], [idCountry], [abbreviation], [name]) VALUES (49, 1, N'WI', N'Wisconsin')
INSERT [dbo].[egs_region] ([idRegion], [idCountry], [abbreviation], [name]) VALUES (50, 1, N'WY', N'Wyoming')
SET IDENTITY_INSERT [dbo].[egs_region] OFF
SET ANSI_PADDING ON

GO
/****** Object:  Index [RoleNameIndex]    Script Date: 11/28/2015 1:31:01 PM ******/
CREATE UNIQUE NONCLUSTERED INDEX [RoleNameIndex] ON [dbo].[AspNetRoles]
(
	[Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_UserId]    Script Date: 11/28/2015 1:31:01 PM ******/
CREATE NONCLUSTERED INDEX [IX_UserId] ON [dbo].[AspNetUserClaims]
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_UserId]    Script Date: 11/28/2015 1:31:01 PM ******/
CREATE NONCLUSTERED INDEX [IX_UserId] ON [dbo].[AspNetUserLogins]
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_RoleId]    Script Date: 11/28/2015 1:31:01 PM ******/
CREATE NONCLUSTERED INDEX [IX_RoleId] ON [dbo].[AspNetUserRoles]
(
	[RoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_UserId]    Script Date: 11/28/2015 1:31:01 PM ******/
CREATE NONCLUSTERED INDEX [IX_UserId] ON [dbo].[AspNetUserRoles]
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [UserNameIndex]    Script Date: 11/28/2015 1:31:01 PM ******/
CREATE UNIQUE NONCLUSTERED INDEX [UserNameIndex] ON [dbo].[AspNetUsers]
(
	[UserName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_egs_player]    Script Date: 11/28/2015 1:31:01 PM ******/
CREATE NONCLUSTERED INDEX [IX_egs_player] ON [dbo].[egs_player]
(
	[idUser] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[AspNetUserClaims]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AspNetUserClaims_dbo.AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserClaims] CHECK CONSTRAINT [FK_dbo.AspNetUserClaims_dbo.AspNetUsers_UserId]
GO
ALTER TABLE [dbo].[AspNetUserLogins]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AspNetUserLogins_dbo.AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserLogins] CHECK CONSTRAINT [FK_dbo.AspNetUserLogins_dbo.AspNetUsers_UserId]
GO
ALTER TABLE [dbo].[AspNetUserRoles]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetRoles_RoleId] FOREIGN KEY([RoleId])
REFERENCES [dbo].[AspNetRoles] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserRoles] CHECK CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetRoles_RoleId]
GO
ALTER TABLE [dbo].[AspNetUserRoles]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserRoles] CHECK CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetUsers_UserId]
GO
ALTER TABLE [dbo].[egs_clients]  WITH CHECK ADD  CONSTRAINT [FK_tngs_clients_tngs_account] FOREIGN KEY([idAccount])
REFERENCES [dbo].[egs_accounts] ([idAccount])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[egs_clients] CHECK CONSTRAINT [FK_tngs_clients_tngs_account]
GO
ALTER TABLE [dbo].[egs_clients]  WITH CHECK ADD  CONSTRAINT [FK_tngs_clients_tngs_applicationType] FOREIGN KEY([idApplicationType])
REFERENCES [dbo].[egs_applicationTypes] ([idApplicationType])
GO
ALTER TABLE [dbo].[egs_clients] CHECK CONSTRAINT [FK_tngs_clients_tngs_applicationType]
GO
ALTER TABLE [dbo].[egs_clients]  WITH CHECK ADD  CONSTRAINT [FK_tngs_clients_tngs_applicationTypes] FOREIGN KEY([idApplicationType])
REFERENCES [dbo].[egs_applicationTypes] ([idApplicationType])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[egs_clients] CHECK CONSTRAINT [FK_tngs_clients_tngs_applicationTypes]
GO
ALTER TABLE [dbo].[egs_counter]  WITH CHECK ADD  CONSTRAINT [FK_egs_counter_egs_game] FOREIGN KEY([idGame])
REFERENCES [dbo].[egs_game] ([idGame])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[egs_counter] CHECK CONSTRAINT [FK_egs_counter_egs_game]
GO
ALTER TABLE [dbo].[egs_counterLanguage]  WITH CHECK ADD  CONSTRAINT [FK_egs_counterLanguage_egs_counter] FOREIGN KEY([idCounter])
REFERENCES [dbo].[egs_counter] ([idCounter])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[egs_counterLanguage] CHECK CONSTRAINT [FK_egs_counterLanguage_egs_counter]
GO
ALTER TABLE [dbo].[egs_counterLanguage]  WITH CHECK ADD  CONSTRAINT [FK_egs_counterLanguage_egs_language] FOREIGN KEY([idLanuage])
REFERENCES [dbo].[egs_language] ([idLanguage])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[egs_counterLanguage] CHECK CONSTRAINT [FK_egs_counterLanguage_egs_language]
GO
ALTER TABLE [dbo].[egs_game]  WITH CHECK ADD  CONSTRAINT [FK_egs_game_egs_accounts] FOREIGN KEY([idAccount])
REFERENCES [dbo].[egs_accounts] ([idAccount])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[egs_game] CHECK CONSTRAINT [FK_egs_game_egs_accounts]
GO
ALTER TABLE [dbo].[egs_gamePiece]  WITH CHECK ADD  CONSTRAINT [FK_egs_gamePiece_AspNetUsers] FOREIGN KEY([createdBy])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE SET NULL
GO
ALTER TABLE [dbo].[egs_gamePiece] CHECK CONSTRAINT [FK_egs_gamePiece_AspNetUsers]
GO
ALTER TABLE [dbo].[egs_gamePiece]  WITH CHECK ADD  CONSTRAINT [FK_egs_gamePiece_egs_game] FOREIGN KEY([idGame])
REFERENCES [dbo].[egs_game] ([idGame])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[egs_gamePiece] CHECK CONSTRAINT [FK_egs_gamePiece_egs_game]
GO
ALTER TABLE [dbo].[egs_gamePiece]  WITH CHECK ADD  CONSTRAINT [FK_egs_gamePiece_egs_gamePieceType] FOREIGN KEY([idGamePieceType])
REFERENCES [dbo].[egs_gamePieceType] ([idGamePieceType])
GO
ALTER TABLE [dbo].[egs_gamePiece] CHECK CONSTRAINT [FK_egs_gamePiece_egs_gamePieceType]
GO
ALTER TABLE [dbo].[egs_gamePieceAttribute]  WITH CHECK ADD  CONSTRAINT [FK_egs_gamePieceAttribute_egs_gamePiece] FOREIGN KEY([idGamePiece])
REFERENCES [dbo].[egs_gamePiece] ([idGamePiece])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[egs_gamePieceAttribute] CHECK CONSTRAINT [FK_egs_gamePieceAttribute_egs_gamePiece]
GO
ALTER TABLE [dbo].[egs_gamePieceAttribute]  WITH CHECK ADD  CONSTRAINT [FK_egs_gamePieceAttribute_egs_gamePieceAttributeType] FOREIGN KEY([idGamePieceAttributeType])
REFERENCES [dbo].[egs_gamePieceAttributeType] ([idGamePieceAttributeType])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[egs_gamePieceAttribute] CHECK CONSTRAINT [FK_egs_gamePieceAttribute_egs_gamePieceAttributeType]
GO
ALTER TABLE [dbo].[egs_gamePieceAttributeType]  WITH CHECK ADD  CONSTRAINT [FK_egs_gamePieceAttributeType_egs_game] FOREIGN KEY([idGame])
REFERENCES [dbo].[egs_game] ([idGame])
GO
ALTER TABLE [dbo].[egs_gamePieceAttributeType] CHECK CONSTRAINT [FK_egs_gamePieceAttributeType_egs_game]
GO
ALTER TABLE [dbo].[egs_gamePieceAttrubuteTypeLanguage]  WITH CHECK ADD  CONSTRAINT [FK_egs_gamePieceAttrubuteTypeLanguage_egs_gamePieceAttributeType] FOREIGN KEY([idGamePieceAttributeType])
REFERENCES [dbo].[egs_gamePieceAttributeType] ([idGamePieceAttributeType])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[egs_gamePieceAttrubuteTypeLanguage] CHECK CONSTRAINT [FK_egs_gamePieceAttrubuteTypeLanguage_egs_gamePieceAttributeType]
GO
ALTER TABLE [dbo].[egs_gamePieceAttrubuteTypeLanguage]  WITH NOCHECK ADD  CONSTRAINT [FK_egs_gamePieceAttrubuteTypeLanguage_egs_language1] FOREIGN KEY([idLanguage])
REFERENCES [dbo].[egs_language] ([idLanguage])
ON DELETE CASCADE
NOT FOR REPLICATION 
GO
ALTER TABLE [dbo].[egs_gamePieceAttrubuteTypeLanguage] CHECK CONSTRAINT [FK_egs_gamePieceAttrubuteTypeLanguage_egs_language1]
GO
ALTER TABLE [dbo].[egs_gamePieceCollection]  WITH CHECK ADD  CONSTRAINT [FK_egs_pieceCollection_egs_game] FOREIGN KEY([idGame])
REFERENCES [dbo].[egs_game] ([idGame])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[egs_gamePieceCollection] CHECK CONSTRAINT [FK_egs_pieceCollection_egs_game]
GO
ALTER TABLE [dbo].[egs_gamePieceCollection]  WITH CHECK ADD  CONSTRAINT [FK_egs_pieceCollection_egs_pieceCollectionType] FOREIGN KEY([idGamePieceCollectionType])
REFERENCES [dbo].[egs_gamePieceCollectionType] ([idGamePieceCollectionType])
GO
ALTER TABLE [dbo].[egs_gamePieceCollection] CHECK CONSTRAINT [FK_egs_pieceCollection_egs_pieceCollectionType]
GO
ALTER TABLE [dbo].[egs_gamePieceCollectionAttribute]  WITH CHECK ADD  CONSTRAINT [FK_egs_gamePieceCollectionAttribute_egs_gamePieceCollection] FOREIGN KEY([idGamePieceCollection])
REFERENCES [dbo].[egs_gamePieceCollection] ([idGamePieceCollection])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[egs_gamePieceCollectionAttribute] CHECK CONSTRAINT [FK_egs_gamePieceCollectionAttribute_egs_gamePieceCollection]
GO
ALTER TABLE [dbo].[egs_gamePieceCollectionAttributeLanguage]  WITH CHECK ADD  CONSTRAINT [FK_egs_gamePieceCollectionAttributeLanguage_egs_gamePieceCollectionAttribute] FOREIGN KEY([idGamePIeceCollectionAttribute])
REFERENCES [dbo].[egs_gamePieceCollectionAttribute] ([idGamePieceCollectionAttribute])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[egs_gamePieceCollectionAttributeLanguage] CHECK CONSTRAINT [FK_egs_gamePieceCollectionAttributeLanguage_egs_gamePieceCollectionAttribute]
GO
ALTER TABLE [dbo].[egs_gamePieceCollectionAttributeLanguage]  WITH CHECK ADD  CONSTRAINT [FK_egs_gamePieceCollectionAttributeLanguage_egs_language] FOREIGN KEY([idLanguage])
REFERENCES [dbo].[egs_language] ([idLanguage])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[egs_gamePieceCollectionAttributeLanguage] CHECK CONSTRAINT [FK_egs_gamePieceCollectionAttributeLanguage_egs_language]
GO
ALTER TABLE [dbo].[egs_gamePieceCollectionGame]  WITH CHECK ADD  CONSTRAINT [FK_egs_gamePieceCollectionGame_egs_gamePieceCollection] FOREIGN KEY([idGamePieceCollection])
REFERENCES [dbo].[egs_gamePieceCollection] ([idGamePieceCollection])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[egs_gamePieceCollectionGame] CHECK CONSTRAINT [FK_egs_gamePieceCollectionGame_egs_gamePieceCollection]
GO
ALTER TABLE [dbo].[egs_gamePieceCollectionGame]  WITH CHECK ADD  CONSTRAINT [FK_egs_gamePieceCollectionGamePiece_egs_gamePiece] FOREIGN KEY([idGamePiece])
REFERENCES [dbo].[egs_gamePiece] ([idGamePiece])
GO
ALTER TABLE [dbo].[egs_gamePieceCollectionGame] CHECK CONSTRAINT [FK_egs_gamePieceCollectionGamePiece_egs_gamePiece]
GO
ALTER TABLE [dbo].[egs_gamePieceCollectionLanguage]  WITH CHECK ADD  CONSTRAINT [FK_egs_GamePieceCollectionLanguage_egs_gamePieceCollection] FOREIGN KEY([idGamePieceCollection])
REFERENCES [dbo].[egs_gamePieceCollection] ([idGamePieceCollection])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[egs_gamePieceCollectionLanguage] CHECK CONSTRAINT [FK_egs_GamePieceCollectionLanguage_egs_gamePieceCollection]
GO
ALTER TABLE [dbo].[egs_gamePieceCollectionLanguage]  WITH CHECK ADD  CONSTRAINT [FK_egs_GamePieceCollectionLanguage_egs_language] FOREIGN KEY([idLanguage])
REFERENCES [dbo].[egs_language] ([idLanguage])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[egs_gamePieceCollectionLanguage] CHECK CONSTRAINT [FK_egs_GamePieceCollectionLanguage_egs_language]
GO
ALTER TABLE [dbo].[egs_gamePieceCollectionPlayer]  WITH CHECK ADD  CONSTRAINT [FK_egs_gamePieceCollectionPlayer_egs_gamePieceCollection] FOREIGN KEY([idGamePieceCollection])
REFERENCES [dbo].[egs_gamePieceCollection] ([idGamePieceCollection])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[egs_gamePieceCollectionPlayer] CHECK CONSTRAINT [FK_egs_gamePieceCollectionPlayer_egs_gamePieceCollection]
GO
ALTER TABLE [dbo].[egs_gamePieceCollectionPlayer]  WITH CHECK ADD  CONSTRAINT [FK_egs_gamePieceCollectionPlayer_egs_player] FOREIGN KEY([idPlayer])
REFERENCES [dbo].[egs_player] ([idPlayer])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[egs_gamePieceCollectionPlayer] CHECK CONSTRAINT [FK_egs_gamePieceCollectionPlayer_egs_player]
GO
ALTER TABLE [dbo].[egs_gamePieceCollectionPlayerPiece]  WITH CHECK ADD  CONSTRAINT [FK_egs_gamePieceCollectionPlayerPiece_egs_gamePiece] FOREIGN KEY([idGamePiece])
REFERENCES [dbo].[egs_gamePiece] ([idGamePiece])
GO
ALTER TABLE [dbo].[egs_gamePieceCollectionPlayerPiece] CHECK CONSTRAINT [FK_egs_gamePieceCollectionPlayerPiece_egs_gamePiece]
GO
ALTER TABLE [dbo].[egs_gamePieceCollectionPlayerPiece]  WITH CHECK ADD  CONSTRAINT [FK_egs_gamePieceCollectionPlayerPiece_egs_gamePieceCollectionPlayer] FOREIGN KEY([idGamePIeceCollectionPlayer])
REFERENCES [dbo].[egs_gamePieceCollectionPlayer] ([idGamePieceCollectionPlayer])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[egs_gamePieceCollectionPlayerPiece] CHECK CONSTRAINT [FK_egs_gamePieceCollectionPlayerPiece_egs_gamePieceCollectionPlayer]
GO
ALTER TABLE [dbo].[egs_gamePieceCollectionType]  WITH CHECK ADD  CONSTRAINT [FK_egs_pieceCollectionType_egs_game] FOREIGN KEY([idGame])
REFERENCES [dbo].[egs_game] ([idGame])
GO
ALTER TABLE [dbo].[egs_gamePieceCollectionType] CHECK CONSTRAINT [FK_egs_pieceCollectionType_egs_game]
GO
ALTER TABLE [dbo].[egs_gamePieceLanguage]  WITH CHECK ADD  CONSTRAINT [FK_egs_gamePieceLanguage_egs_gamePiece] FOREIGN KEY([idGamePiece])
REFERENCES [dbo].[egs_gamePiece] ([idGamePiece])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[egs_gamePieceLanguage] CHECK CONSTRAINT [FK_egs_gamePieceLanguage_egs_gamePiece]
GO
ALTER TABLE [dbo].[egs_gamePieceLanguage]  WITH CHECK ADD  CONSTRAINT [FK_egs_gamePieceLanguage_egs_language] FOREIGN KEY([idLanguage])
REFERENCES [dbo].[egs_language] ([idLanguage])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[egs_gamePieceLanguage] CHECK CONSTRAINT [FK_egs_gamePieceLanguage_egs_language]
GO
ALTER TABLE [dbo].[egs_gamePieceSkin]  WITH CHECK ADD  CONSTRAINT [FK_egs_gamePieceSkin_egs_gamePiece] FOREIGN KEY([idGamePiece])
REFERENCES [dbo].[egs_gamePiece] ([idGamePiece])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[egs_gamePieceSkin] CHECK CONSTRAINT [FK_egs_gamePieceSkin_egs_gamePiece]
GO
ALTER TABLE [dbo].[egs_gamePieceType]  WITH CHECK ADD  CONSTRAINT [FK_egs_gamePieceType_egs_game] FOREIGN KEY([idGame])
REFERENCES [dbo].[egs_game] ([idGame])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[egs_gamePieceType] CHECK CONSTRAINT [FK_egs_gamePieceType_egs_game]
GO
ALTER TABLE [dbo].[egs_gamePieceTypeLanguage]  WITH CHECK ADD  CONSTRAINT [FK_egs_gamePieceTypeLanguage_egs_gamePieceType] FOREIGN KEY([idGamePieceType])
REFERENCES [dbo].[egs_gamePieceType] ([idGamePieceType])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[egs_gamePieceTypeLanguage] CHECK CONSTRAINT [FK_egs_gamePieceTypeLanguage_egs_gamePieceType]
GO
ALTER TABLE [dbo].[egs_gamePieceTypeLanguage]  WITH CHECK ADD  CONSTRAINT [FK_egs_gamePieceTypeLanguage_egs_language] FOREIGN KEY([idLanguage])
REFERENCES [dbo].[egs_language] ([idLanguage])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[egs_gamePieceTypeLanguage] CHECK CONSTRAINT [FK_egs_gamePieceTypeLanguage_egs_language]
GO
ALTER TABLE [dbo].[egs_gameSession]  WITH CHECK ADD  CONSTRAINT [FK_egs_gamesession_egs_game] FOREIGN KEY([idGame])
REFERENCES [dbo].[egs_game] ([idGame])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[egs_gameSession] CHECK CONSTRAINT [FK_egs_gamesession_egs_game]
GO
ALTER TABLE [dbo].[egs_gameSessionPlayer]  WITH CHECK ADD  CONSTRAINT [FK_egs_gameSessionPlayer_egs_gameSession] FOREIGN KEY([idGameSession])
REFERENCES [dbo].[egs_gameSession] ([idGameSession])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[egs_gameSessionPlayer] CHECK CONSTRAINT [FK_egs_gameSessionPlayer_egs_gameSession]
GO
ALTER TABLE [dbo].[egs_gameSessionPlayer]  WITH CHECK ADD  CONSTRAINT [FK_egs_playerGameSession_egs_player] FOREIGN KEY([idPlayer])
REFERENCES [dbo].[egs_player] ([idPlayer])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[egs_gameSessionPlayer] CHECK CONSTRAINT [FK_egs_playerGameSession_egs_player]
GO
ALTER TABLE [dbo].[egs_gameSessionPlayerCollection]  WITH CHECK ADD  CONSTRAINT [FK_egs_gameSessionPlayerCollection_egs_gamePieceCollectionPlayer] FOREIGN KEY([idGamePieceCollectionPlayer])
REFERENCES [dbo].[egs_gamePieceCollectionPlayer] ([idGamePieceCollectionPlayer])
GO
ALTER TABLE [dbo].[egs_gameSessionPlayerCollection] CHECK CONSTRAINT [FK_egs_gameSessionPlayerCollection_egs_gamePieceCollectionPlayer]
GO
ALTER TABLE [dbo].[egs_gameSessionPlayerCollection]  WITH CHECK ADD  CONSTRAINT [FK_egs_gameSessionPlayerCollection_egs_gameSessionPlayer] FOREIGN KEY([idGameSessionPlayer])
REFERENCES [dbo].[egs_gameSessionPlayer] ([idGameSessionPlayer])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[egs_gameSessionPlayerCollection] CHECK CONSTRAINT [FK_egs_gameSessionPlayerCollection_egs_gameSessionPlayer]
GO
ALTER TABLE [dbo].[egs_player]  WITH CHECK ADD  CONSTRAINT [FK_egs_player_AspNetUsers] FOREIGN KEY([idUser])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[egs_player] CHECK CONSTRAINT [FK_egs_player_AspNetUsers]
GO
ALTER TABLE [dbo].[egs_player]  WITH CHECK ADD  CONSTRAINT [FK_egs_player_egs_player] FOREIGN KEY([idRegion])
REFERENCES [dbo].[egs_region] ([idRegion])
GO
ALTER TABLE [dbo].[egs_player] CHECK CONSTRAINT [FK_egs_player_egs_player]
GO
ALTER TABLE [dbo].[egs_region]  WITH CHECK ADD  CONSTRAINT [FK_egs_region_egs_country] FOREIGN KEY([idCountry])
REFERENCES [dbo].[egs_country] ([idCountry])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[egs_region] CHECK CONSTRAINT [FK_egs_region_egs_country]
GO
/****** Object:  StoredProcedure [dbo].[SerializeJSON]    Script Date: 11/28/2015 1:31:01 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 
CREATE PROCEDURE [dbo].[SerializeJSON] (@ParameterSQL AS VARCHAR(MAX))
AS
BEGIN
    DECLARE @SQL NVARCHAR(MAX)
    DECLARE @XMLString VARCHAR(MAX)
    DECLARE @XML XML
    DECLARE @Paramlist NVARCHAR(1000)
 
    SET @Paramlist = N'@XML XML OUTPUT'
    SET @SQL = 'WITH PrepareTable (XMLString)'
    SET @SQL = @SQL + 'AS('
    SET @SQL = @SQL + @ParameterSQL + 'FOR XML RAW,TYPE,ELEMENTS'
    SET @SQL = @SQL + ')'
    SET @SQL = @SQL + 'SELECT @XML=[XMLString]FROM[PrepareTable]'
 
    EXEC sp_executesql @SQL
        , @Paramlist
        , @XML = @XML OUTPUT
 
    SET @XMLString = CAST(@XML AS VARCHAR(MAX))
 
    DECLARE @JSON VARCHAR(MAX)
    DECLARE @Row VARCHAR(MAX)
    DECLARE @RowStart INT
    DECLARE @RowEnd INT
    DECLARE @FieldStart INT
    DECLARE @FieldEnd INT
    DECLARE @KEY VARCHAR(MAX)
    DECLARE @Value VARCHAR(MAX)
    DECLARE @StartRoot VARCHAR(100);
 
    SET @StartRoot = '<row>'
 
    DECLARE @EndRoot VARCHAR(100);
 
    SET @EndRoot = '</row>'
 
    DECLARE @StartField VARCHAR(100);
 
    SET @StartField = '<'
 
    DECLARE @EndField VARCHAR(100);
 
    SET @EndField = '>'
    SET @RowStart = CharIndex(@StartRoot, @XMLString, 0)
    SET @JSON = ''
 
    WHILE @RowStart > 0
    BEGIN
        SET @RowStart = @RowStart + Len(@StartRoot)
        SET @RowEnd = CharIndex(@EndRoot, @XMLString, @RowStart)
        SET @Row = SubString(@XMLString, @RowStart, @RowEnd - @RowStart)
        SET @JSON = @JSON + '{'
        -- for each row
        SET @FieldStart = CharIndex(@StartField, @Row, 0)
 
        WHILE @FieldStart > 0
        BEGIN
            -- parse node key
            SET @FieldStart = @FieldStart + Len(@StartField)
            SET @FieldEnd = CharIndex(@EndField, @Row, @FieldStart)
            SET @KEY = SubString(@Row, @FieldStart, @FieldEnd - @FieldStart)
            SET @JSON = @JSON + '"' + @KEY + '":'
            -- parse node value
            SET @FieldStart = @FieldEnd + 1
            SET @FieldEnd = CharIndex('</', @Row, @FieldStart)
            SET @Value = SubString(@Row, @FieldStart, @FieldEnd - @FieldStart)
            SET @JSON = @JSON + '"' + @Value + '",'
            SET @FieldStart = @FieldStart + Len(@StartField)
            SET @FieldEnd = CharIndex(@EndField, @Row, @FieldStart)
            SET @FieldStart = CharIndex(@StartField, @Row, @FieldEnd)
        END
 
        IF LEN(@JSON) > 0
            SET @JSON = SubString(@JSON, 0, LEN(@JSON))
        SET @JSON = @JSON + '},'
        --/ for each row
        SET @RowStart = CharIndex(@StartRoot, @XMLString, @RowEnd)
    END
 
    IF LEN(@JSON) > 0
        SET @JSON = SubString(@JSON, 0, LEN(@JSON))
    SET @JSON = '[' + @JSON + ']'
 
    SELECT @JSON
END
GO
/****** Object:  StoredProcedure [dbo].[sp_egs_getGameCollectionGamePieces]    Script Date: 11/28/2015 1:31:01 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_egs_getGameCollectionGamePieces]
	@idGamePieceCollection as bigint,
	@isActive as nvarchar(1),
	@idLanguage as nvarchar(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SET FMTONLY OFF;
	SET QUOTED_IDENTIFIER OFF;
	SET ANSI_NULLS ON

	DECLARE
		@cols as nvarchar(MAX),
		@query as nvarchar(MAX)

  SELECT
	@cols = STUFF((SELECT ',' + QUOTENAME(egs_gamePieceAttributeType.pieceAttributeType)
					FROM	egs_gamePieceLanguage INNER JOIN
							egs_gamePiece INNER JOIN
							egs_gamePieceAttribute ON egs_gamePiece.idGamePiece = egs_gamePieceAttribute.idGamePiece INNER JOIN
							egs_gamePieceAttributeType ON egs_gamePieceAttribute.idGamePieceAttributeType = egs_gamePieceAttributeType.idGamePieceAttributeType INNER JOIN
							egs_gamePieceAttrubuteTypeLanguage ON egs_gamePieceAttributeType.idGamePieceAttributeType = egs_gamePieceAttrubuteTypeLanguage.idGamePieceAttributeType ON egs_gamePieceLanguage.idGamePiece = egs_gamePiece.idGamePiece INNER JOIN
							egs_gamePieceSkin ON egs_gamePiece.idGamePiece = egs_gamePieceSkin.idGamePiece INNER JOIN 
							egs_language ON egs_gamePieceLanguage.idLanguage = egs_language.idLanguage AND egs_gamePieceAttrubuteTypeLanguage.idLanguage = egs_language.idLanguage INNER JOIN
							egs_gamePieceCollectionGame on egs_gamePiece.idGamePiece = egs_gamePieceCollectionGame.idGamePiece
					WHERE
						--egs_gamePiece.idGame = @idGame
						egs_gamePieceCollectionGame.idGamePieceCollection = @idGamePieceCollection
						AND egs_language.idLanguage = @idLanguage
					GROUP BY 
							egs_gamePieceAttributeType.pieceAttributeType
					FOR XML PATH(''), TYPE)
					.value('.','nvarchar(MAX)'),1,1,'')
SET @query = N'SELECT idGamePiece, name, pieceType, description,' + @cols + N' FROM
			(
				SELECT egs_gamePiece.idGamePiece, egs_gamePieceTypeLanguage.pieceType, egs_gamePieceLanguage.description, egs_gamePieceLanguage.name, egs_gamePieceAttribute.value, egs_gamePieceAttributeType.pieceAttributeType
				FROM	egs_gamePieceLanguage INNER JOIN
							egs_gamePiece INNER JOIN
							egs_gamePieceAttribute ON egs_gamePiece.idGamePiece = egs_gamePieceAttribute.idGamePiece INNER JOIN
							egs_gamePieceAttributeType ON egs_gamePieceAttribute.idGamePieceAttributeType = egs_gamePieceAttributeType.idGamePieceAttributeType INNER JOIN
							egs_gamePieceAttrubuteTypeLanguage ON egs_gamePieceAttributeType.idGamePieceAttributeType = egs_gamePieceAttrubuteTypeLanguage.idGamePieceAttributeType ON egs_gamePieceLanguage.idGamePiece = egs_gamePiece.idGamePiece INNER JOIN
							egs_gamePieceSkin ON egs_gamePiece.idGamePiece = egs_gamePieceSkin.idGamePiece INNER JOIN
							egs_gamePieceType ON egs_gamePiece.idGamePieceType = egs_gamePieceType.idGamePieceType INNER JOIN
							egs_gamePieceTypeLanguage ON egs_gamePieceType.idGamePieceType = egs_gamePieceTypeLanguage.idGamePieceType INNER JOIN 
							egs_language ON egs_gamePieceLanguage.idLanguage = egs_language.idLanguage AND egs_gamePieceAttrubuteTypeLanguage.idLanguage = egs_language.idLanguage INNER JOIN
							egs_gamePieceCollectionGame on egs_gamePiece.idGamePiece = egs_gamePieceCollectionGame.idGamePiece
			   AND egs_gamePieceCollectionGame.idGamePieceCollection = ' + CONVERT(nvarchar(MAX), @idGamePieceCollection) +'
			   AND egs_language.idLanguage = ''' + @idLanguage + '''
			) x
			PIVOT
			(
				MAX(value)
				FOR pieceAttributeType in (' + @cols + N')
				) p
				
				order by name'

EXECUTE(@query)

END

GO
/****** Object:  StoredProcedure [dbo].[sp_egs_getGamePieces]    Script Date: 11/28/2015 1:31:01 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_egs_getGamePieces]
	@idGame as bigint,
	@isActive as nvarchar(1),
	@culture as nvarchar(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SET FMTONLY OFF;
	SET QUOTED_IDENTIFIER OFF;
	SET ANSI_NULLS ON

	DECLARE
		@cols as nvarchar(MAX),
		@query as nvarchar(MAX)

  SELECT
	@cols = STUFF((SELECT ',' + QUOTENAME(egs_gamePieceAttributeType.pieceAttributeType)
					FROM	egs_gamePieceLanguage INNER JOIN
							egs_gamePiece INNER JOIN
							egs_gamePieceAttribute ON egs_gamePiece.idGamePiece = egs_gamePieceAttribute.idGamePiece INNER JOIN
							egs_gamePieceAttributeType ON egs_gamePieceAttribute.idGamePieceAttributeType = egs_gamePieceAttributeType.idGamePieceAttributeType INNER JOIN
							egs_gamePieceAttrubuteTypeLanguage ON egs_gamePieceAttributeType.idGamePieceAttributeType = egs_gamePieceAttrubuteTypeLanguage.idGamePieceAttributeType ON egs_gamePieceLanguage.idGamePiece = egs_gamePiece.idGamePiece INNER JOIN
							egs_gamePieceSkin ON egs_gamePiece.idGamePiece = egs_gamePieceSkin.idGamePiece INNER JOIN 
							egs_language ON egs_gamePieceLanguage.idLanguage = egs_language.idLanguage AND egs_gamePieceAttrubuteTypeLanguage.idLanguage = egs_language.idLanguage
					WHERE
						egs_gamePiece.idGame = @idGame
						AND egs_language.culture = @culture
					GROUP BY 
							egs_gamePieceAttributeType.pieceAttributeType
					FOR XML PATH(''), TYPE)
					.value('.','nvarchar(MAX)'),1,1,'')
SET @query = N'SELECT idGamePiece, name, pieceType, description,' + @cols + N' FROM
			(
				SELECT egs_gamePiece.idGamePiece, egs_gamePieceTypeLanguage.pieceType, egs_gamePieceLanguage.description, egs_gamePieceLanguage.name, egs_gamePieceAttribute.value, egs_gamePieceAttributeType.pieceAttributeType
				FROM	egs_gamePieceLanguage INNER JOIN
							egs_gamePiece INNER JOIN
							egs_gamePieceAttribute ON egs_gamePiece.idGamePiece = egs_gamePieceAttribute.idGamePiece INNER JOIN
							egs_gamePieceAttributeType ON egs_gamePieceAttribute.idGamePieceAttributeType = egs_gamePieceAttributeType.idGamePieceAttributeType INNER JOIN
							egs_gamePieceAttrubuteTypeLanguage ON egs_gamePieceAttributeType.idGamePieceAttributeType = egs_gamePieceAttrubuteTypeLanguage.idGamePieceAttributeType ON egs_gamePieceLanguage.idGamePiece = egs_gamePiece.idGamePiece INNER JOIN
							egs_gamePieceSkin ON egs_gamePiece.idGamePiece = egs_gamePieceSkin.idGamePiece INNER JOIN
							egs_gamePieceType ON egs_gamePiece.idGamePieceType = egs_gamePieceType.idGamePieceType INNER JOIN
							egs_gamePieceTypeLanguage ON egs_gamePieceType.idGamePieceType = egs_gamePieceTypeLanguage.idGamePieceType INNER JOIN 
							egs_language ON egs_gamePieceLanguage.idLanguage = egs_language.idLanguage AND egs_gamePieceAttrubuteTypeLanguage.idLanguage = egs_language.idLanguage
			   and egs_gamePiece.idGame = '+ CONVERT(nvarchar(MAX), @idGame) +'
			   AND egs_language.culture = ''' + @culture + '''
			) x
			PIVOT
			(
				MAX(value)
				FOR pieceAttributeType in (' + @cols + N')
				) p
				
				order by name'

EXECUTE(@query)

END

GO
/****** Object:  StoredProcedure [dbo].[sp_egs_getGameSessions]    Script Date: 11/28/2015 1:31:01 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Marc Arbesman
-- Project: Project Neptune
-- Create date: 2/16/2015
-- Description:	retrieves all games sessions for a particular user in a format used for presentation in
-- the game client
-- =============================================
CREATE PROCEDURE [dbo].[sp_egs_getGameSessions]
	-- Add the parameters for the stored procedure here
	@userId as nvarchar(128),
	@gameId as bigint,
	@isActive as nvarchar(1)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SET FMTONLY OFF;
	SET QUOTED_IDENTIFIER OFF;
	SET ANSI_NULLS ON;
    
	DECLARE @cols AS NVARCHAR(MAX),
	    @query  AS NVARCHAR(MAX),
		@cols2 as NVARCHAR(MAX),
		@colsA as NVARCHAR(MAX),
		@colsB as NVARCHAR(MAX)
	
	-- Dynamically determine the total number of columns (i.e. players) This allows for an unlimited
	-- amount of players in a game session
	-- Player 1 is designated by the isCreator flag in the egs_gameSessionPlayer table.
	-- If the player for the session is marked as the creator (e.g. true) than they are player 1....ready?
	-- All other players (player 2, 3, 4 etc.) are ordered by whoever was put in first into the egs_GameSessionPlayer table
	set @cols = STUFF(
							(SELECT DISTINCT ',' + QUOTENAME(t.col)
								FROM 
								(SELECT 'player_'+cast(row_number() over(partition by egs_gameSessionPlayer.idGameSession order by egs_gameSessionPlayer.isCreator DESC) as varchar(10)) col
								
									FROM   egs_gameSessionPlayer 
									--INNER JOIN egs_player ON egs_gameSessionPlayer.idPlayer = egs_player.idPlayer 
									INNER JOIN egs_gameSession ON egs_gameSessionPlayer.idGameSession = egs_gameSession.idGameSession 
									--INNER JOIN AspNetUsers ON egs_player.idUser = AspNetUsers.Id
									WHERE egs_gameSession.idGame = @gameId AND egs_gameSessionPlayer.idGameSession IN
									(
										SELECT egs_gameSessionPlayer.idGameSession 
											FROM egs_gameSessionPlayer
											INNER JOIN egs_player ON egs_gameSessionPlayer.idPlayer = egs_player.idPlayer											
											WHERE egs_player.idUser = @userId	
									)
								
								) t                    
								FOR XML PATH(''), TYPE
							).value('.', 'NVARCHAR(MAX)') 
							 ,1,1,'')
IF @cols <> ''
BEGIN
	set @colsA = @cols

	set @cols2 = STUFF(
								(SELECT DISTINCT ',' + QUOTENAME(t.col)
									FROM 
									(SELECT 'idPlayer_'+cast(row_number() over(partition by egs_gameSessionPlayer.idGameSession order by egs_gameSessionPlayer.isCreator DESC) as varchar(10)) col
								
										FROM   egs_gameSessionPlayer 
										--INNER JOIN egs_player ON egs_gameSessionPlayer.idPlayer = egs_player.idPlayer 
										INNER JOIN egs_gameSession ON egs_gameSessionPlayer.idGameSession = egs_gameSession.idGameSession 
										--INNER JOIN AspNetUsers ON egs_player.idUser = AspNetUsers.Id
										WHERE egs_gameSession.idGame = @gameId AND egs_gameSessionPlayer.idGameSession IN
										(
											SELECT egs_gameSessionPlayer.idGameSession 
												FROM egs_gameSessionPlayer
												INNER JOIN egs_player ON egs_gameSessionPlayer.idPlayer = egs_player.idPlayer											
												WHERE egs_player.idUser = @userId	
										)
								
									) t                    
									FOR XML PATH(''), TYPE
								).value('.', 'NVARCHAR(MAX)') 
								 ,1,1,'')
	set @colsB = @cols2
	declare @cols_select varchar(8000)
	set @cols_select = ''
	DECLARE @value varchar(8000)

	while len(@cols) > 0
	begin
	 set @value = left(@cols, charindex(',', @cols+',')-1)
 
	 set @cols_select = @cols_select + 'max(' + @value + ') as ' + @value + ', '
	 set @cols = stuff(@cols, 1, charindex(',', @cols+','), '')
	end

	SET @cols_select = LEFT(@cols_select, LEN(@cols_select) - 1)

	declare @cols_select2 varchar(max)
	set @cols_select2 = ''
	while len(@cols2) > 0
	begin
	 set @value = left(@cols2, charindex(',', @cols2+',')-1)
	 set @cols_select2 = @cols_select2 + 'max(' + @value + ') as ' + @value + ', '

	  set @cols2 = stuff(@cols2, 1, charindex(',', @cols2+','), '')
	end
	SET @cols_select2 = LEFT(@cols_select2, LEN(@cols_select2) - 1)

	-- Create the query to flatten the data and use the columns we determine above
		SET @query = 'SELECT idGameSession as gameSessionID, isActive, max(startTime) as startTime, max(endTime) as endTime, max(lastTurn) as lastTurn,' + @cols_select + ',' + @cols_select2 + 
					 ' FROM 
						(SELECT egs_gameSessionPlayer.idGameSession
								,egs_gameSession.isActive
								,egs_gameSession.startTime
								,egs_gameSession.endTime
								,egs_gameSession.lastTurn
								,egs_player.nickName												
								,egs_player.idPlayer	
								,''player_''+cast(row_number() over(partition by egs_gameSessionPlayer.idGameSession order by egs_gameSessionPlayer.isCreator DESC) as varchar(10)) col
								,''idPlayer_''+cast(row_number() over(partition by egs_gameSessionPlayer.idGameSession order by egs_gameSessionPlayer.isCreator DESC) as varchar(10)) col2
						FROM egs_gameSessionPlayer 
						INNER JOIN egs_player ON egs_gameSessionPlayer.idPlayer = egs_player.idPlayer 
					
						INNER JOIN egs_gameSession ON egs_gameSessionPlayer.idGameSession = egs_gameSession.idGameSession
							WHERE egs_gameSession.idGame = ' + CONVERT(NVARCHAR(100), @gameId) + ' AND egs_gameSessionPlayer.idGameSession IN
						(
							SELECT egs_gameSessionPlayer.idGameSession 
								FROM egs_gameSessionPlayer
								INNER JOIN egs_player ON egs_gameSessionPlayer.idPlayer = egs_player.idPlayer											
								WHERE egs_player.idUser =''' +  @userId + '''	
						)									
						) x
						pivot 
						(
							max(nickName)
							for col in (' + @colsA + ')					
						) p
						pivot
						(
							max(idPlayer)
							for col2 in ('+ @colsB +')
						) d
						WHERE isActive = ' +  @isActive + '
						GROUP BY idGameSession, isActive
						ORDER BY startTime
					'

	EXECUTE(@query)
END

END

GO
/****** Object:  StoredProcedure [dbo].[sp_egs_getSessionPlayerCollectionGamePieces]    Script Date: 11/28/2015 1:31:01 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_egs_getSessionPlayerCollectionGamePieces]
	@idGamePieceCollectionPlayer as bigint,
	@isActive as nvarchar(1),
	@idLanguage as nvarchar(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SET FMTONLY OFF;
	SET QUOTED_IDENTIFIER OFF;
	SET ANSI_NULLS ON

	DECLARE
		@cols as nvarchar(MAX),
		@query as nvarchar(MAX)

  SELECT
	@cols = STUFF((SELECT ',' + QUOTENAME(egs_gamePieceAttributeType.pieceAttributeType)
					FROM	egs_gamePieceLanguage INNER JOIN
							egs_gamePiece INNER JOIN
							egs_gamePieceAttribute ON egs_gamePiece.idGamePiece = egs_gamePieceAttribute.idGamePiece INNER JOIN
							egs_gamePieceAttributeType ON egs_gamePieceAttribute.idGamePieceAttributeType = egs_gamePieceAttributeType.idGamePieceAttributeType INNER JOIN
							egs_gamePieceAttrubuteTypeLanguage ON egs_gamePieceAttributeType.idGamePieceAttributeType = egs_gamePieceAttrubuteTypeLanguage.idGamePieceAttributeType ON egs_gamePieceLanguage.idGamePiece = egs_gamePiece.idGamePiece INNER JOIN
							egs_gamePieceSkin ON egs_gamePiece.idGamePiece = egs_gamePieceSkin.idGamePiece INNER JOIN 
							egs_language ON egs_gamePieceLanguage.idLanguage = egs_language.idLanguage AND egs_gamePieceAttrubuteTypeLanguage.idLanguage = egs_language.idLanguage INNER JOIN
							egs_gamePieceCollectionPlayerPiece on egs_gamePiece.idGamePiece = egs_gamePieceCollectionPlayerPiece.idGamePiece
					WHERE
						--egs_gamePiece.idGame = @idGame
						egs_gamePieceCollectionPlayerPiece.idGamePIeceCollectionPlayer = @idGamePieceCollectionPlayer
						AND egs_language.idLanguage = @idLanguage
					GROUP BY 
							egs_gamePieceAttributeType.pieceAttributeType
					FOR XML PATH(''), TYPE)
					.value('.','nvarchar(MAX)'),1,1,'')
SET @query = N'SELECT idGamePiece, name, pieceType, description,' + @cols + N' FROM
			(
				SELECT egs_gamePiece.idGamePiece, egs_gamePieceTypeLanguage.pieceType, egs_gamePieceLanguage.description, egs_gamePieceLanguage.name, egs_gamePieceAttribute.value, egs_gamePieceAttributeType.pieceAttributeType
				FROM	egs_gamePieceLanguage INNER JOIN
							egs_gamePiece INNER JOIN
							egs_gamePieceAttribute ON egs_gamePiece.idGamePiece = egs_gamePieceAttribute.idGamePiece INNER JOIN
							egs_gamePieceAttributeType ON egs_gamePieceAttribute.idGamePieceAttributeType = egs_gamePieceAttributeType.idGamePieceAttributeType INNER JOIN
							egs_gamePieceAttrubuteTypeLanguage ON egs_gamePieceAttributeType.idGamePieceAttributeType = egs_gamePieceAttrubuteTypeLanguage.idGamePieceAttributeType ON egs_gamePieceLanguage.idGamePiece = egs_gamePiece.idGamePiece INNER JOIN
							egs_gamePieceSkin ON egs_gamePiece.idGamePiece = egs_gamePieceSkin.idGamePiece INNER JOIN
							egs_gamePieceType ON egs_gamePiece.idGamePieceType = egs_gamePieceType.idGamePieceType INNER JOIN
							egs_gamePieceTypeLanguage ON egs_gamePieceType.idGamePieceType = egs_gamePieceTypeLanguage.idGamePieceType INNER JOIN 
							egs_language ON egs_gamePieceLanguage.idLanguage = egs_language.idLanguage AND egs_gamePieceAttrubuteTypeLanguage.idLanguage = egs_language.idLanguage INNER JOIN
							egs_gamePieceCollectionPlayerPiece on egs_gamePiece.idGamePiece = egs_gamePieceCollectionPlayerPiece.idGamePiece
			   AND egs_gamePieceCollectionPlayerPiece.idGamePIeceCollectionPlayer = ' + CONVERT(nvarchar(MAX), @idGamePieceCollectionPlayer) +'
			   AND egs_language.idLanguage = ''' + @idLanguage + '''
			) x
			PIVOT
			(
				MAX(value)
				FOR pieceAttributeType in (' + @cols + N')
				) p
				
				order by name'

EXECUTE(@query)

END

GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1[50] 4[25] 3) )"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1[56] 3) )"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4[50] 3) )"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3) )"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 7
   End
   Begin DiagramPane = 
      PaneHidden = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "egs_gamePieceCollection"
            Begin Extent = 
               Top = 9
               Left = 57
               Bottom = 206
               Right = 378
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "egs_gamePieceCollectionLanguage"
            Begin Extent = 
               Top = 64
               Left = 552
               Bottom = 261
               Right = 912
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "egs_gamePieceCollectionType"
            Begin Extent = 
               Top = 207
               Left = 57
               Bottom = 377
               Right = 378
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "egs_language"
            Begin Extent = 
               Top = 9
               Left = 969
               Bottom = 225
               Right = 1191
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
      PaneHidden = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1000
         Width = 1000
         Width = 1000
         Width = 1400
         Width = 1000
         Width = 1000
         Width = 1000
         Width = 1000
      End
   End
   Begin CriteriaPane = 
      PaneHidden = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'v_egs_collections'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane2', @value=N'1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'v_egs_collections'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=2 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'v_egs_collections'
GO
USE [master]
GO
ALTER DATABASE [zebulonDB] SET  READ_WRITE 
GO
