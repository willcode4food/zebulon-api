﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Web;

namespace zebulon.EFModel.Extensions
{
    /// <summary>
    /// 
    /// </summary>
    public static class ParameterExtension
    {
        public static void AddParameterWithValue(this DbCommand command, string parameterName, object parameterValue)
        {
            var parameter = command.CreateParameter();
            parameter.ParameterName = parameterName;
            parameter.Value = parameterValue;
            command.Parameters.Add(parameter);
        }
    }
}
