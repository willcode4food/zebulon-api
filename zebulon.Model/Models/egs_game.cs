using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace zebulon.EFModel.Models
{
    public partial class egs_game
    {
        public egs_game()
        {
            this.egs_counter = new List<egs_counter>();
            this.egs_gameMap = new List<egs_gameMap>();
            this.egs_gamePiece = new List<egs_gamePiece>();
            this.egs_gamePieceAttributeType = new List<egs_gamePieceAttributeType>();
            this.egs_gamePieceType = new List<egs_gamePieceType>();
            this.egs_gameSession = new List<egs_gameSession>();
            this.egs_gamePieceCollection = new List<egs_gamePieceCollection>();
            this.egs_gamePieceCollectionType = new List<egs_gamePieceCollectionType>();
        }

        public long idGame { get; set; }
        public Nullable<long> idAccount { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public virtual egs_accounts egs_accounts { get; set; }
        public virtual ICollection<egs_counter> egs_counter { get; set; }
        [JsonIgnore]
        public virtual ICollection<egs_gameMap> egs_gameMap { get; set; }
        public virtual ICollection<egs_gamePiece> egs_gamePiece { get; set; }
        [JsonIgnore]
        public virtual ICollection<egs_gamePieceAttributeType> egs_gamePieceAttributeType { get; set; }
        [JsonIgnore]
        public virtual ICollection<egs_gamePieceType> egs_gamePieceType { get; set; }
        [JsonIgnore]
        public virtual ICollection<egs_gameSession> egs_gameSession { get; set; }
        public virtual ICollection<egs_gamePieceCollection> egs_gamePieceCollection { get; set; }
        public virtual ICollection<egs_gamePieceCollectionType> egs_gamePieceCollectionType { get; set; }
    }
}
