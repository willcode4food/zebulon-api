﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using zebulon.EFModel.Extensions;

namespace zebulon.EFModel.Models
{
    public partial class sp_egs_GetGameCollectionGamePieces
    {

        public async Task<List<dynamic>> Execute(long idGamePieceCollectionGame, int idLanguage, string isActive)
        {
            try
            {
                List<dynamic> returnList = new List<dynamic>();

                using (var db = new AuthContext())
                {
                    var dbCommand = db.Database.Connection.CreateCommand();
                    await dbCommand.Connection.OpenAsync();

                    dbCommand.CommandText = "sp_egs_GetCollectionGamePieces";
                    dbCommand.CommandType = System.Data.CommandType.StoredProcedure;
                    dbCommand.AddParameterWithValue("@idGamePieceCollectionGame", idGamePieceCollectionGame);
                    dbCommand.AddParameterWithValue("@idLanguage", idLanguage);
                    dbCommand.AddParameterWithValue("@isActive", isActive);

                    DbDataReader reader = await dbCommand.ExecuteReaderAsync(System.Data.CommandBehavior.CloseConnection);
                    
                    while (reader.Read())
                    {
                        returnList.Add(SqlDataReaderToExpando(reader));
                    }

                    dbCommand.Connection.Close();
                }

                return returnList;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            
            
        }
        private dynamic SqlDataReaderToExpando(DbDataReader reader)
        {
            var expandoObject = new ExpandoObject() as IDictionary<string, object>;

            for (var i = 0; i < reader.FieldCount; i++)
                expandoObject.Add(reader.GetName(i), reader[i]);

            return expandoObject;
        }
    }
}
