using System;
using System.Collections.Generic;
using Newtonsoft.Json;
namespace zebulon.EFModel.Models
{
    public partial class egs_country
    {
        public egs_country()
        {
            this.egs_region = new List<egs_region>();
        }

        public int idCountry { get; set; }
        public string name { get; set; }
        [JsonIgnore]
        public virtual ICollection<egs_region> egs_region { get; set; }
    }
}
