using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace zebulon.EFModel.Models
{
    public partial class egs_gamePieceAttribute
    {
        public long idGamePieceAttribute { get; set; }
        [JsonIgnore]
        public Nullable<long> idGamePiece { get; set; }
        public Nullable<int> idGamePieceAttributeType { get; set; }
        public string value { get; set; }
        [JsonIgnore]
        public virtual egs_gamePiece egs_gamePiece { get; set; }
        [JsonProperty("gamePieceAttributeType")]
        public virtual egs_gamePieceAttributeType egs_gamePieceAttributeType { get; set; }
    }
}
