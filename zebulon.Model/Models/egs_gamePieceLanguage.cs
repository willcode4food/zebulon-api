using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace zebulon.EFModel.Models
{
    public partial class egs_gamePieceLanguage
    {
        public long idGamePieceLanguage { get; set; }
        public Nullable<int> idLanguage { get; set; }
        [JsonIgnore]
        public Nullable<long> idGamePiece { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        [JsonIgnore]
        public virtual egs_gamePiece egs_gamePiece { get; set; }
        [JsonProperty("language")]
        public virtual egs_language egs_language { get; set; }
    }
}
