using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace zebulon.EFModel.Models
{
    public partial class egs_counter
    {
        public egs_counter()
        {
            this.egs_counterLanguage = new List<egs_counterLanguage>();
        }

        public int idCounter { get; set; }
        public Nullable<long> idGame { get; set; }
        public Nullable<long> counterStart { get; set; }
        public Nullable<long> counterEnd { get; set; }
        [JsonIgnore]
        public virtual egs_game egs_game { get; set; }
        public virtual ICollection<egs_counterLanguage> egs_counterLanguage { get; set; }
    }
}
