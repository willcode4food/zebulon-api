using System;
using System.Collections.Generic;

namespace zebulon.EFModel.Models
{
    public partial class egs_clients
    {
        public long idClient { get; set; }
        public long idAccount { get; set; }
        public long idApplicationType { get; set; }
        public string name { get; set; }
        public string secret { get; set; }
        public string description { get; set; }
        public Nullable<bool> isActive { get; set; }
        public Nullable<int> refreshTokenLifetime { get; set; }
        public string allowedOrigin { get; set; }
        public string clientIdentifier { get; set; }
        public virtual egs_accounts egs_accounts { get; set; }
        public virtual egs_applicationTypes egs_applicationTypes { get; set; }
        public virtual egs_applicationTypes egs_applicationTypes1 { get; set; }
    }
}
