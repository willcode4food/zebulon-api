using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace zebulon.EFModel.Models
{
    public partial class egs_gamePieceCollectionPlayerPiece
    {
        public long idGamePieceCollectionPlayerPiece { get; set; }
        public long idGamePIeceCollectionPlayer { get; set; }
        public long idGamePiece { get; set; }
        public virtual egs_gamePiece egs_gamePiece { get; set; }
        [JsonIgnore]
        public virtual egs_gamePieceCollectionPlayer egs_gamePieceCollectionPlayer { get; set; }
    }
}
