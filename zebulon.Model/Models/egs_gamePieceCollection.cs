using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace zebulon.EFModel.Models
{
    public partial class egs_gamePieceCollection
    {
        public egs_gamePieceCollection()
        {
            this.egs_gamePieceCollectionAttribute = new List<egs_gamePieceCollectionAttribute>();
            this.egs_gamePieceCollectionLanguage = new List<egs_gamePieceCollectionLanguage>();
            this.egs_gamePieceCollectionPlayer = new List<egs_gamePieceCollectionPlayer>();
        }

        public long idGamePieceCollection { get; set; }
        public Nullable<long> idGame { get; set; }
        public Nullable<int> idGamePieceCollectionType { get; set; }
        public Nullable<int> size { get; set; }
        [JsonIgnore]
        public virtual egs_game egs_game { get; set; }
        [JsonIgnore]
        public virtual ICollection<egs_gamePieceCollectionAttribute> egs_gamePieceCollectionAttribute { get; set; }
        public virtual ICollection<egs_gamePieceCollectionLanguage> egs_gamePieceCollectionLanguage { get; set; }
        [JsonIgnore]
        public virtual ICollection<egs_gamePieceCollectionPlayer> egs_gamePieceCollectionPlayer { get; set; }
        public virtual egs_gamePieceCollectionType egs_gamePieceCollectionType { get; set; }
    }
}
