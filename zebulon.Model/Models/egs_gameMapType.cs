using System;
using System.Collections.Generic;

namespace zebulon.EFModel.Models
{
    public partial class egs_gameMapType
    {
        public egs_gameMapType()
        {
            this.egs_gameMap = new List<egs_gameMap>();
        }

        public int idGameMapType { get; set; }
        public string mapType { get; set; }
        public virtual ICollection<egs_gameMap> egs_gameMap { get; set; }
    }
}
