using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
namespace zebulon.EFModel.Models
{
    public partial class egs_gamePieceCollectionLanguage
    {
        [Key]
        public int idGamePieceCollectionLanguage { get; set; }
        public Nullable<long> idGamePieceCollection { get; set; }
        public Nullable<int> idLanguage { get; set; }
        public string collectionName { get; set; }
        [JsonIgnore]
        public virtual egs_gamePieceCollection egs_gamePieceCollection { get; set; }
        public virtual egs_language egs_language { get; set; }
    }
}
