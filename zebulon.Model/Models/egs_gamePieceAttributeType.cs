using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace zebulon.EFModel.Models
{
    public partial class egs_gamePieceAttributeType
    {
        public egs_gamePieceAttributeType()
        {
            this.egs_gamePieceAttribute = new List<egs_gamePieceAttribute>();
            this.egs_gamePieceAttrubuteTypeLanguage = new List<egs_gamePieceAttrubuteTypeLanguage>();
        }

        public int idGamePieceAttributeType { get; set; }
        [JsonIgnore]
        public Nullable<long> idGame { get; set; }
        public string pieceAttributeType { get; set; }
        [JsonIgnore]
        public virtual egs_game egs_game { get; set; }
        [JsonIgnore]
        public virtual ICollection<egs_gamePieceAttribute> egs_gamePieceAttribute { get; set; }
        [JsonProperty("gamePieceAttributeTypeLanguage")]
        public virtual ICollection<egs_gamePieceAttrubuteTypeLanguage> egs_gamePieceAttrubuteTypeLanguage { get; set; }
    }
}
