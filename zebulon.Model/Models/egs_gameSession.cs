using System;
using System.Collections.Generic;

namespace zebulon.EFModel.Models
{
    public partial class egs_gameSession
    {
        public egs_gameSession()
        {
            this.egs_gameSessionPlayer = new List<egs_gameSessionPlayer>();
        }

        public long idGameSession { get; set; }
        public Nullable<long> idGame { get; set; }
        public bool isActive { get; set; }
        public Nullable<System.DateTime> startTime { get; set; }
        public Nullable<System.DateTime> endTime { get; set; }
        public Nullable<System.DateTime> lastTurn { get; set; }
        public byte[] thumbnail { get; set; }
        public virtual egs_game egs_game { get; set; }
        public virtual ICollection<egs_gameSessionPlayer> egs_gameSessionPlayer { get; set; }
    }
}
