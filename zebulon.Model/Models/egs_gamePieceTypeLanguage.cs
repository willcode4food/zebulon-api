using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace zebulon.EFModel.Models
{
    public partial class egs_gamePieceTypeLanguage
    {
        public long idGamePIeceTypeLanguage { get; set; }
        public Nullable<int> idGamePieceType { get; set; }
        public Nullable<int> idLanguage { get; set; }
        public string pieceType { get; set; }
        [JsonIgnore]
        public virtual egs_gamePieceType egs_gamePieceType { get; set; }
        public virtual egs_language egs_language { get; set; }
    }
}
