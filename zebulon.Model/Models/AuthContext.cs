using System;
using System.Dynamic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Core.Objects;
using zebulon.EFModel.Models.Mapping;

namespace zebulon.EFModel.Models
{
    public partial class AuthContext : DbContext
    {
        static AuthContext()
        {
            Database.SetInitializer<AuthContext>(null);
        }

        public AuthContext()
            : base("Name=AuthContext")
        {
            
        }

        public DbSet<AspNetRole> AspNetRoles { get; set; }
        public DbSet<AspNetUserClaim> AspNetUserClaims { get; set; }
        public DbSet<AspNetUserLogin> AspNetUserLogins { get; set; }
        public DbSet<AspNetUser> AspNetUsers { get; set; }
        public DbSet<egs_accounts> egs_accounts { get; set; }
        public DbSet<egs_applicationTypes> egs_applicationTypes { get; set; }
        public DbSet<egs_clients> egs_clients { get; set; }
        public DbSet<egs_counter> egs_counter { get; set; }
        public DbSet<egs_counterLanguage> egs_counterLanguage { get; set; }
        public DbSet<egs_country> egs_country { get; set; }
        public DbSet<egs_game> egs_game { get; set; }
        public DbSet<egs_gameMap> egs_gameMap { get; set; }
        public DbSet<egs_gameMapType> egs_gameMapType { get; set; }
        public DbSet<egs_gamePiece> egs_gamePiece { get; set; }
        public DbSet<egs_gamePieceAttribute> egs_gamePieceAttribute { get; set; }
        public DbSet<egs_gamePieceAttributeType> egs_gamePieceAttributeType { get; set; }
        public DbSet<egs_gamePieceAttrubuteTypeLanguage> egs_gamePieceAttrubuteTypeLanguage { get; set; }
        public DbSet<egs_gamePieceCollection> egs_gamePieceCollection { get; set; }
        public DbSet<egs_gamePieceCollectionAttribute> egs_gamePieceCollectionAttribute { get; set; }
        public DbSet<egs_gamePieceCollectionAttributeLanguage> egs_gamePieceCollectionAttributeLanguage { get; set; }
        public DbSet<egs_gamePieceCollectionLanguage> egs_gamePieceCollectionLanguage { get; set; }
        public DbSet<egs_gamePieceCollectionPlayer> egs_gamePieceCollectionPlayer { get; set; }
        public DbSet<egs_gamePieceCollectionPlayerPiece> egs_gamePieceCollectionPlayerPiece { get; set; }
        public DbSet<egs_gamePieceCollectionType> egs_gamePieceCollectionType { get; set; }
        public DbSet<egs_gamePieceLanguage> egs_gamePieceLanguage { get; set; }
        public DbSet<egs_gamePieceSkin> egs_gamePieceSkin { get; set; }
        public DbSet<egs_gamePieceType> egs_gamePieceType { get; set; }
        public DbSet<egs_gamePieceTypeLanguage> egs_gamePieceTypeLanguage { get; set; }
        public DbSet<egs_gameSession> egs_gameSession { get; set; }
        public DbSet<egs_gameSessionPlayer> egs_gameSessionPlayer { get; set; }
        public DbSet<egs_gameSessionPlayerCollection> egs_gameSessionPlayerCollection { get; set; }
        public DbSet<egs_language> egs_language { get; set; }
        public DbSet<egs_player> egs_player { get; set; }
        public DbSet<egs_refreshTokens> egs_refreshTokens { get; set; }
        public DbSet<egs_region> egs_region { get; set; }
        public DbSet<v_egs_collections> v_egs_collections { get; set; }


        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new AspNetRoleMap());
            modelBuilder.Configurations.Add(new AspNetUserClaimMap());
            modelBuilder.Configurations.Add(new AspNetUserLoginMap());
            modelBuilder.Configurations.Add(new AspNetUserMap());
            modelBuilder.Configurations.Add(new egs_accountsMap());
            modelBuilder.Configurations.Add(new egs_applicationTypesMap());
            modelBuilder.Configurations.Add(new egs_clientsMap());
            modelBuilder.Configurations.Add(new egs_counterMap());
            modelBuilder.Configurations.Add(new egs_counterLanguageMap());
            modelBuilder.Configurations.Add(new egs_countryMap());
            modelBuilder.Configurations.Add(new zebulon.EFModel.Models.Mapping.egs_gameMap());
            modelBuilder.Configurations.Add(new egs_gameMapMap());
            modelBuilder.Configurations.Add(new egs_gameMapTypeMap());
            modelBuilder.Configurations.Add(new egs_gamePieceMap());
            modelBuilder.Configurations.Add(new egs_gamePieceAttributeMap());
            modelBuilder.Configurations.Add(new egs_gamePieceAttributeTypeMap());
            modelBuilder.Configurations.Add(new egs_gamePieceAttrubuteTypeLanguageMap());
            modelBuilder.Configurations.Add(new egs_gamePieceCollectionMap());
            modelBuilder.Configurations.Add(new egs_gamePieceCollectionAttributeMap());
            modelBuilder.Configurations.Add(new egs_gamePieceCollectionAttributeLanguageMap());
            modelBuilder.Configurations.Add(new egs_gamePieceCollectionLanguageMap());
            modelBuilder.Configurations.Add(new egs_gamePieceCollectionPlayerMap());
            modelBuilder.Configurations.Add(new egs_gamePieceCollectionPlayerPieceMap());
            modelBuilder.Configurations.Add(new egs_gamePieceCollectionTypeMap());
            modelBuilder.Configurations.Add(new egs_gamePieceLanguageMap());
            modelBuilder.Configurations.Add(new egs_gamePieceSkinMap());
            modelBuilder.Configurations.Add(new egs_gamePieceTypeMap());
            modelBuilder.Configurations.Add(new egs_gamePieceTypeLanguageMap());
            modelBuilder.Configurations.Add(new egs_gameSessionMap());
            modelBuilder.Configurations.Add(new egs_gameSessionPlayerMap());
            modelBuilder.Configurations.Add(new egs_gameSessionPlayerCollectionMap());
            modelBuilder.Configurations.Add(new egs_languageMap());
            modelBuilder.Configurations.Add(new egs_playerMap());
            modelBuilder.Configurations.Add(new egs_refreshTokensMap());
            modelBuilder.Configurations.Add(new egs_regionMap());
            modelBuilder.Configurations.Add(new v_egs_collectionsMap());
        }       
    }
}
