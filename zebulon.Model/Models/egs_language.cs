using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace zebulon.EFModel.Models
{
    public partial class egs_language
    {
        public egs_language()
        {
            this.egs_counterLanguage = new List<egs_counterLanguage>();
            this.egs_gamePieceAttrubuteTypeLanguage = new List<egs_gamePieceAttrubuteTypeLanguage>();
            this.egs_gamePieceCollectionAttributeLanguage = new List<egs_gamePieceCollectionAttributeLanguage>();
            this.egs_gamePieceCollectionLanguage = new List<egs_gamePieceCollectionLanguage>();
            this.egs_gamePieceLanguage = new List<egs_gamePieceLanguage>();
            this.egs_gamePieceTypeLanguage = new List<egs_gamePieceTypeLanguage>();
        }

        public int idLanguage { get; set; }
        public string language { get; set; }
        public string culture { get; set; }
        [JsonIgnore]
        public virtual ICollection<egs_counterLanguage> egs_counterLanguage { get; set; }
        [JsonIgnore]
        public virtual ICollection<egs_gamePieceAttrubuteTypeLanguage> egs_gamePieceAttrubuteTypeLanguage { get; set; }
        [JsonIgnore]
        public virtual ICollection<egs_gamePieceCollectionAttributeLanguage> egs_gamePieceCollectionAttributeLanguage { get; set; }
        [JsonIgnore]
        public virtual ICollection<egs_gamePieceCollectionLanguage> egs_gamePieceCollectionLanguage { get; set; }
        [JsonIgnore]
        public virtual ICollection<egs_gamePieceLanguage> egs_gamePieceLanguage { get; set; }
        [JsonIgnore]
        public virtual ICollection<egs_gamePieceTypeLanguage> egs_gamePieceTypeLanguage { get; set; }
    }
}
