using System;
using System.Collections.Generic;
using Newtonsoft.Json;
namespace zebulon.EFModel.Models
{
    public partial class egs_gamePieceSkin
    {
        public int idGamePieceSkin { get; set; }
        [JsonIgnore]
        public Nullable<long> idGamePiece { get; set; }
        public string smallImg { get; set; }
        public string bigImg { get; set; }
        public string minImg { get; set; }
        public string font1 { get; set; }
        public string font2 { get; set; }
        [JsonIgnore]
        public virtual egs_gamePiece egs_gamePiece { get; set; }
    }
}
