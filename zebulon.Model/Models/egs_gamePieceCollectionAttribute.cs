using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;

namespace zebulon.EFModel.Models
{
    public partial class egs_gamePieceCollectionAttribute
    {
        public egs_gamePieceCollectionAttribute()
        {
            this.egs_gamePieceCollectionAttributeLanguage = new List<egs_gamePieceCollectionAttributeLanguage>();
        }
        [Key]
        public long idGamePieceCollectionAttribute { get; set; }
        public Nullable<long> idGamePieceCollection { get; set; }
        public string value { get; set; }
        public string dataName { get; set; }
        public virtual egs_gamePieceCollection egs_gamePieceCollection { get; set; }
        public virtual ICollection<egs_gamePieceCollectionAttributeLanguage> egs_gamePieceCollectionAttributeLanguage { get; set; }
    }
}
