using System;
using System.Collections.Generic;

namespace zebulon.EFModel.Models
{
    public partial class v_egs_collections
    {
        public long idGamePieceCollection { get; set; }
        public Nullable<long> idGame { get; set; }
        public string collectionType { get; set; }
        public Nullable<int> size { get; set; }
        public string collectionName { get; set; }
        public string culture { get; set; }
    }
}
