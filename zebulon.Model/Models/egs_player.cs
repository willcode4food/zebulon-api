using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace zebulon.EFModel.Models
{
    public partial class egs_player
    {
        public egs_player()
        {
            this.egs_gamePieceCollectionPlayer = new List<egs_gamePieceCollectionPlayer>();
            this.egs_gameSessionPlayer = new List<egs_gameSessionPlayer>();
        }

        public string idUser { get; set; }
        public long idPlayer { get; set; }
        public string firstname { get; set; }
        public string lastname { get; set; }
        public string city { get; set; }
        public Nullable<int> idRegion { get; set; }
        public string nickName { get; set; }
        [JsonIgnore]
        public virtual AspNetUser AspNetUser { get; set; }
        [JsonIgnore]
        public virtual ICollection<egs_gamePieceCollectionPlayer> egs_gamePieceCollectionPlayer { get; set; }
        [JsonIgnore]
        public virtual ICollection<egs_gameSessionPlayer> egs_gameSessionPlayer { get; set; }
        public virtual egs_region egs_region { get; set; }
    }
}
