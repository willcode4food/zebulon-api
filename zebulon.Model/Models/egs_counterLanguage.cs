using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace zebulon.EFModel.Models
{
    public partial class egs_counterLanguage
    {
        public int idCounterLanguage { get; set; }
        public Nullable<int> idLanuage { get; set; }
        public Nullable<int> idCounter { get; set; }
        public string counterName { get; set; }
        [JsonIgnore]
        public virtual egs_counter egs_counter { get; set; }
        public virtual egs_language egs_language { get; set; }
    }
}
