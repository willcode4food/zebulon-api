using System;
using System.Collections.Generic;
using Newtonsoft.Json;
namespace zebulon.EFModel.Models
{
    public partial class egs_region
    {
        public egs_region()
        {
            this.egs_player = new List<egs_player>();
        }

        public int idRegion { get; set; }
        public Nullable<int> idCountry { get; set; }
        public string abbreviation { get; set; }
        public string name { get; set; }
        public virtual egs_country egs_country { get; set; }
        [JsonIgnore]
        public virtual ICollection<egs_player> egs_player { get; set; }
    }
}
