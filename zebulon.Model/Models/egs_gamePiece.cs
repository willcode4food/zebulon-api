using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace zebulon.EFModel.Models
{
    public partial class egs_gamePiece
    {
        public egs_gamePiece()
        {
            this.egs_gamePieceAttribute = new List<egs_gamePieceAttribute>();
            this.egs_gamePieceCollectionPlayerPiece = new List<egs_gamePieceCollectionPlayerPiece>();
            this.egs_gamePieceLanguage = new List<egs_gamePieceLanguage>();
            this.egs_gamePieceSkin = new List<egs_gamePieceSkin>();
        }

        public long idGamePiece { get; set; }
        public Nullable<long> idGame { get; set; }
        public Nullable<int> idGamePieceType { get; set; }
        public Nullable<System.DateTime> createdDate { get; set; }
        public Nullable<System.DateTime> updatedDate { get; set; }
        public Nullable<bool> isActive { get; set; }
        public string createdBy { get; set; }
        [JsonIgnore]
        public virtual AspNetUser AspNetUser { get; set; }
        [JsonIgnore]
        public virtual egs_game egs_game { get; set; }
        public virtual egs_gamePieceType egs_gamePieceType { get; set; }
        public virtual ICollection<egs_gamePieceAttribute> egs_gamePieceAttribute { get; set; }
        public virtual ICollection<egs_gamePieceCollectionPlayerPiece> egs_gamePieceCollectionPlayerPiece { get; set; }
        public virtual ICollection<egs_gamePieceLanguage> egs_gamePieceLanguage { get; set; }
        public virtual ICollection<egs_gamePieceSkin> egs_gamePieceSkin { get; set; }
    }
}
