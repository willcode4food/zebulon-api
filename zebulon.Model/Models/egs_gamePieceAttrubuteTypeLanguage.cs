using System;
using System.Collections.Generic;
using Newtonsoft.Json;
namespace zebulon.EFModel.Models
{
    public partial class egs_gamePieceAttrubuteTypeLanguage
    {
        public long idGamePieceAttributeLanguage { get; set; }
        public Nullable<int> idGamePieceAttributeType { get; set; }
        public Nullable<int> idLanguage { get; set; }
        public string name { get; set; }
        [JsonIgnore]
        public virtual egs_gamePieceAttributeType egs_gamePieceAttributeType { get; set; }
        [JsonProperty("language")]
        public virtual egs_language egs_language { get; set; }
    }
}
