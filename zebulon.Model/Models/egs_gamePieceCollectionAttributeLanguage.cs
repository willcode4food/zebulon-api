using System;
using System.Collections.Generic;

namespace zebulon.EFModel.Models
{
    public partial class egs_gamePieceCollectionAttributeLanguage
    {
        public long idGamePieceCollectionAttributeLanguage { get; set; }
        public Nullable<long> idGamePIeceCollectionAttribute { get; set; }
        public Nullable<int> idLanguage { get; set; }
        public string name { get; set; }
        public virtual egs_gamePieceCollectionAttribute egs_gamePieceCollectionAttribute { get; set; }
        public virtual egs_language egs_language { get; set; }
    }
}
