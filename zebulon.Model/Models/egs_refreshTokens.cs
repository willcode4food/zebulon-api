using System;
using System.Collections.Generic;

namespace zebulon.EFModel.Models
{
    public partial class egs_refreshTokens
    {
        public long idRefeshToken { get; set; }
        public string idAspNetUser { get; set; }
        public string token { get; set; }
        public string protectedTicket { get; set; }
        public Nullable<System.DateTime> issuedUTC { get; set; }
        public Nullable<System.DateTime> expiredUTC { get; set; }
        public string clientIdentifier { get; set; }
    }
}
