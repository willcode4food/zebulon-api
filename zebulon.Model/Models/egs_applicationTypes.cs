using System;
using System.Collections.Generic;

namespace zebulon.EFModel.Models
{
    public partial class egs_applicationTypes
    {
        public egs_applicationTypes()
        {
            this.egs_clients = new List<egs_clients>();
            this.egs_clients1 = new List<egs_clients>();
        }

        public long idApplicationType { get; set; }
        public string applicationType { get; set; }
        public virtual ICollection<egs_clients> egs_clients { get; set; }
        public virtual ICollection<egs_clients> egs_clients1 { get; set; }
    }
}
