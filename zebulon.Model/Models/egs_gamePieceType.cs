using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace zebulon.EFModel.Models
{
    public partial class egs_gamePieceType
    {
        public egs_gamePieceType()
        {
            this.egs_gamePiece = new List<egs_gamePiece>();
            this.egs_gamePieceTypeLanguage = new List<egs_gamePieceTypeLanguage>();
        }

        public int idGamePieceType { get; set; }
        public long idGame { get; set; }
        [JsonIgnore]
        public virtual egs_game egs_game { get; set; }
        [JsonIgnore]
        public virtual ICollection<egs_gamePiece> egs_gamePiece { get; set; }
        public virtual ICollection<egs_gamePieceTypeLanguage> egs_gamePieceTypeLanguage { get; set; }
    }
}
