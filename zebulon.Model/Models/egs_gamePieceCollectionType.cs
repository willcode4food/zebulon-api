using System;
using System.Collections.Generic;
using Newtonsoft.Json;
namespace zebulon.EFModel.Models
{
    public partial class egs_gamePieceCollectionType
    {
        public egs_gamePieceCollectionType()
        {
            this.egs_gamePieceCollection = new List<egs_gamePieceCollection>();
        }

        public int idGamePieceCollectionType { get; set; }
        public Nullable<long> idGame { get; set; }
        public string collectionType { get; set; }
        [JsonIgnore]
        public virtual egs_game egs_game { get; set; }
        [JsonIgnore]
        public virtual ICollection<egs_gamePieceCollection> egs_gamePieceCollection { get; set; }
    }
}
