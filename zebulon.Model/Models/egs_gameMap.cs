using System;
using System.Collections.Generic;

namespace zebulon.EFModel.Models
{
    public partial class egs_gameMap
    {
        public long idGameMap { get; set; }
        public Nullable<int> idGameMapType { get; set; }
        public Nullable<long> idGame { get; set; }
        public string name { get; set; }
        public Nullable<int> numCols { get; set; }
        public Nullable<int> numRows { get; set; }
        public virtual egs_game egs_game { get; set; }
        public virtual egs_gameMapType egs_gameMapType { get; set; }
    }
}
