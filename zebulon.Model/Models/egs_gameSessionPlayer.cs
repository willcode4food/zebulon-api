using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace zebulon.EFModel.Models
{
    public partial class egs_gameSessionPlayer
    {
        public egs_gameSessionPlayer()
        {
            this.egs_gameSessionPlayerCollection = new List<egs_gameSessionPlayerCollection>();
        }

        public long idGameSessionPlayer { get; set; }
        public long idPlayer { get; set; }
        public long idGameSession { get; set; }
        public bool isCreator { get; set; }
        public Nullable<bool> hasAccepted { get; set; }
        [JsonIgnore]
        public virtual egs_gameSession egs_gameSession { get; set; }
        [JsonIgnore]
        public virtual ICollection<egs_gameSessionPlayerCollection> egs_gameSessionPlayerCollection { get; set; }
        public virtual egs_player egs_player { get; set; }
    }
}
