using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace zebulon.EFModel.Models.Mapping
{
    public class egs_gamePieceMap : EntityTypeConfiguration<egs_gamePiece>
    {
        public egs_gamePieceMap()
        {
            // Primary Key
            this.HasKey(t => t.idGamePiece);

            // Properties
            this.Property(t => t.createdBy)
                .HasMaxLength(128);

            // Table & Column Mappings
            this.ToTable("egs_gamePiece");
            this.Property(t => t.idGamePiece).HasColumnName("idGamePiece");
            this.Property(t => t.idGame).HasColumnName("idGame");
            this.Property(t => t.idGamePieceType).HasColumnName("idGamePieceType");
            this.Property(t => t.createdDate).HasColumnName("createdDate");
            this.Property(t => t.updatedDate).HasColumnName("updatedDate");
            this.Property(t => t.isActive).HasColumnName("isActive");
            this.Property(t => t.createdBy).HasColumnName("createdBy");

            // Relationships
            this.HasOptional(t => t.AspNetUser)
                .WithMany(t => t.egs_gamePiece)
                .HasForeignKey(d => d.createdBy);
            this.HasOptional(t => t.egs_game)
                .WithMany(t => t.egs_gamePiece)
                .HasForeignKey(d => d.idGame);
            this.HasOptional(t => t.egs_gamePieceType)
                .WithMany(t => t.egs_gamePiece)
                .HasForeignKey(d => d.idGamePieceType);

        }
    }
}
