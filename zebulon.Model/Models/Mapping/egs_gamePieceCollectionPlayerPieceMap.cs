using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace zebulon.EFModel.Models.Mapping
{
    public class egs_gamePieceCollectionPlayerPieceMap : EntityTypeConfiguration<egs_gamePieceCollectionPlayerPiece>
    {
        public egs_gamePieceCollectionPlayerPieceMap()
        {
            // Primary Key
            this.HasKey(t => t.idGamePieceCollectionPlayerPiece);

            // Properties
            // Table & Column Mappings
            this.ToTable("egs_gamePieceCollectionPlayerPiece");
            this.Property(t => t.idGamePieceCollectionPlayerPiece).HasColumnName("idGamePieceCollectionPlayerPiece");
            this.Property(t => t.idGamePIeceCollectionPlayer).HasColumnName("idGamePIeceCollectionPlayer");
            this.Property(t => t.idGamePiece).HasColumnName("idGamePiece");

            // Relationships
            this.HasRequired(t => t.egs_gamePiece)
                .WithMany(t => t.egs_gamePieceCollectionPlayerPiece)
                .HasForeignKey(d => d.idGamePiece);
            this.HasRequired(t => t.egs_gamePieceCollectionPlayer)
                .WithMany(t => t.egs_gamePieceCollectionPlayerPiece)
                .HasForeignKey(d => d.idGamePIeceCollectionPlayer);

        }
    }
}
