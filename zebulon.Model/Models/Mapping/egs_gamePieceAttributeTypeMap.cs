using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace zebulon.EFModel.Models.Mapping
{
    public class egs_gamePieceAttributeTypeMap : EntityTypeConfiguration<egs_gamePieceAttributeType>
    {
        public egs_gamePieceAttributeTypeMap()
        {
            // Primary Key
            this.HasKey(t => t.idGamePieceAttributeType);

            // Properties
            this.Property(t => t.pieceAttributeType)
                .HasMaxLength(100);

            // Table & Column Mappings
            this.ToTable("egs_gamePieceAttributeType");
            this.Property(t => t.idGamePieceAttributeType).HasColumnName("idGamePieceAttributeType");
            this.Property(t => t.idGame).HasColumnName("idGame");
            this.Property(t => t.pieceAttributeType).HasColumnName("pieceAttributeType");

            // Relationships
            this.HasOptional(t => t.egs_game)
                .WithMany(t => t.egs_gamePieceAttributeType)
                .HasForeignKey(d => d.idGame);

        }
    }
}
