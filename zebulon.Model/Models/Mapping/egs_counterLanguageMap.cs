using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace zebulon.EFModel.Models.Mapping
{
    public class egs_counterLanguageMap : EntityTypeConfiguration<egs_counterLanguage>
    {
        public egs_counterLanguageMap()
        {
            // Primary Key
            this.HasKey(t => t.idCounterLanguage);

            // Properties
            this.Property(t => t.counterName)
                .HasMaxLength(100);

            // Table & Column Mappings
            this.ToTable("egs_counterLanguage");
            this.Property(t => t.idCounterLanguage).HasColumnName("idCounterLanguage");
            this.Property(t => t.idLanuage).HasColumnName("idLanuage");
            this.Property(t => t.idCounter).HasColumnName("idCounter");
            this.Property(t => t.counterName).HasColumnName("counterName");

            // Relationships
            this.HasOptional(t => t.egs_counter)
                .WithMany(t => t.egs_counterLanguage)
                .HasForeignKey(d => d.idCounter);
            this.HasOptional(t => t.egs_language)
                .WithMany(t => t.egs_counterLanguage)
                .HasForeignKey(d => d.idLanuage);

        }
    }
}
