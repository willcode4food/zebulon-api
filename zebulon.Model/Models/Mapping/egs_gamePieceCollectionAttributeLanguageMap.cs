using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace zebulon.EFModel.Models.Mapping
{
    public class egs_gamePieceCollectionAttributeLanguageMap : EntityTypeConfiguration<egs_gamePieceCollectionAttributeLanguage>
    {
        public egs_gamePieceCollectionAttributeLanguageMap()
        {
            // Primary Key
            this.HasKey(t => t.idGamePieceCollectionAttributeLanguage);

            // Properties
            this.Property(t => t.name)
                .HasMaxLength(500);

            // Table & Column Mappings
            this.ToTable("egs_gamePieceCollectionAttributeLanguage");
            this.Property(t => t.idGamePieceCollectionAttributeLanguage).HasColumnName("idGamePieceCollectionAttributeLanguage");
            this.Property(t => t.idGamePIeceCollectionAttribute).HasColumnName("idGamePIeceCollectionAttribute");
            this.Property(t => t.idLanguage).HasColumnName("idLanguage");
            this.Property(t => t.name).HasColumnName("name");

            // Relationships
            this.HasOptional(t => t.egs_gamePieceCollectionAttribute)
                .WithMany(t => t.egs_gamePieceCollectionAttributeLanguage)
                .HasForeignKey(d => d.idGamePIeceCollectionAttribute);
            this.HasOptional(t => t.egs_language)
                .WithMany(t => t.egs_gamePieceCollectionAttributeLanguage)
                .HasForeignKey(d => d.idLanguage);

        }
    }
}
