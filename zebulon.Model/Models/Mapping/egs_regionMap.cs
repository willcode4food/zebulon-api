using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace zebulon.EFModel.Models.Mapping
{
    public class egs_regionMap : EntityTypeConfiguration<egs_region>
    {
        public egs_regionMap()
        {
            // Primary Key
            this.HasKey(t => t.idRegion);

            // Properties
            this.Property(t => t.abbreviation)
                .HasMaxLength(10);

            this.Property(t => t.name)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("egs_region");
            this.Property(t => t.idRegion).HasColumnName("idRegion");
            this.Property(t => t.idCountry).HasColumnName("idCountry");
            this.Property(t => t.abbreviation).HasColumnName("abbreviation");
            this.Property(t => t.name).HasColumnName("name");

            // Relationships
            this.HasOptional(t => t.egs_country)
                .WithMany(t => t.egs_region)
                .HasForeignKey(d => d.idCountry);

        }
    }
}
