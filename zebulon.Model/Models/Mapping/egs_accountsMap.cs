using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace zebulon.EFModel.Models.Mapping
{
    public class egs_accountsMap : EntityTypeConfiguration<egs_accounts>
    {
        public egs_accountsMap()
        {
            // Primary Key
            this.HasKey(t => t.idAccount);

            // Properties
            this.Property(t => t.idAspNetRoles)
                .HasMaxLength(128);

            this.Property(t => t.accountName)
                .HasMaxLength(256);

            // Table & Column Mappings
            this.ToTable("egs_accounts");
            this.Property(t => t.idAccount).HasColumnName("idAccount");
            this.Property(t => t.idAspNetRoles).HasColumnName("idAspNetRoles");
            this.Property(t => t.accountName).HasColumnName("accountName");
        }
    }
}
