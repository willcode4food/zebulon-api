using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace zebulon.EFModel.Models.Mapping
{
    public class egs_gamePieceCollectionMap : EntityTypeConfiguration<egs_gamePieceCollection>
    {
        public egs_gamePieceCollectionMap()
        {
            // Primary Key
            this.HasKey(t => t.idGamePieceCollection);

            // Properties
            // Table & Column Mappings
            this.ToTable("egs_gamePieceCollection");
            this.Property(t => t.idGamePieceCollection).HasColumnName("idGamePieceCollection");
            this.Property(t => t.idGame).HasColumnName("idGame");
            this.Property(t => t.idGamePieceCollectionType).HasColumnName("idGamePieceCollectionType");
            this.Property(t => t.size).HasColumnName("size");

            // Relationships
            this.HasOptional(t => t.egs_game)
                .WithMany(t => t.egs_gamePieceCollection)
                .HasForeignKey(d => d.idGame);
            this.HasOptional(t => t.egs_gamePieceCollectionType)
                .WithMany(t => t.egs_gamePieceCollection)
                .HasForeignKey(d => d.idGamePieceCollectionType);

        }
    }
}
