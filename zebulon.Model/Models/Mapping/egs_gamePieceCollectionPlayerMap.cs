using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace zebulon.EFModel.Models.Mapping
{
    public class egs_gamePieceCollectionPlayerMap : EntityTypeConfiguration<egs_gamePieceCollectionPlayer>
    {
        public egs_gamePieceCollectionPlayerMap()
        {
            // Primary Key
            this.HasKey(t => t.idGamePieceCollectionPlayer);

            // Properties
            // Table & Column Mappings
            this.ToTable("egs_gamePieceCollectionPlayer");
            this.Property(t => t.idGamePieceCollectionPlayer).HasColumnName("idGamePieceCollectionPlayer");
            this.Property(t => t.idGamePieceCollection).HasColumnName("idGamePieceCollection");
            this.Property(t => t.idPlayer).HasColumnName("idPlayer");
            this.Property(t => t.isDefault).HasColumnName("isDefault");
            this.Property(t => t.dateCreated).HasColumnName("dateCreated");
            this.Property(t => t.dateUpdated).HasColumnName("dateUpdated");

            // Relationships
            this.HasRequired(t => t.egs_gamePieceCollection)
                .WithMany(t => t.egs_gamePieceCollectionPlayer)
                .HasForeignKey(d => d.idGamePieceCollection);
            this.HasRequired(t => t.egs_player)
                .WithMany(t => t.egs_gamePieceCollectionPlayer)
                .HasForeignKey(d => d.idPlayer);

        }
    }
}
