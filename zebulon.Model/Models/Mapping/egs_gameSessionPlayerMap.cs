using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace zebulon.EFModel.Models.Mapping
{
    public class egs_gameSessionPlayerMap : EntityTypeConfiguration<egs_gameSessionPlayer>
    {
        public egs_gameSessionPlayerMap()
        {
            // Primary Key
            this.HasKey(t => t.idGameSessionPlayer);

            // Properties
            // Table & Column Mappings
            this.ToTable("egs_gameSessionPlayer");
            this.Property(t => t.idGameSessionPlayer).HasColumnName("idGameSessionPlayer");
            this.Property(t => t.idPlayer).HasColumnName("idPlayer");
            this.Property(t => t.idGameSession).HasColumnName("idGameSession");
            this.Property(t => t.isCreator).HasColumnName("isCreator");
            this.Property(t => t.hasAccepted).HasColumnName("hasAccepted");

            // Relationships
            this.HasRequired(t => t.egs_gameSession)
                .WithMany(t => t.egs_gameSessionPlayer)
                .HasForeignKey(d => d.idGameSession);
            this.HasRequired(t => t.egs_player)
                .WithMany(t => t.egs_gameSessionPlayer)
                .HasForeignKey(d => d.idPlayer);

        }
    }
}
