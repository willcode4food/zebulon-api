using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace zebulon.EFModel.Models.Mapping
{
    public class egs_playerMap : EntityTypeConfiguration<egs_player>
    {
        public egs_playerMap()
        {
            // Primary Key
            this.HasKey(t => t.idPlayer);

            // Properties
            this.Property(t => t.idUser)
                .HasMaxLength(128);

            this.Property(t => t.firstname)
                .HasMaxLength(50);

            this.Property(t => t.lastname)
                .HasMaxLength(50);

            this.Property(t => t.city)
                .HasMaxLength(50);

            this.Property(t => t.nickName)
                .HasMaxLength(128);

            // Table & Column Mappings
            this.ToTable("egs_player");
            this.Property(t => t.idUser).HasColumnName("idUser");
            this.Property(t => t.idPlayer).HasColumnName("idPlayer");
            this.Property(t => t.firstname).HasColumnName("firstname");
            this.Property(t => t.lastname).HasColumnName("lastname");
            this.Property(t => t.city).HasColumnName("city");
            this.Property(t => t.idRegion).HasColumnName("idRegion");
            this.Property(t => t.nickName).HasColumnName("nickName");

            // Relationships
            this.HasOptional(t => t.AspNetUser)
                .WithMany(t => t.egs_player)
                .HasForeignKey(d => d.idUser);
            this.HasOptional(t => t.egs_region)
                .WithMany(t => t.egs_player)
                .HasForeignKey(d => d.idRegion);

        }
    }
}
