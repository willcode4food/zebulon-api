using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace zebulon.EFModel.Models.Mapping
{
    public class egs_gamePieceCollectionAttributeMap : EntityTypeConfiguration<egs_gamePieceCollectionAttribute>
    {
        public egs_gamePieceCollectionAttributeMap()
        {
            // Primary Key
            this.HasKey(t => t.idGamePieceCollectionAttribute);

            // Properties
            this.Property(t => t.dataName)
                .HasMaxLength(500);

            // Table & Column Mappings
            this.ToTable("egs_gamePieceCollectionAttribute");
            this.Property(t => t.idGamePieceCollectionAttribute).HasColumnName("idGamePieceCollectionAttribute");
            this.Property(t => t.idGamePieceCollection).HasColumnName("idGamePieceCollection");
            this.Property(t => t.value).HasColumnName("value");
            this.Property(t => t.dataName).HasColumnName("dataName");

            // Relationships
            this.HasOptional(t => t.egs_gamePieceCollection)
                .WithMany(t => t.egs_gamePieceCollectionAttribute)
                .HasForeignKey(d => d.idGamePieceCollection);

        }
    }
}
