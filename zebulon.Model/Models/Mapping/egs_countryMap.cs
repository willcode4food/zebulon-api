using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace zebulon.EFModel.Models.Mapping
{
    public class egs_countryMap : EntityTypeConfiguration<egs_country>
    {
        public egs_countryMap()
        {
            // Primary Key
            this.HasKey(t => t.idCountry);

            // Properties
            this.Property(t => t.name)
                .HasMaxLength(150);

            // Table & Column Mappings
            this.ToTable("egs_country");
            this.Property(t => t.idCountry).HasColumnName("idCountry");
            this.Property(t => t.name).HasColumnName("name");
        }
    }
}
