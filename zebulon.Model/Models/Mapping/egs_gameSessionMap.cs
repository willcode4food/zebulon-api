using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace zebulon.EFModel.Models.Mapping
{
    public class egs_gameSessionMap : EntityTypeConfiguration<egs_gameSession>
    {
        public egs_gameSessionMap()
        {
            // Primary Key
            this.HasKey(t => t.idGameSession);

            // Properties
            // Table & Column Mappings
            this.ToTable("egs_gameSession");
            this.Property(t => t.idGameSession).HasColumnName("idGameSession");
            this.Property(t => t.idGame).HasColumnName("idGame");
            this.Property(t => t.isActive).HasColumnName("isActive");
            this.Property(t => t.startTime).HasColumnName("startTime");
            this.Property(t => t.endTime).HasColumnName("endTime");
            this.Property(t => t.lastTurn).HasColumnName("lastTurn");
            this.Property(t => t.thumbnail).HasColumnName("thumbnail");

            // Relationships
            this.HasOptional(t => t.egs_game)
                .WithMany(t => t.egs_gameSession)
                .HasForeignKey(d => d.idGame);

        }
    }
}
