using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace zebulon.EFModel.Models.Mapping
{
    public class egs_gamePieceCollectionTypeMap : EntityTypeConfiguration<egs_gamePieceCollectionType>
    {
        public egs_gamePieceCollectionTypeMap()
        {
            // Primary Key
            this.HasKey(t => t.idGamePieceCollectionType);

            // Properties
            this.Property(t => t.collectionType)
                .HasMaxLength(100);

            // Table & Column Mappings
            this.ToTable("egs_gamePieceCollectionType");
            this.Property(t => t.idGamePieceCollectionType).HasColumnName("idGamePieceCollectionType");
            this.Property(t => t.idGame).HasColumnName("idGame");
            this.Property(t => t.collectionType).HasColumnName("collectionType");

            // Relationships
            this.HasOptional(t => t.egs_game)
                .WithMany(t => t.egs_gamePieceCollectionType)
                .HasForeignKey(d => d.idGame);

        }
    }
}
