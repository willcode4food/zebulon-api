using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace zebulon.EFModel.Models.Mapping
{
    public class egs_gameMap : EntityTypeConfiguration<egs_game>
    {
        public egs_gameMap()
        {
            // Primary Key
            this.HasKey(t => t.idGame);

            // Properties
            this.Property(t => t.name)
                .HasMaxLength(150);

            // Table & Column Mappings
            this.ToTable("egs_game");
            this.Property(t => t.idGame).HasColumnName("idGame");
            this.Property(t => t.idAccount).HasColumnName("idAccount");
            this.Property(t => t.name).HasColumnName("name");
            this.Property(t => t.description).HasColumnName("description");

            // Relationships
            this.HasOptional(t => t.egs_accounts)
                .WithMany(t => t.egs_game)
                .HasForeignKey(d => d.idAccount);

        }
    }
}
