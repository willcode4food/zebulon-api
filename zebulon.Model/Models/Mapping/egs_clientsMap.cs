using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace zebulon.EFModel.Models.Mapping
{
    public class egs_clientsMap : EntityTypeConfiguration<egs_clients>
    {
        public egs_clientsMap()
        {
            // Primary Key
            this.HasKey(t => t.idClient);

            // Properties
            this.Property(t => t.name)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.secret)
                .IsRequired()
                .HasMaxLength(4000);

            this.Property(t => t.description)
                .HasMaxLength(150);

            this.Property(t => t.allowedOrigin)
                .HasMaxLength(500);

            this.Property(t => t.clientIdentifier)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("egs_clients");
            this.Property(t => t.idClient).HasColumnName("idClient");
            this.Property(t => t.idAccount).HasColumnName("idAccount");
            this.Property(t => t.idApplicationType).HasColumnName("idApplicationType");
            this.Property(t => t.name).HasColumnName("name");
            this.Property(t => t.secret).HasColumnName("secret");
            this.Property(t => t.description).HasColumnName("description");
            this.Property(t => t.isActive).HasColumnName("isActive");
            this.Property(t => t.refreshTokenLifetime).HasColumnName("refreshTokenLifetime");
            this.Property(t => t.allowedOrigin).HasColumnName("allowedOrigin");
            this.Property(t => t.clientIdentifier).HasColumnName("clientIdentifier");

            // Relationships
            this.HasRequired(t => t.egs_accounts)
                .WithMany(t => t.egs_clients)
                .HasForeignKey(d => d.idAccount);
            this.HasRequired(t => t.egs_applicationTypes)
                .WithMany(t => t.egs_clients)
                .HasForeignKey(d => d.idApplicationType);
            this.HasRequired(t => t.egs_applicationTypes1)
                .WithMany(t => t.egs_clients1)
                .HasForeignKey(d => d.idApplicationType);

        }
    }
}
