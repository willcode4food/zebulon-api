using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace zebulon.EFModel.Models.Mapping
{
    public class egs_gameMapMap : EntityTypeConfiguration<zebulon.EFModel.Models.egs_gameMap>
    {
        public egs_gameMapMap()
        {
            // Primary Key
            this.HasKey(t => t.idGameMap);

            // Properties
            this.Property(t => t.name)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("egs_gameMap");
            this.Property(t => t.idGameMap).HasColumnName("idGameMap");
            this.Property(t => t.idGameMapType).HasColumnName("idGameMapType");
            this.Property(t => t.idGame).HasColumnName("idGame");
            this.Property(t => t.name).HasColumnName("name");
            this.Property(t => t.numCols).HasColumnName("numCols");
            this.Property(t => t.numRows).HasColumnName("numRows");

            // Relationships
            this.HasOptional(t => t.egs_game)
                .WithMany(t => t.egs_gameMap)
                .HasForeignKey(d => d.idGame);
            this.HasOptional(t => t.egs_gameMapType)
                .WithMany(t => t.egs_gameMap)
                .HasForeignKey(d => d.idGameMapType);

        }
    }
}
