using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace zebulon.EFModel.Models.Mapping
{
    public class v_egs_collectionsMap : EntityTypeConfiguration<v_egs_collections>
    {
        public v_egs_collectionsMap()
        {
            // Primary Key
            this.HasKey(t => t.idGamePieceCollection);

            // Properties
            this.Property(t => t.idGamePieceCollection)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.collectionType)
                .HasMaxLength(100);

            this.Property(t => t.collectionName)
                .HasMaxLength(100);

            this.Property(t => t.culture)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("v_egs_collections");
            this.Property(t => t.idGamePieceCollection).HasColumnName("idGamePieceCollection");
            this.Property(t => t.idGame).HasColumnName("idGame");
            this.Property(t => t.collectionType).HasColumnName("collectionType");
            this.Property(t => t.size).HasColumnName("size");
            this.Property(t => t.collectionName).HasColumnName("collectionName");
            this.Property(t => t.culture).HasColumnName("culture");
        }
    }
}
