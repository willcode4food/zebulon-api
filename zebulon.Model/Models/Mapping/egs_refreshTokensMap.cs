using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace zebulon.EFModel.Models.Mapping
{
    public class egs_refreshTokensMap : EntityTypeConfiguration<egs_refreshTokens>
    {
        public egs_refreshTokensMap()
        {
            // Primary Key
            this.HasKey(t => t.idRefeshToken);

            // Properties
            this.Property(t => t.idAspNetUser)
                .HasMaxLength(128);

            this.Property(t => t.token)
                .HasMaxLength(4000);

            this.Property(t => t.protectedTicket)
                .HasMaxLength(4000);

            this.Property(t => t.clientIdentifier)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("egs_refreshTokens");
            this.Property(t => t.idRefeshToken).HasColumnName("idRefeshToken");
            this.Property(t => t.idAspNetUser).HasColumnName("idAspNetUser");
            this.Property(t => t.token).HasColumnName("token");
            this.Property(t => t.protectedTicket).HasColumnName("protectedTicket");
            this.Property(t => t.issuedUTC).HasColumnName("issuedUTC");
            this.Property(t => t.expiredUTC).HasColumnName("expiredUTC");
            this.Property(t => t.clientIdentifier).HasColumnName("clientIdentifier");
        }
    }
}
