using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace zebulon.EFModel.Models.Mapping
{
    public class egs_gamePieceAttributeMap : EntityTypeConfiguration<egs_gamePieceAttribute>
    {
        public egs_gamePieceAttributeMap()
        {
            // Primary Key
            this.HasKey(t => t.idGamePieceAttribute);

            // Properties
            // Table & Column Mappings
            this.ToTable("egs_gamePieceAttribute");
            this.Property(t => t.idGamePieceAttribute).HasColumnName("idGamePieceAttribute");
            this.Property(t => t.idGamePiece).HasColumnName("idGamePiece");
            this.Property(t => t.idGamePieceAttributeType).HasColumnName("idGamePieceAttributeType");
            this.Property(t => t.value).HasColumnName("value");

            // Relationships
            this.HasOptional(t => t.egs_gamePiece)
                .WithMany(t => t.egs_gamePieceAttribute)
                .HasForeignKey(d => d.idGamePiece);
            this.HasOptional(t => t.egs_gamePieceAttributeType)
                .WithMany(t => t.egs_gamePieceAttribute)
                .HasForeignKey(d => d.idGamePieceAttributeType);

        }
    }
}
