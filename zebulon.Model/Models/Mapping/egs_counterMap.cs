using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace zebulon.EFModel.Models.Mapping
{
    public class egs_counterMap : EntityTypeConfiguration<egs_counter>
    {
        public egs_counterMap()
        {
            // Primary Key
            this.HasKey(t => t.idCounter);

            // Properties
            // Table & Column Mappings
            this.ToTable("egs_counter");
            this.Property(t => t.idCounter).HasColumnName("idCounter");
            this.Property(t => t.idGame).HasColumnName("idGame");
            this.Property(t => t.counterStart).HasColumnName("counterStart");
            this.Property(t => t.counterEnd).HasColumnName("counterEnd");

            // Relationships
            this.HasOptional(t => t.egs_game)
                .WithMany(t => t.egs_counter)
                .HasForeignKey(d => d.idGame);

        }
    }
}
