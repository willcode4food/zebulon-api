using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace zebulon.EFModel.Models.Mapping
{
    public class egs_applicationTypesMap : EntityTypeConfiguration<egs_applicationTypes>
    {
        public egs_applicationTypesMap()
        {
            // Primary Key
            this.HasKey(t => t.idApplicationType);

            // Properties
            this.Property(t => t.applicationType)
                .HasMaxLength(150);

            // Table & Column Mappings
            this.ToTable("egs_applicationTypes");
            this.Property(t => t.idApplicationType).HasColumnName("idApplicationType");
            this.Property(t => t.applicationType).HasColumnName("applicationType");
        }
    }
}
