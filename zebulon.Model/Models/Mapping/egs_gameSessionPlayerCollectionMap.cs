using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace zebulon.EFModel.Models.Mapping
{
    public class egs_gameSessionPlayerCollectionMap : EntityTypeConfiguration<egs_gameSessionPlayerCollection>
    {
        public egs_gameSessionPlayerCollectionMap()
        {
            // Primary Key
            this.HasKey(t => t.idGameSessionPlayerCollection);

            // Properties
            // Table & Column Mappings
            this.ToTable("egs_gameSessionPlayerCollection");
            this.Property(t => t.idGameSessionPlayerCollection).HasColumnName("idGameSessionPlayerCollection");
            this.Property(t => t.idGameSessionPlayer).HasColumnName("idGameSessionPlayer");
            this.Property(t => t.idGamePieceCollectionPlayer).HasColumnName("idGamePieceCollectionPlayer");

            // Relationships
            this.HasOptional(t => t.egs_gamePieceCollectionPlayer)
                .WithMany(t => t.egs_gameSessionPlayerCollection)
                .HasForeignKey(d => d.idGamePieceCollectionPlayer);
            this.HasOptional(t => t.egs_gameSessionPlayer)
                .WithMany(t => t.egs_gameSessionPlayerCollection)
                .HasForeignKey(d => d.idGameSessionPlayer);

        }
    }
}
