using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace zebulon.EFModel.Models.Mapping
{
    public class egs_gamePieceTypeMap : EntityTypeConfiguration<egs_gamePieceType>
    {
        public egs_gamePieceTypeMap()
        {
            // Primary Key
            this.HasKey(t => t.idGamePieceType);

            // Properties
            // Table & Column Mappings
            this.ToTable("egs_gamePieceType");
            this.Property(t => t.idGamePieceType).HasColumnName("idGamePieceType");
            this.Property(t => t.idGame).HasColumnName("idGame");

            // Relationships
            this.HasRequired(t => t.egs_game)
                .WithMany(t => t.egs_gamePieceType)
                .HasForeignKey(d => d.idGame);

        }
    }
}
