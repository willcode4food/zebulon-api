using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace zebulon.EFModel.Models.Mapping
{
    public class egs_gameMapTypeMap : EntityTypeConfiguration<egs_gameMapType>
    {
        public egs_gameMapTypeMap()
        {
            // Primary Key
            this.HasKey(t => t.idGameMapType);

            // Properties
            this.Property(t => t.mapType)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("egs_gameMapType");
            this.Property(t => t.idGameMapType).HasColumnName("idGameMapType");
            this.Property(t => t.mapType).HasColumnName("mapType");
        }
    }
}
