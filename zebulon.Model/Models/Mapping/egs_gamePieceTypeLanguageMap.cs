using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace zebulon.EFModel.Models.Mapping
{
    public class egs_gamePieceTypeLanguageMap : EntityTypeConfiguration<egs_gamePieceTypeLanguage>
    {
        public egs_gamePieceTypeLanguageMap()
        {
            // Primary Key
            this.HasKey(t => t.idGamePIeceTypeLanguage);

            // Properties
            this.Property(t => t.pieceType)
                .HasMaxLength(100);

            // Table & Column Mappings
            this.ToTable("egs_gamePieceTypeLanguage");
            this.Property(t => t.idGamePIeceTypeLanguage).HasColumnName("idGamePIeceTypeLanguage");
            this.Property(t => t.idGamePieceType).HasColumnName("idGamePieceType");
            this.Property(t => t.idLanguage).HasColumnName("idLanguage");
            this.Property(t => t.pieceType).HasColumnName("pieceType");

            // Relationships
            this.HasOptional(t => t.egs_gamePieceType)
                .WithMany(t => t.egs_gamePieceTypeLanguage)
                .HasForeignKey(d => d.idGamePieceType);
            this.HasOptional(t => t.egs_language)
                .WithMany(t => t.egs_gamePieceTypeLanguage)
                .HasForeignKey(d => d.idLanguage);

        }
    }
}
