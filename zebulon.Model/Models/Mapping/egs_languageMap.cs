using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace zebulon.EFModel.Models.Mapping
{
    public class egs_languageMap : EntityTypeConfiguration<egs_language>
    {
        public egs_languageMap()
        {
            // Primary Key
            this.HasKey(t => t.idLanguage);

            // Properties
            this.Property(t => t.language)
                .HasMaxLength(50);

            this.Property(t => t.culture)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("egs_language");
            this.Property(t => t.idLanguage).HasColumnName("idLanguage");
            this.Property(t => t.language).HasColumnName("language");
            this.Property(t => t.culture).HasColumnName("culture");
        }
    }
}
