using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace zebulon.EFModel.Models.Mapping
{
    public class egs_gamePieceAttrubuteTypeLanguageMap : EntityTypeConfiguration<egs_gamePieceAttrubuteTypeLanguage>
    {
        public egs_gamePieceAttrubuteTypeLanguageMap()
        {
            // Primary Key
            this.HasKey(t => t.idGamePieceAttributeLanguage);

            // Properties
            this.Property(t => t.name)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("egs_gamePieceAttrubuteTypeLanguage");
            this.Property(t => t.idGamePieceAttributeLanguage).HasColumnName("idGamePieceAttributeLanguage");
            this.Property(t => t.idGamePieceAttributeType).HasColumnName("idGamePieceAttributeType");
            this.Property(t => t.idLanguage).HasColumnName("idLanguage");
            this.Property(t => t.name).HasColumnName("name");

            // Relationships
            this.HasOptional(t => t.egs_gamePieceAttributeType)
                .WithMany(t => t.egs_gamePieceAttrubuteTypeLanguage)
                .HasForeignKey(d => d.idGamePieceAttributeType);
            this.HasOptional(t => t.egs_language)
                .WithMany(t => t.egs_gamePieceAttrubuteTypeLanguage)
                .HasForeignKey(d => d.idLanguage);

        }
    }
}
