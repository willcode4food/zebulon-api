using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace zebulon.EFModel.Models.Mapping
{
    public class egs_gamePieceLanguageMap : EntityTypeConfiguration<egs_gamePieceLanguage>
    {
        public egs_gamePieceLanguageMap()
        {
            // Primary Key
            this.HasKey(t => t.idGamePieceLanguage);

            // Properties
            this.Property(t => t.name)
                .HasMaxLength(200);

            // Table & Column Mappings
            this.ToTable("egs_gamePieceLanguage");
            this.Property(t => t.idGamePieceLanguage).HasColumnName("idGamePieceLanguage");
            this.Property(t => t.idLanguage).HasColumnName("idLanguage");
            this.Property(t => t.idGamePiece).HasColumnName("idGamePiece");
            this.Property(t => t.name).HasColumnName("name");
            this.Property(t => t.description).HasColumnName("description");

            // Relationships
            this.HasOptional(t => t.egs_gamePiece)
                .WithMany(t => t.egs_gamePieceLanguage)
                .HasForeignKey(d => d.idGamePiece);
            this.HasOptional(t => t.egs_language)
                .WithMany(t => t.egs_gamePieceLanguage)
                .HasForeignKey(d => d.idLanguage);

        }
    }
}
