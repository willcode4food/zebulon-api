using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace zebulon.EFModel.Models.Mapping
{
    public class egs_gamePieceSkinMap : EntityTypeConfiguration<egs_gamePieceSkin>
    {
        public egs_gamePieceSkinMap()
        {
            // Primary Key
            this.HasKey(t => t.idGamePieceSkin);

            // Properties
            this.Property(t => t.smallImg)
                .HasMaxLength(500);

            this.Property(t => t.bigImg)
                .HasMaxLength(500);

            this.Property(t => t.minImg)
                .HasMaxLength(500);

            this.Property(t => t.font1)
                .HasMaxLength(100);

            this.Property(t => t.font2)
                .HasMaxLength(100);

            // Table & Column Mappings
            this.ToTable("egs_gamePieceSkin");
            this.Property(t => t.idGamePieceSkin).HasColumnName("idGamePieceSkin");
            this.Property(t => t.idGamePiece).HasColumnName("idGamePiece");
            this.Property(t => t.smallImg).HasColumnName("smallImg");
            this.Property(t => t.bigImg).HasColumnName("bigImg");
            this.Property(t => t.minImg).HasColumnName("minImg");
            this.Property(t => t.font1).HasColumnName("font1");
            this.Property(t => t.font2).HasColumnName("font2");

            // Relationships
            this.HasOptional(t => t.egs_gamePiece)
                .WithMany(t => t.egs_gamePieceSkin)
                .HasForeignKey(d => d.idGamePiece);

        }
    }
}
