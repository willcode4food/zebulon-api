using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace zebulon.EFModel.Models.Mapping
{
    public class egs_gamePieceCollectionLanguageMap : EntityTypeConfiguration<egs_gamePieceCollectionLanguage>
    {
        public egs_gamePieceCollectionLanguageMap()
        {
            // Primary Key
            this.HasKey(t => t.idGamePieceCollectionLanguage);

            // Properties
            this.Property(t => t.collectionName)
                .HasMaxLength(100);

            // Table & Column Mappings
            this.ToTable("egs_gamePieceCollectionLanguage");
            this.Property(t => t.idGamePieceCollectionLanguage).HasColumnName("idGamePieceCollectionLanguage");
            this.Property(t => t.idGamePieceCollection).HasColumnName("idGamePieceCollection");
            this.Property(t => t.idLanguage).HasColumnName("idLanguage");
            this.Property(t => t.collectionName).HasColumnName("collectionName");

            // Relationships
            this.HasOptional(t => t.egs_gamePieceCollection)
                .WithMany(t => t.egs_gamePieceCollectionLanguage)
                .HasForeignKey(d => d.idGamePieceCollection);
            this.HasOptional(t => t.egs_language)
                .WithMany(t => t.egs_gamePieceCollectionLanguage)
                .HasForeignKey(d => d.idLanguage);

        }
    }
}
