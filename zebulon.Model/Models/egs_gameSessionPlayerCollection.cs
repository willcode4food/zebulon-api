using System;
using System.Collections.Generic;

namespace zebulon.EFModel.Models
{
    public partial class egs_gameSessionPlayerCollection
    {
        public long idGameSessionPlayerCollection { get; set; }
        public Nullable<long> idGameSessionPlayer { get; set; }
        public Nullable<long> idGamePieceCollectionPlayer { get; set; }
        public virtual egs_gamePieceCollectionPlayer egs_gamePieceCollectionPlayer { get; set; }
        public virtual egs_gameSessionPlayer egs_gameSessionPlayer { get; set; }
    }
}
