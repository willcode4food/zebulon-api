using System;
using System.Collections.Generic;

namespace zebulon.EFModel.Models
{
    public partial class egs_gamePieceCollectionPlayer
    {
        public egs_gamePieceCollectionPlayer()
        {
            this.egs_gamePieceCollectionPlayerPiece = new List<egs_gamePieceCollectionPlayerPiece>();
            this.egs_gameSessionPlayerCollection = new List<egs_gameSessionPlayerCollection>();
        }

        public long idGamePieceCollectionPlayer { get; set; }
        public long idGamePieceCollection { get; set; }
        public long idPlayer { get; set; }
        public Nullable<bool> isDefault { get; set; }
        public Nullable<System.DateTime> dateCreated { get; set; }
        public Nullable<System.DateTime> dateUpdated { get; set; }
        public virtual egs_gamePieceCollection egs_gamePieceCollection { get; set; }
        public virtual egs_player egs_player { get; set; }
        public virtual ICollection<egs_gamePieceCollectionPlayerPiece> egs_gamePieceCollectionPlayerPiece { get; set; }
        public virtual ICollection<egs_gameSessionPlayerCollection> egs_gameSessionPlayerCollection { get; set; }
    }
}
