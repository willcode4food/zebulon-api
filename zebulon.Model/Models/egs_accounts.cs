using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace zebulon.EFModel.Models
{
    public partial class egs_accounts
    {
        public egs_accounts()
        {
            this.egs_game = new List<egs_game>();
            this.egs_clients = new List<egs_clients>();
        }

        public long idAccount { get; set; }
        public string idAspNetRoles { get; set; }
        public string accountName { get; set; }
        [JsonIgnore]
        public virtual ICollection<egs_game> egs_game { get; set; }
        [JsonIgnore]
        public virtual ICollection<egs_clients> egs_clients { get; set; }
    }
}
