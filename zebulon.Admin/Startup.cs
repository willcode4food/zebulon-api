﻿using Microsoft.Owin;
using Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using Thinktecture.IdentityManager;

[assembly: OwinStartup(typeof(zebulon.Admin.Startup))]
namespace zebulon.Admin
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            HttpConfiguration config = new HttpConfiguration();       
            var factory = new AspNetIdentityIdentityManagerFactory("AuthContext");
            app.UseIdentityManager(new IdentityManagerConfiguration()
            {
                IdentityManagerFactory = factory.Create
            });
           
        }
    }
}