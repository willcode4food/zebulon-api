﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.Owin;
using Microsoft.Owin.Security.Facebook;
using Microsoft.Owin.Security.Google;
using Microsoft.Owin.Security.OAuth;
using Owin;
using System.Threading.Tasks;
using System.Security.Claims;

namespace zebulon.API.Providers
{
    /// <summary>
    /// Google Authentication Provideer implementation 
    /// Creates custom claim from Google
    /// </summary>
   public class GoogleAuthProvider : IGoogleOAuth2AuthenticationProvider
   {
       /// <summary>
       /// 
       /// </summary>
       /// <param name="context"></param>
       public void ApplyRedirect(GoogleOAuth2ApplyRedirectContext context)
       {
           context.Response.Redirect(context.RedirectUri);
       }
       /// <summary>
       /// 
       /// </summary>
       /// <param name="context"></param>
       /// <returns></returns>
       public Task Authenticated(GoogleOAuth2AuthenticatedContext context)
       {
           context.Identity.AddClaim(new Claim("ExternalAccessToken", context.AccessToken));
           return Task.FromResult<object>(null);
       }
       /// <summary>
       /// 
       /// </summary>
       /// <param name="context"></param>
       /// <returns></returns>
       public Task ReturnEndpoint(GoogleOAuth2ReturnEndpointContext context)
       {
           return Task.FromResult<object>(null);
       }
   }
}