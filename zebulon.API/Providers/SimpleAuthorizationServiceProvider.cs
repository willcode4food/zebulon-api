﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.OAuth;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Security.Principal;
using System.Threading.Tasks;
using System.Web;
using zebulon.EFModel.Models;
using zebulon.API.Util;

namespace zebulon.API.Providers
{
    /// <summary>
    /// Implements Owin Middleware authrization service provider
    /// Thank you to Taiseer Joudeh and his article series on Owin and Web API 2
    /// 
    /// Web: http://bitoftech.net/2014/07/16/enable-oauth-refresh-tokens-angularjs-app-using-asp-net-web-api-2-owin/
    /// Github: https://github.com/tjoudeh/AngularJSAuthentication
    /// </summary>
    public class SimpleAuthorizationServiceProvider : OAuthAuthorizationServerProvider
    {
        /// <summary>
        /// Validates the client that is connecting to the web service, assumes allowed request via CORS
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public override Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {
            string clientId = string.Empty;
            string clientSecret = string.Empty;
            egs_clients client = null;

            if(!context.TryGetBasicCredentials(out clientId, out clientSecret))
            {
                context.TryGetFormCredentials(out clientId, out clientSecret);
            }
            if (context.ClientId == null)
            {
                //Remove the comments from the below line context.SetError, and invalidate context 
                //if you want to force sending clientId/secrects once you obtain access tokens. 
                context.Validated();
                //context.SetError("invalid_clientId", "ClientId should be sent.");
                return Task.FromResult<object>(null);
            }
            using (AuthRepository _repo = new AuthRepository())
            {
                client = _repo.FindClientByPrimaryKey(context.ClientId);
            }
            if(client == null)
            {
                context.SetError("invalid_clientId", string.Format("Client '{0}' is not registered in the system.", context.ClientId));
                return Task.FromResult<object>(null);
            }
            
                       
            if(client.egs_applicationTypes.applicationType == Constants.APPLICATION_TYPE_PRIVATE)
            {
                if (string.IsNullOrWhiteSpace(clientSecret))
                {
                    context.SetError("invalid_clientId", "Client secret should be sent.");
                    return Task.FromResult<object>(null);
                }
                else
                {
                    if (client.secret != Hashing.GetHash(clientSecret))
                    {
                        context.SetError("invalid_clientId", "Client secret is invalid.");
                        return Task.FromResult<object>(null);
                    }
                }
            }

            if (client.isActive != true)
            {
                //invalidate the client as the are inactive
                context.SetError("invalid_clientId", "Client is inactive.");
                return Task.FromResult<object>(null);
            }

            context.OwinContext.Set<string>("as:clientAllowedOrigin", client.allowedOrigin);
            context.OwinContext.Set<string>("as:clientRefreshTokenLifeTime", client.refreshTokenLifetime.ToString());

            context.Validated();
            return Task.FromResult<object>(null);
            
        }
        /// <summary>
        /// Validating the username and password sent to authorization server's token endpoint
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public override async Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {
            
            var allowedOrigin = context.OwinContext.Get<string>("as:clientAllowedOrigin");
            string userId = null, rolesJson = null;
            List<IdentityUserClaim> roles = null;
            
            if (allowedOrigin == null) allowedOrigin = "*";            

            context.OwinContext.Response.Headers.Add("Access-Control-Allow-Origin", new[] { allowedOrigin });

            using (AuthRepository _repo = new AuthRepository())
            {
                IdentityUser user = await _repo.FindUser(context.UserName, context.Password);

                if (user == null)
                {
                    context.SetError("invalid_grant", "The user name or password is incorrect.");
                    return;
                }
                else{
                    //ClaimsIdentity oAuthIdentity = await _repo.FindClaimsIdentity(user, context.Options.AuthenticationType);
                    roles  = user.Claims.Where(c => c.ClaimType == "role").ToList();                    
                    
                    userId = user.Id;
                }
            }     
       
            var identity = new ClaimsIdentity(context.Options.AuthenticationType);
            identity.AddClaim(new Claim(ClaimTypes.Name, context.UserName));
            identity.AddClaim(new Claim("sub", context.UserName));

            if (userId != null)
            {
                identity.AddClaim(new Claim("userId", userId));

            }
            else
            {
                context.SetError("invalid_grant", "The user does not have a valid ID associated.");
            }
            if (roles != null)
            {
                if(roles.Count > 0)
                {
                    rolesJson = Newtonsoft.Json.JsonConvert.SerializeObject(roles.Select(x => x.ClaimValue));  
                    foreach(var r in roles)
                    {
                        identity.AddClaim(new Claim(ClaimTypes.Role, r.ClaimValue));
                    }
                    
                }
                
            }
            var props = new AuthenticationProperties(new Dictionary<string, string>
            {
              { 
                  "as:client_id", (context.ClientId == null) ? string.Empty : context.ClientId
              },
              { 
                  "userName", context.UserName
              },
              {
                  "roles", rolesJson
              },
              {
                  "userId", userId
              },
              {
                  "applicationType", Constants.APPLICATION_TYPE_PUBLIC
              }
            });

            
            
            var ticket = new AuthenticationTicket(identity, props);
            context.Validated(ticket);            
        }

        public override Task GrantClientCredentials(OAuthGrantClientCredentialsContext context)
        {
            egs_clients client = null;
            using (AuthRepository _repo = new AuthRepository())
            {
                client = _repo.FindClientByPrimaryKey(context.ClientId);
            }

            var props = new AuthenticationProperties(new Dictionary<string, string>
            {
                {
                    "as:client_id", (context.ClientId == null) ? string.Empty : context.ClientId
                },
                {
                    "applicationType", client.egs_applicationTypes.applicationType
                },
                {
                    "roles", "Service"
                }
            });
            var identity = new ClaimsIdentity(context.Options.AuthenticationType);
            identity.AddClaim(new Claim(ClaimTypes.Role, "Service"));
            var ticket = new AuthenticationTicket(identity, props);
            context.Validated(ticket);
            return base.GrantClientCredentials(context);

        }
        /// <summary>
        /// allows the issue of a new claim, updating existing, and  provide a new access token
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public override Task GrantRefreshToken(OAuthGrantRefreshTokenContext context)
        {
            var originalClient = context.Ticket.Properties.Dictionary["as:client_id"];
            var currentClient = context.ClientId;

            if (originalClient != currentClient)
            {
                context.SetError("invalid_clientId", "Refresh token is issued to a different clientId.");
                return Task.FromResult<object>(null);
            }

            // Change auth ticket for refresh token requests
            var newIdentity = new ClaimsIdentity(context.Ticket.Identity);
            newIdentity.AddClaim(new Claim("newClaim", "newValue"));

            var newTicket = new AuthenticationTicket(newIdentity, context.Ticket.Properties);
            context.Validated(newTicket);

            return Task.FromResult<object>(null);
        
        }

        public static AuthenticationProperties CreateProperties(string userName, string Roles)
        {
            IDictionary<string, string> data = new Dictionary<string, string>
            {             
                {"roles",Roles}
            };
            return new AuthenticationProperties(data);
        }
        /// <summary>
        /// Returns more detail including the Issued and Expire dates the username, and the clientId
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public override Task TokenEndpoint(OAuthTokenEndpointContext context)
        {
            foreach (KeyValuePair<string, string> property in context.Properties.Dictionary)
            {
                context.AdditionalResponseParameters.Add(property.Key, property.Value);
            }

            return Task.FromResult<object>(null);
        }
    }

}