﻿using zebulon.EFModel.Models;
using zebulon.API.Util;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Infrastructure;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;

namespace zebulon.API.Providers
{
    /// <summary>
    /// Implments Owin middleware for generating Refresh  Toeksn and storing them in the database
    /// </summary>
    public class SimpleRefreshTokenProvider : IAuthenticationTokenProvider
    {
        /// <summary>
        /// Implements custom logic for token generations.  Required by the interface IAuthticationTokenProvider.  
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public async Task CreateAsync(AuthenticationTokenCreateContext context)
        {
            var clientid = context.Ticket.Properties.Dictionary["as:client_id"];
            var applicationType = context.Ticket.Properties.Dictionary["applicationType"];

            if (string.IsNullOrEmpty(clientid))
            {
                return;
            }
            // generating new refresh token
            var refreshTokenId = Guid.NewGuid().ToString("n");
            string strIdAspNetUser = null;
            
            foreach (Claim claim in context.Ticket.Identity.Claims)
            {
                if (claim.Type == "userId")
                {
                    strIdAspNetUser = claim.Value;
                }
            }

            if (strIdAspNetUser == null && applicationType == Constants.APPLICATION_TYPE_PUBLIC)
            {
                throw new Exception("Identity User ID was not issued in this claim");

            }
            
            using (AuthRepository _repo = new AuthRepository())
            {
                var refreshTokenLifeTime = context.OwinContext.Get<string>("as:clientRefreshTokenLifeTime");
               
                var token = new egs_refreshTokens()
                {
                    token = Hashing.GetHash(refreshTokenId),
                    clientIdentifier = clientid,       
                    idAspNetUser = strIdAspNetUser,
                    issuedUTC = DateTime.UtcNow,
                    expiredUTC = DateTime.UtcNow.AddMinutes(Convert.ToDouble(refreshTokenLifeTime))
                };

                context.Ticket.Properties.IssuedUtc = token.issuedUTC;
                context.Ticket.Properties.ExpiresUtc = token.expiredUTC;

                token.protectedTicket = context.SerializeTicket();

                var result = await _repo.AddRefreshToken(token);

                if (result)
                {
                    context.SetToken(refreshTokenId);
                }

            }
        }
        /// <summary>
        /// receives the refresh token and generates new access token
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public async Task ReceiveAsync(AuthenticationTokenReceiveContext context)
        {

            var allowedOrigin = context.OwinContext.Get<string>("as:clientAllowedOrigin");
            context.OwinContext.Response.Headers.Add("Access-Control-Allow-Origin", new[] { allowedOrigin });

            string hashedTokenId = Hashing.GetHash(context.Token);

            using (AuthRepository _repo = new AuthRepository())
            {
                var refreshToken = await _repo.FindRefreshToken(hashedTokenId);

                if (refreshToken != null)
                {
                    //Get protectedTicket from refreshToken class
                    context.DeserializeTicket(refreshToken.protectedTicket);
                    var result = await _repo.RemoveRefreshToken(hashedTokenId);
                }
            }
        }

        public void Create(AuthenticationTokenCreateContext context)
        {
            throw new NotImplementedException();
        }

        public void Receive(AuthenticationTokenReceiveContext context)
        {
            throw new NotImplementedException();
        }
    }
}