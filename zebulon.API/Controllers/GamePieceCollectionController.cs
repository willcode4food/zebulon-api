﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using zebulon.API.Models;
using System.Threading.Tasks;

namespace zebulon.API.Controllers
{
    [System.Web.Http.RoutePrefix("api/Collection")]
    public class GamePieceCollectionController : ApiController
    {
        private AuthRepository _AuthRepo = null;
        private GamePieceCollectionRepository _GPCRepo = null;
  
        public GamePieceCollectionController()
        {
            _GPCRepo = new GamePieceCollectionRepository();
            _AuthRepo = new AuthRepository();

        }
        // Administrative maintenance routes for Collections
        /// <summary>
        /// 
        /// </summary>
        /// <param name="collection"></param>
        /// <returns></returns>     
        [HttpPost]
        [Route("admin")]
        public async Task<IHttpActionResult> PostCollection(GamePieceCollectionModel collection)
        {
            var newCollection = await _GPCRepo.addCollection(collection);
            return Ok("New Collection Completed");
        }

        //[HttpGet]
        //[Route("all/{idGame}")]
        //public async Task<IHttpActionResult> GetCollectionByGame(long idGame)
        //{
        //    var collections = await _GPCRepo.getCollectionByIdGame(idGame);
        //    return Ok(collections);
        //}
        [HttpGet]
        [Route("admin/{idGamePieceCollection}")]
        [Authorize(Roles = "Admin")]
        public async Task<IHttpActionResult> GetCollection(long idGamePieceCollection)
        {
            return Ok();
        }
        /// <summary>
        /// Returns a list of Collection Types for a specific game
        /// </summary>
        /// <param name="idGame"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("types/{idGame}")]
        [Authorize(Roles = "Admin")]
        public async Task<IHttpActionResult> GetCollectionTypes(long idGame)
        {
            var types = await this._GPCRepo.getCollectionTypes(idGame);
            return Ok(types);
        }
        [HttpPut]
        [Route("admin")]
        [Authorize(Roles = "Admin")]
        public async Task<IHttpActionResult> PutCollection()
        {
            return Ok();
        }
       
        // Player maintenance routes for Collections
        [HttpPost]
        [Route("player")]
        [Authorize(Roles = "Player")]
        public async Task<IHttpActionResult> PostPlayerCollection(GamePieceCollectionPlayerModel playerCollection)
        {
            var userId = _AuthRepo.GetUserIdByUsername(User.Identity.Name);
            var player = _AuthRepo.FindPlayer(userId);

            await _GPCRepo.addPlayerCollection(playerCollection, player.idPlayer);

            return Ok("Collection Added");
        }
        [HttpGet]
        [Route("player")]
        public async Task<IHttpActionResult> GetPlayerCollectionByPlayer()
        {
            return Ok();
        }
        [HttpPut]
        [Route("player")]
        public async Task<IHttpActionResult> PutPlayerCollection()
        {
            return Ok();
        }
    }
}
