﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using zebulon.API.Models;
using System.Threading.Tasks;

namespace zebulon.API.Controllers
{
    /// <summary>
    /// Controller for handling game piece operations
    /// </summary>
    [System.Web.Http.RoutePrefix("api/GamePieces")]
    public class GamePiecesController : ApiController
    {
        private GamePieceRepository _GPRepo = null;
        private AuthRepository _AuthRepo = null;

        public GamePiecesController()
        {
            _GPRepo = new GamePieceRepository();
            _AuthRepo = new AuthRepository();
        }
        /// <summary>
        /// Post end point for adding pieces
        /// available at /api/gamepieces/edit
        /// Available for users in the Admin role
        /// </summary>
        /// <param name="gamePiece"></param>
        /// <returns></returns>
        [Route("edit")]
        [HttpPost]
        [Authorize(Roles = "Admin")]
        public async Task<IHttpActionResult> AddGamePieces(List<GamePieceModel> gamePiece)
        {
            var userId = _AuthRepo.GetUserIdByUsername(User.Identity.Name);

            foreach (var g in gamePiece)
            {
                var newPiece = await _GPRepo.addGamePiece(g, userId);
            }

            return Ok("Game Piece(s) added.");
        }
        ///// <summary>
        ///// 
        ///// </summary>
        ///// <param name="gamePiece"></param>
        ///// <returns></returns>
        //[Route("edit")]
        //[HttpPost]
        //[Authorize(Roles = "Admin")]
        //public async Task<IHttpActionResult> AddGamePiece(GamePieceModel gamePiece)
        //{
        //    var newPiece = await _GPRepo.addGamePiece(gamePiece);
            
        //    return Ok("A New Game Piece Has Been Added");
        //}
        /// <summary>
        /// Edit Get end point.  Returns an object formatted for us in editing tools
        /// Available for users in the Admin role
        /// </summary>
        /// <returns></returns>
        [Route("edit")]
        [HttpGet]
        [Authorize(Roles ="Admin")]
        public IHttpActionResult GetGamePieceForEdit()
        {
            return Ok();
        }

        [Route("edit")]
        [HttpPut]
        [Authorize(Roles = "Admin")]
        public IHttpActionResult PutGamePieceForEdit()
        {
            return Ok();
        }
        /// <summary>
        /// Gets all game pieces associated with a game in plain english
        /// Defaults to english but supports other languages.  
        /// Available for users in the Admin role
        /// </summary>
        /// <param name="idGame">Game Id</param>
        /// <param name="lang">Culture for the language to return (i.e. en-US) </param>
        /// <returns></returns>
        [HttpGet]
        [Route("{idGame}/{culture}")]
        [Authorize(Roles = "Admin")]
        public async Task<IHttpActionResult> GetGamePieces(int idGame, string culture, string active)
        {
            var isActive = active == "true" ? "1" : "0";

            return Ok(await _GPRepo.getGamePiecesDisplayInfo(idGame, culture, isActive));
        }

    }

}
