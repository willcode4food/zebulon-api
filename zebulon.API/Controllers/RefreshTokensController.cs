﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using zebulon.API.Util;

namespace zebulon.API.Controllers
{
    /// <summary>
    /// Handles managment of Refresh Tokens. View, Delete currently supported
    /// </summary>
    [RoutePrefix("api/RefreshTokens")]
    public class RefreshTokensController : ApiController
    {
        private AuthRepository _repo = null;

        public RefreshTokensController()
        {
            _repo = new AuthRepository();
        }
        /// <summary>
        /// Lists all Refresh tokens that have been issued by the server
        ///  - Required Roles: Admin
        /// </summary>
        /// <returns></returns>

        [Authorize(Roles = "Admin")]
        [Route("")]        
        public IHttpActionResult Get()
        {
            return Ok(_repo.GetAllRefreshTokens());
        }

        /// <summary>
        /// Deletes a refresh token entry 
        ///  - Required Roles: Admin
        /// </summary>
        /// <param name="tokenId">Refresh Token Issued to the Client</param>
        /// <returns></returns>
        [Authorize]       
        [Route("")]
        public async Task<IHttpActionResult> Delete(string tokenId)
        {
            var hashedToken = Hashing.GetHash(tokenId);
            var result = await _repo.RemoveRefreshToken(hashedToken);
            if (result)
            {
                return Ok();
            }
            return BadRequest("Token Id does not exist");

        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _repo.Dispose();
            }

            base.Dispose(disposing);
        }

    }
}
