﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Runtime.Serialization;
using System.Threading.Tasks;
using System.Web.Http;
using Newtonsoft.Json;
using Microsoft.AspNet.Identity;
using zebulon.API.Models;

namespace zebulon.API.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    public class GameSessionsController : ApiController
    {
        private AuthRepository _AuthRepo = null;
        private GameSessionRepository _gameSessionRepo = null;

        /// <summary>
        /// 
        /// </summary>
        public GameSessionsController()
        {
            _AuthRepo = new AuthRepository();
            _gameSessionRepo = new GameSessionRepository();

        }
        /// <summary>
        /// Returns JSON list of players formatted for display purpose (summarized from the raw data)
        /// </summary>
        /// <param name="idGame">Game ID</param>
        /// <param name="active">Game ID</param>
        /// <returns></returns>
        [Authorize]
        [Route("api/game/{idGame}/gameSession")]             
        public async Task<IHttpActionResult> GetSessionDisplayList(int idGame, string active)
        {
            var userId = _AuthRepo.GetUserIdByUsername(User.Identity.Name);
            var isActive = active == "true" ? "1" : "0";

            return Ok(await _gameSessionRepo.getSessionDisplayInfo(userId, idGame, isActive));

        }
        [Route("api/initGameSession/{idGameSession}/{culture}")]
        public async Task<IHttpActionResult> GetSessionById(long idGameSession, string culture)
        {
            return Ok(await _gameSessionRepo.getGameStateBySessionsId(idGameSession, culture));
        }
        /// <summary>
        /// Creates a new session for a givin game
        /// </summary>
        /// <param name="sessionInfo">GameSessionCreatModel includes an array of player IDs and a game ID</param>
        /// <returns></returns>
       [Authorize]
       [HttpPost]
       [Route("api/gameSession")]
        public async Task<IHttpActionResult> PostGameSession(GameSessionCreateModel sessionInfo)
        {
            var userId = _AuthRepo.GetUserIdByUsername(User.Identity.Name);
            var newSession = await _gameSessionRepo.addSession(sessionInfo.idGame, sessionInfo.playerIds, userId, sessionInfo.collectionIds);

            return Ok(newSession); 
        }
        /// <summary>
        /// Deletes a Game Session
        /// </summary>
        /// <param name="gameSessionId"></param>
        /// <returns></returns>
        [Authorize]
        [Route("api/gameSession")]
       public async Task<IHttpActionResult> Delete(string gameSessionId)
       {
         
              var result = await _gameSessionRepo.deleteGameSession(gameSessionId);
              if (result)
              {
                  return Ok("Session has been removed");
              }
              return BadRequest("Session does not exist");

       }

    }
}
