﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Linq;
using System.Web;

namespace zebulon.API.Util
{
    /// <summary>
    /// Class contains hashing tools for encoding UT8
    /// with SHA256 hashing algorith,
    /// </summary>
    public static class Hashing
    {
        /// <summary>
        /// converts strings to hashed values
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static string GetHash(string input)
        {
            HashAlgorithm hashAlgorithm = new SHA256CryptoServiceProvider();

            byte[] byteValue = System.Text.Encoding.UTF8.GetBytes(input);

            byte[] byteHash = hashAlgorithm.ComputeHash(byteValue);

            return Convert.ToBase64String(byteHash);
        }
    }
}