﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace zebulon.API.Models
{
    public class GamePieceCollectionPlayerModel
    {
        public long idGamePieceCollection { get; set; }
        public long[] gamePieceIds { get; set; }
    }
}