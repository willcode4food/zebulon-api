﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace zebulon.API.Models
{
    public class GamePieceCollectionModel
    {
        public int idGame;
        public int idGamePieceCollectionType;
        public int size;
        public GamePieceCollectionLanguageModel collectionLanguage;
    }
    public class GamePieceCollectionLanguageModel
    {
        public int idLanguage;
        public string name;
    }
}