﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace zebulon.API.Models
{
    public class GamePieceModel
    {
        public int idGamePiece;
        public int idGame;
        public int idGamePieceType;
        public bool isActive;
        public DateTime createDate;
        public List<GamePieceAttributeModel> gamePieceAttributes;
        public GamePieceLanguageModel gamePieceLanguage;
        public GamePieceSkinModel gamePieceSkin;
    }

    public class GamePieceTypeModel
    {
        public int idGamePieceType;
        public int idGame;
        public string pieceType;

    }

    public class GamePieceAttributeModel
    {
        public int idGamePieceAttribute;        
        public int idGamePieceAttributeType;
        public string value;
    }
    public class GamePieceAttributeLanguageModel
    {
        public int idGamePieceAttributeLanguage;
        public int idGamePieceAttribute;
        public int idLanguage;
        public string name;
        public string value;
    }
    public class GamePieceAttributeTypeModel
    {
        public int idGamePieceAttributeType;
        public int idGame;
        public string pieceAttributeType;
    }

    public class GamePieceLanguageModel
    {
        public int idGamePieceLanguage;
        public int idLanguage;        
        public string name;
        public string description;
    }
    public class GamePieceSkinModel
    {
        public int idGamePieceSkin;        
        public string smallImg;
        public string bigImg;
        public string minImg;
        public string font1;
        public string font2;
    }
}