﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace zebulon.API.Models.GameState
{
    public class GameStateModel
    { 
       public long idGameSession { get; set; }
       public long idGame { get; set; }
       public DateTime startTime { get; set; }
       public DateTime startNextTurn { get; set; }
       public DateTime endTurnTime { get; set; }
       public List<Player> players { get; set; }
       public List<Counter> counters { get; set; }
    }
    public class Player
    {
       public long idPlayer { get; set; }
       public string name { get; set; }
       public List<PlayerCollection> playerCollections { get; set; }

    }
    public class PlayerCollection
    {
       public string name { get; set; }
       public string type { get; set; }
       public int size { get; set; }
       public List<dynamic> gamePieces { get; set; }
    }
    public class Counter
    {
       public string name { get; set; }
       public long start { get; set; }
       public long end { get; set; }
    }
}