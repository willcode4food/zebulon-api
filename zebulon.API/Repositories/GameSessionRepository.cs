﻿using System;
using System.Dynamic;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Data.Entity.Core;
using System.Reflection;
using System.Web;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity;
using zebulon.EFModel.Models;
using zebulon.API.Models.GameState;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace zebulon.API
{
    /// <summary>
    /// Repository Class for operations pertaining to Game Sessions
    /// </summary>
    public class GameSessionRepository : IDisposable
    {
        private zebulon.EFModel.Models.AuthContext _EFModelctx;

        public GameSessionRepository()
        {
            _EFModelctx = new zebulon.EFModel.Models.AuthContext();
        }
        /// <summary>
        /// Returns a list of sessions from the database relative to the user and game that is 
        /// requesting 
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="gameId"></param>
        /// <param name="isActive"></param>
        /// <returns>List<dynamic> of Game Sessions</dynamic></returns>
        public async Task<List<dynamic>> getSessionDisplayInfo(string userId, int gameId, string isActive)
        {
            // getting data base from a store proc that formats the data in a flattened and simple 
            // format for showing the end user a list of sessions and allow them to pick one to resume
            var dbObjs = new sp_egs_GetGameSessions();
            //Using and expano object, because the players may be 1 or more.  This will
            // translate to an object with several player properties we cannot predict
            List<dynamic> objList = await dbObjs.Execute(userId, gameId, isActive);  
            
            return objList;
        }
        /// <summary>
        /// Creates a new session
        /// </summary>
        /// <param name="gameId"> ID of the game for the session we are creating</param>
        /// <param name="playerIds">A integer array of player IDs</param>
        /// <param name="userId">User ID of the user creating the session</param>
        /// <param name="collectionId">A list of collection ids to add to each player for the session</param>
        public async Task<bool> addSession(int gameId,  long[] playerIds, string userId, long[] collectionId)
        {
            var isCreator = false;

            try
            {
                // Create new session
                egs_gameSession sessionEntity = new egs_gameSession
                {
                    idGame = gameId,
                    isActive = true,
                    startTime = DateTime.Now
                };
                this._EFModelctx.egs_gameSession.Add(sessionEntity);
                await this._EFModelctx.SaveChangesAsync();
                // add players
              for(int i = 0; i < playerIds.Length; i++)
              {
                  if (playerIds[i] == this.getPlayerIdfromUserId(userId))
                  {
                      isCreator = true;
                  }
                  else
                  {
                      isCreator = false;
                  }

                    egs_gameSessionPlayer sessionPlayer = new egs_gameSessionPlayer
                    {
                        idGameSession = sessionEntity.idGameSession,
                        idPlayer = playerIds[i],
                        isCreator = isCreator,
                        hasAccepted = true
                   };
                    if (isCreator)
                    {
                        for (int j = 0; j < collectionId.Length; j++)
                        {
                            egs_gameSessionPlayerCollection sessionPlayerColl = new egs_gameSessionPlayerCollection
                            {
                                idGamePieceCollectionPlayer = collectionId[j]

                            };
                            sessionPlayer.egs_gameSessionPlayerCollection.Add(sessionPlayerColl);
                        }
                    }
                   this._EFModelctx.egs_gameSessionPlayer.Add(sessionPlayer);
                  
              }
                await this._EFModelctx.SaveChangesAsync();
                return true;
            }
            catch(Exception e)
            {
                throw new Exception(e.Message);
                //return false;
            }
            
        }
        private long getPlayerIdfromUserId(string userId)
        {
            var userPlayer = 
                    from players in this._EFModelctx.egs_player
                    where players.idUser == userId

                    select new 
                    {
                        idPlayer = players.idPlayer
                    };
            
            return userPlayer.FirstOrDefault().idPlayer;
        }
        public async Task<bool> deleteGameSession(string gameSessionId)
        {
            long idGameSession = Convert.ToInt64(gameSessionId);
            var gameSession = await this._EFModelctx.egs_gameSession.Where(g => g.idGameSession == idGameSession).ToListAsync();
            if (gameSession.Count > 0)
            {
                this._EFModelctx.egs_gameSession.Remove(gameSession.First());

                return await this._EFModelctx.SaveChangesAsync() > 0;
            }
            return false;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="gameSessionId"></param>
        /// <returns></returns>
        public async Task<JObject> getGameStateBySessionsId(long idGameSession, string culture)
        {
            try
            {
                var gameState = await _EFModelctx.egs_gameSession.FindAsync(idGameSession);
                var lang = await _EFModelctx.egs_language.Where(s => s.culture == culture).SingleAsync();


                GameStateModel gsm = new GameStateModel()
                {
                    idGameSession = gameState.idGameSession,
                    idGame = (long)gameState.idGame,
                    startTime = (DateTime)gameState.startTime
                };
                gsm.players = new List<Player>();
                gsm.counters = new List<Counter>();


                var playersArray = new List<Player>();

                foreach (var p in gameState.egs_gameSessionPlayer)
                {
                    Player player = new Player()
                    {
                        idPlayer = p.idPlayer,
                        name = p.egs_player.nickName
                    };
                    player.playerCollections = new List<PlayerCollection>();

                    foreach (var pc in p.egs_gameSessionPlayerCollection)
                    {

                        PlayerCollection playerColl = new PlayerCollection()
                        {
                            name = pc.egs_gamePieceCollectionPlayer.egs_gamePieceCollection.egs_gamePieceCollectionLanguage.Where(s => s.idLanguage == lang.idLanguage).FirstOrDefault().collectionName,
                            size = (int)pc.egs_gamePieceCollectionPlayer.egs_gamePieceCollection.size,
                            type = pc.egs_gamePieceCollectionPlayer.egs_gamePieceCollection.egs_gamePieceCollectionType.collectionType

                        };
                     
                            var dbObjs = new sp_egs_GetSessionPlayerCollectionGamePieces();
                            //Using and expando object, because the players may be 1 or more.  This will
                            // translate to an object with several player properties we cannot predict
                            List<dynamic> gamePieces = await dbObjs.Execute((long)pc.idGamePieceCollectionPlayer, lang.idLanguage, "true");

                            playerColl.gamePieces = gamePieces;
                     
                        player.playerCollections.Add(playerColl);
                  
                        
                    }
                    gsm.players.Add(player);
                }
                var gameColls = gameState.egs_game.egs_gamePieceCollection.Where(g => g.idGamePieceCollectionType == 4);
                
                foreach (var c in gameState.egs_game.egs_counter)
                {
                    Counter counter = new Counter()
                    {
                        name = c.egs_counterLanguage.Where(cc => cc.idLanuage == lang.idLanguage).FirstOrDefault().counterName,
                        start = (long)c.counterStart,
                        end = (long)c.counterEnd
                    };
                    gsm.counters.Add(counter);
                }
                var jSession = JObject.FromObject(gsm);
                var jGameMaps = this.getGameMaps(gsm.idGame);

                jSession.Merge(jGameMaps, new JsonMergeSettings
                {
                    MergeArrayHandling = MergeArrayHandling.Union
                });

                return jSession;
            }
            catch(Exception e)
            {
                throw new Exception(e.Message);
            }
            
        }

        /// <summary>
        /// Returns a JSON formated object of the Game Map structure (currently only supports grid based maps)
        /// </summary>
        /// <param name="idGame"></param>
        /// <returns></returns>
        public JObject getGameMaps(long idGame)
        {
            try
            {
                _EFModelctx.Configuration.LazyLoadingEnabled = false;
                // get the meta data describing the game map
                var gameMaps = (from g in _EFModelctx.egs_gameMap
                                where g.idGame == idGame
                                select g).Include("egs_gameMapType");

                var jGameMaps = new JObject();
                dynamic jmaps = jGameMaps;
                jmaps.gameMaps = new JArray() as dynamic;

                foreach (var map in gameMaps)
                {
                    if (map.egs_gameMapType.mapType == Constants.GAME_MAP_TYPE_GRID)
                    {
                        // support grid based maps only for now
                        var jmap = new JObject();
                        // create JSON object to represent the grid
                        for (var i = 0; i < map.numRows; i++)
                        {
                            var jmapRow = new JObject();
                            for (var j = 0; j < map.numCols; j++)
                            {
                                jmapRow.Add(j.ToString(), "");
                            }
                            jmap.Add(i.ToString(), jmapRow);
                        }
                        jmaps.gameMaps.Add(jmap);
                    }
                }
                return jmaps;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void Dispose()
        {
            _EFModelctx.Dispose();

        }
    }
}