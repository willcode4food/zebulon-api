﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using zebulon.EFModel.Models;
using zebulon.API.Models;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity;

namespace zebulon.API
{
    public class GamePieceRepository : IDisposable
    {
        private zebulon.EFModel.Models.AuthContext _EFModelctx;
       

        public GamePieceRepository()
        {
            _EFModelctx = new zebulon.EFModel.Models.AuthContext();
        }

        public async Task<egs_gamePiece> addGamePiece(GamePieceModel gamePiece, string userId)
        {
          
            try
            {
                egs_gamePiece piece = new egs_gamePiece() {idGame = gamePiece.idGame, idGamePieceType = gamePiece.idGamePieceType, createdDate = DateTime.Now, updatedDate = DateTime.Now, isActive = gamePiece.isActive, createdBy = userId };

                foreach (var pieceAttr in gamePiece.gamePieceAttributes)
                {
                    egs_gamePieceAttribute attr = new egs_gamePieceAttribute() { idGamePieceAttributeType = pieceAttr.idGamePieceAttributeType, value = pieceAttr.value };
                    piece.egs_gamePieceAttribute.Add(attr);
                }

                egs_gamePieceLanguage lang = new egs_gamePieceLanguage() { name = gamePiece.gamePieceLanguage.name, description = gamePiece.gamePieceLanguage.description, idLanguage = gamePiece.gamePieceLanguage.idLanguage };
                piece.egs_gamePieceLanguage.Add(lang);

                egs_gamePieceSkin skin = new egs_gamePieceSkin() { smallImg = gamePiece.gamePieceSkin.smallImg, bigImg = gamePiece.gamePieceSkin.bigImg, minImg = gamePiece.gamePieceSkin.minImg, font1 = gamePiece.gamePieceSkin.font1, font2 = gamePiece.gamePieceSkin.font2 };
                piece.egs_gamePieceSkin.Add(skin);
                this._EFModelctx.Configuration.LazyLoadingEnabled = false;
                this._EFModelctx.egs_gamePiece.Add(piece);
                await this._EFModelctx.SaveChangesAsync();
                this._EFModelctx.Entry(piece).Reference(s => s.egs_gamePieceType).Load();
                this._EFModelctx.Entry(piece).Collection(s => s.egs_gamePieceLanguage).Query().Include(l => l.egs_language).Load();
                this._EFModelctx.Entry(piece).Collection(s => s.egs_gamePieceAttribute).Query().Include(l => l.egs_gamePieceAttributeType).Load();
                this._EFModelctx.Entry(piece).Collection(s => s.egs_gamePieceAttribute).Query().Include(l => l.egs_gamePieceAttributeType.egs_gamePieceAttrubuteTypeLanguage).Load();
                
                return piece;
                
            }
            catch(Exception e)
            { 
                throw new Exception(e.InnerException.ToString());
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="gameId"></param>
        /// <param name="culture"></param>
        /// <param name="isActive"></param>
        /// <returns></returns>
        public async Task<List<dynamic>> getGamePiecesDisplayInfo(int gameId, string culture, string isActive)
        {
            // getting data base from a store proc that formats the data in a flattened and simple 
            // format for showing the end user a list of sessions and allow them to pick one to resume
            var dbObjs = new sp_egs_GetGamePieces();
            //Using and expano object, because the players may be 1 or more.  This will
            // translate to an object with several player properties we cannot predict
            List<dynamic> objList = await dbObjs.Execute(gameId, culture, isActive);

            return objList;
        }
        public void Dispose()
        {
            _EFModelctx.Dispose();

        }

    }
}