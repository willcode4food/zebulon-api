﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity;
using zebulon.EFModel.Models;

namespace zebulon.API
{
    /// <summary>
    /// Repository Class to support ASP.NET Identity Sysetem
    /// </summary>
    public class AuthRepository : IDisposable
    {
        private AuthContext _ctx;
        private zebulon.EFModel.Models.AuthContext _EFModelctx;

        private UserManager<IdentityUser> _userManager;

        /// <summary>
        /// Initializes the DBContext and UserManager
        /// </summary>
        public AuthRepository()
        {
            _ctx = new AuthContext();
            _EFModelctx = new zebulon.EFModel.Models.AuthContext();
            _userManager = new UserManager<IdentityUser>(new UserStore<IdentityUser>(_ctx));
        }
        /// <summary>
        /// Faciliates Registering the User with ASP.NET Identity
        /// </summary>
        /// <param name="userModel"></param>
        /// <returns>Identity Result Object</returns>

        public async Task<IdentityResult> RegisterUser(Models.UserModel userModel)
        {
            IdentityUser user = new IdentityUser
            {
                UserName = userModel.UserName
            };
            var result = await _userManager.CreateAsync(user, userModel.Password);
            return result;
        }
        public async Task<IList<string>> GetRoles(string userId)
        {
            return await _userManager.GetRolesAsync(userId);
        }

        /// <summary>
        /// Facilitates Finding the User with ASP.NET Identity
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="password"></param>
        /// <returns>Identity User Object</returns>
        public async Task<IdentityUser> FindUser(string userName, string password)
        {
            var user = await _userManager.FindAsync(userName, password);

            return user;
        }
        public egs_player FindPlayer(string userId)
        {
            var player = _EFModelctx.egs_player.Where(p => p.idUser == userId).SingleOrDefault();
            _EFModelctx.Entry(player).Reference(s => s.egs_region).Load();
            return player;

        }
     /// <summary>
     /// 
     /// </summary>
     /// <param name="userName"></param>
     /// <returns></returns>
        public string GetUserIdByUsername(string userName)
        {
            var user = _userManager.FindByName(userName);
            return user.Id;
        }
        /// <summary>
        ///  Facilitates finding the client via Entity Framework
        /// </summary>
        /// <param name="clientId"></param>
        /// <returns></returns>
        public egs_clients FindClientByPrimaryKey(string clientId)
        {
            try
            {
                var intClientId = Convert.ToInt64(clientId);


                _EFModelctx.Configuration.LazyLoadingEnabled = false;
                var client = _EFModelctx.egs_clients.Find(intClientId);

                // Use explicit loading to include the child object containing the application type         
                // lazy loading is being lazy and does not include my related objects 
                // POCO Implementation below
                _EFModelctx.Entry(client).Reference(s => s.egs_applicationTypes).Load();
        
                return client;
            }
            catch(Exception e)
            {
                throw new Exception(e.Message);
            }
          
        }

        public egs_clients FindClientByIdentifier(string clientIdentifier, string clientOrigin)
        {
            _EFModelctx.Configuration.LazyLoadingEnabled = false;
            var client = _EFModelctx.egs_clients.Where(r => r.clientIdentifier == clientIdentifier && r.allowedOrigin == clientOrigin).SingleOrDefault();
            // Use explicit loading to include the child object containing the application type         
            // lazy loading is being lazy and does not include my related objects 
            // POCO Implementation below
            _EFModelctx.Entry(client).Reference(s => s.egs_applicationTypes).Load();
            return client;

        }
        public async Task<bool> AddRefreshToken(egs_refreshTokens token)
        {
            try
            {
                var existingToken = _EFModelctx.egs_refreshTokens.Where(r =>
                r.idAspNetUser == token.idAspNetUser && r.clientIdentifier == token.clientIdentifier).SingleOrDefault();

                if (existingToken != null)
                {
                    var result = await RemoveRefreshToken(existingToken);
                }
                _EFModelctx.egs_refreshTokens.Add(token);

                return await _EFModelctx.SaveChangesAsync() > 0;
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
            
        }

        public async Task<bool> RemoveRefreshToken(string refreshTokenId)
        {
                       
            var refreshToken = await _EFModelctx.egs_refreshTokens.Where(b => b.token == refreshTokenId).ToListAsync();
            if (refreshToken.Count > 0)   
            {
                _EFModelctx.egs_refreshTokens.Remove(refreshToken.First());
                return await _EFModelctx.SaveChangesAsync() > 0;
            }
            return false;
        }

        public async Task<bool> RemoveRefreshToken(egs_refreshTokens refreshToken)
        {
            _EFModelctx.egs_refreshTokens.Remove(refreshToken);
            return await _EFModelctx.SaveChangesAsync() > 0;
        }

        public async Task<egs_refreshTokens> FindRefreshToken(string refreshTokenId)
        {
            var refreshToken = await _EFModelctx.egs_refreshTokens.Where(b => b.token == refreshTokenId).ToListAsync();
            if (refreshToken.Count == 0)
            {
                return null;
            }

            return refreshToken.First();
        }

        public List<egs_refreshTokens> GetAllRefreshTokens()
        {
            return _EFModelctx.egs_refreshTokens.ToList();
        }
        /// <summary>
        /// Method for support for external logins
        /// </summary>
        /// <param name="loginInfo"></param>
        /// <returns></returns>
        public async Task<IdentityUser> FindAsync(UserLoginInfo loginInfo)
        {
            IdentityUser user = await _userManager.FindAsync(loginInfo);

            return user;
        }
        /// <summary>
        /// Method for support for external logins
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public async Task<IdentityResult> CreateAsync(IdentityUser user)
        {
            var result = await _userManager.CreateAsync(user);
            var claimResult = await _userManager.AddClaimAsync(user.Id, new Claim("role", "Player"));

            return result;
        }
        /// <summary>
        /// Method for support for external logins
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="login"></param>
        /// <returns></returns>
        public async Task<IdentityResult> AddLoginAsync(string userId, UserLoginInfo login)
        {
            var result = await _userManager.AddLoginAsync(userId, login);

            return result;
        }

        /// <summary>
        /// Disposes the DB Context and Usermanager
        /// Must be overriden to inherit from IDisposable
        /// </summary>
        public void Dispose()
        {
            _ctx.Dispose();
            _EFModelctx.Dispose();
            _userManager.Dispose();

        }
        private bool isLong(string strTest)
        {
            long l;
            return long.TryParse(strTest, out l);
        }

    }
}