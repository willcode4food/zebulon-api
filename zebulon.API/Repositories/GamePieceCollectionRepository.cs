﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using zebulon.EFModel.Models;
using zebulon.API.Models;
using System.Threading.Tasks;

namespace zebulon.API
{
    /// <summary>
    /// 
    /// </summary>
    public class GamePieceCollectionRepository : IDisposable
    {
        private zebulon.EFModel.Models.AuthContext _EFModelctx;
        /// <summary>
        /// 
        /// </summary>
        public GamePieceCollectionRepository()
        {
            _EFModelctx = new zebulon.EFModel.Models.AuthContext();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="gamePieceCollection"></param>
        /// <returns></returns>
        public async Task<egs_gamePieceCollection> addCollection(GamePieceCollectionModel gamePieceCollection)
        {
            try
            {
                egs_gamePieceCollection collection = new egs_gamePieceCollection() { idGame = gamePieceCollection.idGame, idGamePieceCollectionType = gamePieceCollection.idGamePieceCollectionType, size = gamePieceCollection.size };
                egs_gamePieceCollectionLanguage lang = new egs_gamePieceCollectionLanguage() { idLanguage = gamePieceCollection.collectionLanguage.idLanguage, collectionName = gamePieceCollection.collectionLanguage.name };

                collection.egs_gamePieceCollectionLanguage.Add(lang);
                _EFModelctx.egs_gamePieceCollection.Add(collection);
                await _EFModelctx.SaveChangesAsync();
                return collection;

            }
            catch (Exception e)
            {
                throw new Exception(e.InnerException.ToString());
            }
        }
        public async Task<egs_gamePieceCollectionPlayer> addPlayerCollection(GamePieceCollectionPlayerModel playerCollection, long idPlayer)
        {
            try
            {
                egs_gamePieceCollectionPlayer coll = new egs_gamePieceCollectionPlayer()
                {
                    idGamePieceCollection = playerCollection.idGamePieceCollection,
                    idPlayer = idPlayer,
                    isDefault = true,
                    dateCreated = DateTime.Now,
                    dateUpdated = DateTime.Now
                };
                
                foreach (long g in playerCollection.gamePieceIds)
                {
                    egs_gamePieceCollectionPlayerPiece collPiece = new egs_gamePieceCollectionPlayerPiece()
                    {
                        idGamePIeceCollectionPlayer = coll.idGamePieceCollectionPlayer,
                        idGamePiece = g
                    };
                    coll.egs_gamePieceCollectionPlayerPiece.Add(collPiece);
                }
                _EFModelctx.egs_gamePieceCollectionPlayer.Add(coll);
                await _EFModelctx.SaveChangesAsync();
                return coll;

            }
            catch (Exception e)
            {
                throw new Exception(e.InnerException.ToString());
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="idGame"></param>
        /// <returns></returns>
                        
        public async Task<List<egs_gamePieceCollection>> getCollectionByIdGame(long idGame)
        {
            try
            {
                var collections =
                      from collection in this._EFModelctx.egs_gamePieceCollection
                      where collection.idGame == idGame
                      select collection;

                return await collections.ToListAsync();


            }
            catch (Exception e)
            {
                throw new Exception(e.InnerException.ToString());
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="idGame"></param>
        /// <returns></returns>
        public async Task<List<egs_gamePieceCollectionType>> getCollectionTypes(long idGame)
        {
            var collectionTypes =
                 from types in this._EFModelctx.egs_gamePieceCollectionType
                 where types.idGame == idGame
                 select types;
         
            return await collectionTypes.ToListAsync();
        }
        /// <summary>
        /// 
        /// </summary>
        public void Dispose()
        {
            _EFModelctx.Dispose();

        }

    }
}