﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace zebulon.API
{
    public class Constants
    {
        public const string APPLICATION_TYPE_PRIVATE  = "Private";
        public const string APPLICATION_TYPE_PUBLIC  = "Public";
        public const string GAME_MAP_TYPE_GRID = "grid";
        public const string GAME_MAP_TYPE_HEX = "hex";
        public const string GAME_MAP_TYPE_IRREGULAR = "irregular";
    }
}