using System.Web.Http;
using zebulon.API;
using WebActivatorEx;
using Swashbuckle.Application;


[assembly: PreApplicationStartMethod(typeof(SwaggerConfig), "Register")]

namespace zebulon.API
{
    public class SwaggerConfig
    {
        public static void Register()
        {
            HttpConfiguration config = new HttpConfiguration();
                    config.EnableSwagger(c => c.SingleApiVersion("v1", "Zebulon API")).EnableSwaggerUi(); 

            // NOTE: If you want to customize the generated swagger or UI, use SwaggerSpecConfig and/or SwaggerUiConfig here ...
        }
    }
}