﻿using Microsoft.Owin;
using Microsoft.Owin.Security.OAuth;
using Microsoft.Owin.Security.Google;
using Microsoft.Owin.Security.Facebook;
using Newtonsoft.Json;
using Owin;
using zebulon.API.Providers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using Swashbuckle.Application;

[assembly: OwinStartup(typeof(zebulon.API.Startup))]
namespace zebulon.API
{
    /// <summary>
    /// Implementation of OWIN in Web API 2.  This system could not have been achieved without 
    /// the explicit help from Taiseer Joudeh. You can find his informative article series 
    /// 
    /// http://bitoftech.net/2014/06/01/token-based-authentication-asp-net-web-api-2-owin-asp-net-identity/
    ///
    /// </summary>
    public class Startup
    {
        public static OAuthBearerAuthenticationOptions OAuthBearerOptions { get; private set; }
        public static GoogleOAuth2AuthenticationOptions googleAuthOptions { get; private set; }
        public static FacebookAuthenticationOptions facebookAuthOptions { get; private set; }

        public void Configuration(IAppBuilder app)
        {
            HttpConfiguration config = new HttpConfiguration();
            ConfigureOAuth(app);
            WebApiConfig.Register(config);
          
            config.EnableSwagger(c => c.SingleApiVersion("v1", "Zebulon API")).EnableSwaggerUi(); 

            app.UseCors(Microsoft.Owin.Cors.CorsOptions.AllowAll);
            app.UseWebApi(config);
        }
        /// <summary>
        /// Creates the Server Options for OAuth.  Generates  the Bearer Tokens
        /// </summary>
        /// <param name="app"></param>
        public void ConfigureOAuth(IAppBuilder app)
        {
            //use a cookie to temporarily store information about a user logging in with a third party login provider
            app.UseExternalSignInCookie(Microsoft.AspNet.Identity.DefaultAuthenticationTypes.ExternalCookie);
            OAuthBearerOptions = new OAuthBearerAuthenticationOptions();
            OAuthAuthorizationServerOptions OAuthServerOptions = new OAuthAuthorizationServerOptions()
            {
                AllowInsecureHttp = true, //HTTPS not required -- But this needs to change in production!
                TokenEndpointPath = new PathString("/token"), // path that is used to issue the token http://<server>:<port>/token 
                //AuthorizeEndpointPath = new PathString("/api/Account/ExternalLogin"), // path that is used for authorization
                AccessTokenExpireTimeSpan = TimeSpan.FromHours(24), // epxires 24 hours
                Provider = new SimpleAuthorizationServiceProvider(), // custom authorizion provider 
                RefreshTokenProvider = new SimpleRefreshTokenProvider() //  custom refresh token provider

            };

            app.UseOAuthAuthorizationServer(OAuthServerOptions);
            app.UseOAuthBearerAuthentication(OAuthBearerOptions);

            //Configure Google External Login
            googleAuthOptions = new GoogleOAuth2AuthenticationOptions()
            {
                ClientId = "248031000872-1cldvvjvlid89netsqejssdkeiisp2ds.apps.googleusercontent.com",
                ClientSecret = "7f3-0eD8pQltzWvlBIT4TNDX",
                Provider = new GoogleAuthProvider()
            };
            app.UseGoogleAuthentication(googleAuthOptions);

            //Configure Facebook External Login
            facebookAuthOptions = new FacebookAuthenticationOptions()
            {
                AppId = "690712067681096",
                AppSecret = "a3ada4b01da51ef555795c6be0871b6d",
                Provider = new FacebookAuthProvider()
            };
            facebookAuthOptions.Scope.Add("email");
            app.UseFacebookAuthentication(facebookAuthOptions);

        }
    }
}