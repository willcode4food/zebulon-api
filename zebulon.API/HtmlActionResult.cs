﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;

namespace zebulon.API.Http
{
    /// <summary>
    /// 
    /// </summary>
    public class HtmlActionResult:IHttpActionResult
    {
        private const string ViewDirectory = @"E:devConsoleApplication8ConsoleApplication8";
        private readonly string _viewName;
        private readonly string _controllerName;
        private readonly dynamic _model;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="viewName"></param>
        /// <param name="controllerName"></param>
        /// <param name="model"></param>
        public HtmlActionResult(string viewName, dynamic model, string controllerName)
        {
            _viewName = viewName;
            _model = model;
            _controllerName = controllerName;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public Task<HttpResponseMessage> ExecuteAsync(CancellationToken cancellationToken)
        {
            var response = new HttpResponseMessage(HttpStatusCode.OK);
            //var parsedView = RazorEngine.Razor.Parse(_view, _model);
            var parsedView = RenderViewToString(_controllerName, _viewName, _model);
            response.Content = new StringContent(parsedView);
            response.Content.Headers.ContentType = new MediaTypeHeaderValue("text/html");
            return Task.FromResult(response);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="controllerName"></param>
        /// <param name="viewName"></param>
        /// <param name="viewData"></param>
        /// <returns></returns>
        public static string RenderViewToString(string controllerName, string viewName, object viewData)
        {
            using (var writer = new StringWriter())
            {
                var routeData = new System.Web.Routing.RouteData();
                routeData.Values.Add("controller", controllerName);
                var fakeControllerContext = new ControllerContext(new HttpContextWrapper(new HttpContext(new HttpRequest(null, "http://google.com", null), new HttpResponse(null))), routeData, new FakeController());
                var razorViewEngine = new RazorViewEngine();
                var razorViewResult = razorViewEngine.FindView(fakeControllerContext, viewName, "", false);

                var viewContext = new ViewContext(fakeControllerContext, razorViewResult.View, new ViewDataDictionary(viewData), new TempDataDictionary(), writer);
                razorViewResult.View.Render(viewContext, writer);
                return writer.ToString();
            }
        }
    }
    
}